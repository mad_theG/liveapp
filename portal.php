<?php
session_start(); // start/resume session

//ini_set('display_errors', 1); <- is this stuff necessary? can we remove it?
//error_reporting(E_ALL);
if (!isset($_SESSION['user_id'])) // if userid isn't stored in session variable...
{
    // ... then user is not logged in, move to login page
	header("location:index.php?alert=expired");
	session_destroy(); // destroy all data registered with this session
	exit; // and exit cleanly
}

// include php libraries

include 'common/JSON.php';
include 'common/functions.php';
include 'config.php';

//  GET VARIABLE CASES --------------------------

// store userid from session variable in userId variable
$userId = $_SESSION['user_id']; 
// store username from session variable in userName variable
$userName =  $_SESSION['user_name'];
// if formdesigner string is stored in session variable
if (isset($_SESSION['fromdesigner']))
{
	// store data from session variable in local formdesigner variable
	if($_SESSION['fromdesigner'] == "true")
	{
		$fromdesigner = "true";
	}
	else
	{
		$fromdesigner = "false";
	}
}
/*
	if formdesigner string isn't stored in session variable, then 
	formdesigner variable stores the string "false"
*/
else
{
	$fromdesigner = "false";
}

// What is the purpose of this code? ---------------------------------------------------------//

$message = '';
$payment = '';

// if alert variable was passed in through URL parameters...
if(isset($_GET['alert']))
{
	// store data retrieved from URL in payment variable
	$payment = $_GET['alert'];
	// if the string 'publish' is stored in payment variable...
	if($payment == 'publish')
	{
		// ... then store "publish" in message variable
		$message = "publish";
		// $target_path = "users-folder/$userName/liveapp/."; <- is this stuff necessary? can we remove it?
		// $cmd = "imgopt " . $target_path . "  &> imgopt.txt &";
		// $cmd = escapeshellcmd($cmd);
		// shell_exec($cmd);
	}
}

// --------------------------------------------------------------------------------------------//

// How're we using this code? ---------------------------------------------------------------- //
// (is this code being used for a twitter stream in the app or just for the share button?) --- //

$twitterItem = ''; // this stores "liveapp_id"
$twitter = '';

// if twitter variable is passed in through URL parameters...
if(isset($_GET['twitter']))
{
	// store data retrieved from URL in twitter variable
	$twitter = $_GET['twitter'];
}

// if item variable is passed in through URL parameters...
if(isset($_GET['item']))
{
	// store data retrieved from URL in twitterItem variable
	$twitterItem = $_GET['item'];
	// remove all occurences of the string 'input' from twitterItem variable
	$twitterItem = str_replace('input','', $twitterItem);
}

// if twitter and twitterItem variables aren't empty...
if($twitter !== '' && $twitterItem !== '')
{
	// assign twitter variable to url variable <- why?
	$url = $twitter;
	// create query to update database
	$updateQuery = "UPDATE liveapp SET url = '$twitter' WHERE liveapp_id = '$twitterItem'";
	// update database with our new twitter and twitterItem variables
	$updateResult = mysql_query($updateQuery);
}

// -------------------------------------------------------------------------------------------- //

//DATABASE QUERIES ------------------------------------------------
//THEME LAYOUT
// load "Aqua" theme (which is the default) into theme variable
$theme = "Aqua"; 
// create query to retrieve theme data from database
$settingsQuery = "SELECT * FROM user_settings WHERE user_id = '$userId'";
// retrieve theme data from database
$settingsResult = mysql_query($settingsQuery);
$settingsRow = mysql_fetch_array($settingsResult);
$preiumeTheme = '0'; // <--- what is this variable doing?
$premiumTheme = $settingsRow['premium_theme']; // <- what is this statement doing and why is it doing it?
$maxpages = $settingsRow['maxpages'];// <- what is this statement doing and why is it doing it?
// if a theme variable is stored in the database...
if($settingsRow['theme'] != '')
{
	/*
		...then retrieve the theme value stored in the database and 
		store it in the theme variable
	*/
	$theme = $settingsRow['theme'];
}
$icon = $settingsRow['app_icon'];
$appname = $settingsRow['app_name'];
$featured = $settingsRow['featured_in_app'];
$iconChange = $settingsRow['icon_change'];
$lastPublished = $settingsRow['last_publish']; // always stored in GMT timezone
$lastPublished = date("F d, Y, h:i A", strtotime($lastPublished));
$lastPublished .= " (GMT)"; // tell the user this timestamp is always in GMT

/*
//ICON RETRIEVAL
// iconQuery was a duplicate of layoutQuery. This code has been refactored
$iconQuery = "SELECT * FROM user_settings WHERE user_id = '$userId'"; 
$iconResult = mysql_query($iconQuery);
$iconRow = mysql_fetch_array($iconResult);
$icon = $iconRow['app_icon'];
$appname = $iconRow['app_name'];
$featured = $iconRow['featured_in_app'];
$iconChange = $iconRow['icon_change'];
*/

//ICON RETRIEVAL
$featuresQuery = "SELECT * FROM features WHERE user_id = '$userId'";
$featuresResult = mysql_query($featuresQuery);
$featuresRow = mysql_fetch_array($featuresResult);
$audio = $featuresRow['audio'];
$video = $featuresRow['video'];
$login = $featuresRow['login'];
$filebrowser = $featuresRow['filebrowser'];
$photogallery = $featuresRow['photogallery'];
$videogallery = $featuresRow['videogallery'];
$findme = $featuresRow['findme'];
$html = $featuresRow['html'];

//FB INFO/ PAID INFO/ PUBLISH INFO -------------------
$dataQuery = "SELECT * FROM users WHERE user_id = '$userId'";
$dataResult = mysql_query($dataQuery);
$dataRow = mysql_fetch_array($dataResult);
$fbId = $dataRow['fbid'];
$fbToken = $dataRow['accesstoken'];
$paid = $dataRow['paid'];
$publish = $dataRow['published'];
$premium = $dataRow['premium'];
$newuser = $dataRow["newuser"];
$foldersize = $dataRow['foldersize'];
$designer_id = $dataRow['designer_id'];
$launched = $dataRow['launched'];
$freeApp = $dataRow['designer_free'];
$userEmail = $dataRow['admin_email'];
// ZAC
$liToken = $dataRow["linkedin_token"];
$liSecret = $dataRow["linkedin_token_secret"];

/*
// ZAC - NEW
// Find the date of the app's most recent publish
// iconQuery was a duplicate of layoutQuery.
$publishDateQuery = "SELECT * FROM user_settings WHERE user_id='$userId'";
$publishDateResult = mysql_query($publishDateQuery);
$publishDateRow = mysql_fetch_array($publishDateResult);
$lastPublished = $publishDateRow['last_publish']; // always stored in GMT timezone
$lastPublished = date("F d, Y, h:i A", strtotime($lastPublished));
$lastPublished .= " (GMT)"; // tell the user this timestamp is always in GMT
*/

//FRANCIS: Load in any stored URLs for app (pass to App Store popup)
function createConnection() {
	$dbhost="mysql.liveapp.ca";
	$dbuser="onebiteadmin";
	$dbpass="Liveapp12#$";
	$dbname="onebitetest";
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}

try {
	$db = createConnection();
	//Check if the URL already exists first
	$sql = "SELECT * FROM onebite.appstore_urls WHERE app_name = '$userName';";
	$stmt = $db->prepare($sql);
	$stmt->execute();
	$urls = "";
	while ($row = $stmt->fetch(PDO::FETCH_OBJ)){
		$urls .= $row->store_id . "~" . $row->download_url . "^";
	}
	$_SESSION['urls'] = rtrim($urls, "^");
	$db	= null;
} catch(PDOException $e) {
	die('Error: ' . $e->getMessage());
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>LiveApp</title>
<!--script>window.location = "https://www.launchliveapp.com/temp_msg.html";</script-->
</head>

<!-- JQUERY and OTHER linked scripts-->
<script language="javascript" src="scripts/picker.js"></script>
<script type = "text/javascript" src="https://connect.facebook.net/en_US/all.js"></script>


<script type = "text/javascript" src="scripts/jquery-1.6.4.min.js"></script>
<script type = "text/javascript" src="scripts/jquery.scrollTo-min.js"></script>
<script type = "text/javascript"  src="scripts/jquery-ui-1.8.16.custom.min.js"></script>
<script type = "text/javascript" src="scripts/jquery.dropshadow.js"></script>
<script type = "text/javascript" src="scripts/jquery.topzindex.min.js"></script>
<script type = "text/javascript" src="scripts/jquery.form.js"></script>
<script type = "text/javascript" src="scripts/jquery.scrollTo-min.js"></script>
<script type = "text/javascript" src="scripts/popUp.js"></script>
<script type = "text/javascript" src="scripts/fileuploader.js"></script>
<script type = "text/javascript" src="scripts/jquery.ui.touch-punch.js"></script>
<script type = "text/javascript" src="scripts/spin.js"></script>
<!--tooltip plugin-->
<script type="text/javascript" src="scripts/jquery.qtip.min.js"></script>
<link href="NewPortalCSS/jquery.qtip.min.css" rel="stylesheet" type="text/css" />

<!--resizable column plugin-->
<script type="text/javascript" src="scripts/colResizable-1.3.min.js"></script>
<!-- Don't touch this! -->
<link class="include" rel="stylesheet" type="text/css" href="jqplot/jquery.jqplot.min.css" />
<!-- Additional plugins go here -->
<script class="include" type="text/javascript" src="jqplot/jquery.jqplot.min.js"></script>
<script class="include" type="text/javascript" src="jqplot/plugins/jqplot.barRenderer.min.js"></script>
<script class="include" type="text/javascript" src="jqplot/plugins/jqplot.pieRenderer.min.js"></script>
<script class="include" type="text/javascript" src="jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script class="include" type="text/javascript" src="jqplot/plugins/jqplot.pointLabels.min.js"></script>
<script type="text/javascript" src="jscolor/jscolor.js"></script>

<!-- PORTAL SCRIPTS LINKED-->
<script type = "text/javascript" src="new_portalFunctions/miscfunctions.js"></script>
<script type = "text/javascript" src="new_portalFunctions/pages.js"></script>
<script type = "text/javascript" src="new_portalFunctions/validation.js"></script>
<script type = "text/javascript" src="new_portalFunctions/popUp2.0.js"></script>
<script type = "text/javascript" src="new_portalFunctions/textediting.js"></script>
<script type = "text/javascript" src="new_portalFunctions/facebook.js"></script>
<script type = "text/javascript" src="new_portalFunctions/twitter.js"></script>
<script type = "text/javascript" src="new_portalFunctions/theme.js"></script>
<script type = "text/javascript" src="new_portalFunctions/doEdit.js"></script>
<script type = "text/javascript" src="new_portalFunctions/uploading.js"></script>
<script type = "text/javascript" src="new_portalFunctions/items.js"></script>
<script type = "text/javascript" src="new_portalFunctions/dragdrop.js"></script>
<script type = "text/javascript" src="new_portalFunctions/features.js"></script>
<script type = "text/javascript" src="new_portalFunctions/barfunctions.js"></script>
<script type = "text/javascript" src="new_portalFunctions/gallery.js"></script>
<script type = "text/javascript" src="new_portalFunctions/dragdrop_desktop.js"></script>
<script type = "text/javascript" src="new_portalFunctions/add_item_to_database.js"></script>
<script type = "text/javascript" src="new_portalFunctions/set_app_bg.js"></script>
<!-- ZAC -->
<script type = "text/javascript" src="new_portalFunctions/linkedin.js"></script>
<!-- LinkedIn javascript API -->
<script type="text/javascript" src="https://platform.linkedin.com/in.js">
  api_key: fn4fhodxr0oz
  authorize: true
  credentials_cookie: true
</script>

<!-- For rotating image in the publishing widnow by SRS -->
<script src="scripts/jQueryRotate.js"></script>

<!-- CSS LINKED-->
<link href="NewPortalCSS/portalMain.css" rel="stylesheet" type="text/css" />
<link href="NewPortalCSS/qrPrint.css" rel="stylesheet" type="text/css" media="print" />
<link href="themes/cupertino/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />

<link href="NewPortalCSS/picker.css" rel="stylesheet" type="text/css" />


<script type = "text/javascript" src="new_portalFunctions/accordianScript.js"></script>
<link href="NewPortalCSS/fileuploader.css" rel="stylesheet" type="text/css">

<link href="NewPortalCSS/widgetMenu.css" rel="stylesheet" type="text/css" />
<!--Taylor - plugins and libraries for rich text editor-->
<!--Taylor - plugins and libraries for rich text editor-->
<!--Redactor-->
<!--Notice:
	Redactor requires jquery 1.9.1, where as other plugins require 1.6.4
	to avoid conflict, I've loaded the version of jquery redactor needs into a variable called tQuery
	all references to 1.9.1 shall be made like: tQuery(...) instead of $(...).-->
<script src="scripts/spectrum/spectrumvaish.js" type="text/javascript"></script>
<link rel='stylesheet' href='scripts/spectrum/spectrumvaish.css' />
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="wvaish.js" type="text/javascript"></script>
<script src="redactor/redactor/redactor_vaish.js" type="text/javascript"></script>

<script src="redactor/plugins/fontcolor.js" type="text/javascript"></script>
<script src="redactor/plugins/fontfamily.js" type="text/javascript"></script>
<script src="redactor/plugins/fontsize.js" type="text/javascript"></script>
<script src="redactor/plugins/limiter.js" type="text/javascript"></script>
<link rel="stylesheet" href="redactor/redactor/redactor.css" />
<script>
	var tQuery = jQuery.noConflict(true);
	// takes references to $ and jquery global variables for 1.9.1 and places them in tQuery
	// switches to jQuery version 1.6.4
</script>

<script>
// global variable for row height
var ROW_HEIGHT = new Array();

// ZAC
// redirect to required https
if(document.location.protocol == 'http:') {
	window.location.href = "https:" + window.location.href.substring(window.location.protocol.length);
}
// MIKAEL - Redirect to iPad portal if this portal is accessed from the iPad


function updateLinkedInCSS()
{
	// executes when complete page is fully loaded, including all images and css
	$(".seperator.new-item").css("font-family", $('.textbox.new-item').css('font-family'));
	$(".seperator.new-item").css("background-color", $('.textbox.new-item').css('background-color'));
	$(".seperator.new-item").css("width", $('.textbox.new-item').css('width'));
	$(".seperator.new-item").css("height", $('.textbox.new-item').css('height'));
	$(".seperator.new-item").css("font-size", $('.textbox.new-item').css('font-size'));
	$(".seperator.new-item").css("line-height", $('.textbox.new-item').css('line-height'));
	$(".seperator.new-item").css("text-align", $('.textbox.new-item').css('text-align'));
	$(".seperator.new-item").css("color", $('.textbox.new-item').css('color'));
}

$(window).load(function()
{
	setTimeout("updateLinkedInCSS()", 5000);
});
// end of ZAC code

//Variables
var fromdesigner = '<?php echo $fromdesigner?>';
var disabledButtons = false;
var pagenumber = 1;
var notificationOpen;
var deleteedit;
var addcolumn;
var audio = <?php echo $audio;?>;
var video = <?php echo $video;?>;
var filebrowser = <?php echo $filebrowser;?>;
var videogallery = <?php echo $videogallery;?>;
var photogallery = <?php echo $photogallery;?>;
var findme = <?php echo $findme;?>;
var html = <?php echo $html;?>;
var icon_change = '<?php echo $iconChange; ?>';
var premium = '<?php echo $premium?>';
var payment = '<?php echo $payment; ?>';
var premium_theme = '<?php echo $premiumTheme;?>';
var theme = '<?php echo $theme;?>'
var publish = '<?php echo $publish;?>';
var paid = '<?php echo $paid; ?>';
var message = '<?php echo $message;?>';
var editItem;
var user_id = '<?php echo $userId ?>';
var user_name = '<?php echo $userName ?>';
var fbid = '<?php echo $fbId ?>';
var fbtoken = '<?php echo $fbToken ?>';
var myTemplate = '<?php echo $theme;?>';
var maxpages = '<?php echo $maxpages; ?>';
var newuser = '<?php echo $newuser; ?>';
var user_email = '<?php echo $userEmail ?>';
var foldersize = '<? echo $foldersize; ?>';
var folderfull = false;
var isOpen = false;
var formtype = "";
var wizardOn = false;
var tips = new Array();
var isEdit = false;
var proThemes = new Array();
var lightBoxOn = false;
var designer_id = '<?php echo $designer_id; ?>';
var launched = '<? echo $launched; ?>';
var freeapp ='<?php echo $freeApp?>';
// ZAC
var liToken = '<?php echo $liToken; ?>';
var liSecret = '<?php echo $liSecret; ?>';

browserDetect();


// INITIALIZING JQUERY FUNCTIONS
jQuery(function()
{
 var agentShultz = navigator.userAgent.toLowerCase();
   if(agentShultz.indexOf("ipad")!= -1){
    	window.location = "https://www.launchliveapp.com/testportal_galleryiPad.php";
    }

	$("#EditorHeader").mouseover(function(e){
	   if(isEdit == false)
	   {
			$('#text_formatting').hide();
	   }
	});
});

$(document).ready(function() {
	$('#bg-color').change(function () {
		var liveapp_id = $("#liveapp_idimg").val();
		var $img = $('#' + liveapp_id);
		$img.css('background-color', $('#bg-color').val());
	});
});

function makeItemBgTransparent() {
	if($('#bg-color').length != 0) {
		$('#bg-color').css('background-color', 'transparent');
		//$('#bg-color').css('color', 'black');
		$('#bg-color').val('transparent');
	}
}

function removeItemImage() {
	var liveapp_id = $("#liveapp_idimg").val();
	var $img = $('#' + liveapp_id);
	$img.css('background-image', 'url("portalGraphics/transparent.png")');
	$img.css('background-color', $('#bg-color').val());
	$img.css('width', '380px');
	$img.css('height', '300px');
	$("#no_img").val('true');
}

function ShowStats(appId)
{
	launchPopUp('App Stats','LiveApp Stats', 700, 'http://www.liveappdesign.com/test/new_app_stats.php?user_id='+user_id);
}

function onloadFunctions()
{
	paymentNotification();
	changePage(1);
	tipSection();
	deleteSkip();
	radioButtonLogic("image");
	loadTemplates();
	closebar();
	newUser();
	disableFeatures();
	hidePublish();
	popUpPublish();
}

(function(d){
    var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    d.getElementsByTagName('head')[0].appendChild(js);
}(document));

function showFeedback(type){
    document.getElementById('settingsFrame').src = "";
	if(type == 'feedback'){
		document.getElementById('settingsFrame').src = "user_feedback.php?adminemail=" + user_email;
	}
	if(type == 'help'){
		document.getElementById('settingsFrame').src = "user_feedback.php?adminemail=" + user_email + "&type=help";
	}
	$('#lightbox').css('filter', 'alpha(opacity=80)');
	$("#lightbox").topZIndex();
	$("#lightbox").fadeIn(300);
	$('#settingsHolder').topZIndex();
	$('#settingsHolder').fadeIn(300);
	$('#settingsHolder').css("top", window.pageYOffset);
	settingsOn = true;
	lightBoxOn = true;
}

function show_page(title, page){
	launchPopUp("", title, 700, page);
}

</script>

<body onload = "onloadFunctions()" >
<div id="cssHolder"></div>
<div id="lightbox"> </div>
<!-- HOLDS CSS THEME -->
<div id="fb-root"></div>

<div id="header">
	<div id="topMenu">
		<div id="topLinks">
			<a onClick="show_page('', 'new_user_feedback.php?type=help')" target="_blank">Help</a> | <a onClick = "show_page('My Account', 'new_myaccount.php')">My Account</a> | <a onClick = "show_page('', 'new_user_feedback.php')">Feedback</a> |<?php if($fromdesigner == 'true'){echo '<a href="javascript:" onclick="window.location=\'http://www.liveappdesign.com/client_manager.php\';">Manage Clients</a>';}else{echo '<a href = "https://www.launchliveapp.com/logout.php">Log Out</a>';}?>
		</div>
	</div>
	<div id="headerContent" class="wrapper" >
		<div id="laLogo">
			<a href="https://www.launchliveapp.com">
				<img src="https://www.launchliveapp.com/new_portal/PortalImages/portal_logo.png" />
			</a>
		</div>


		<div id="userInfoHolder" onClick="changeSettings()">
			<div id="iconHolder" >
				<img id="appiconholder" src = "<?php if ($icon == ''){echo 'portalIMG/icon_512.png';} else{echo '../users-folder/'.$userName.'/liveapp/appicon.png';}?>" ></img></div>
				<div id="InfoWrapper"> <!-- ZAC - NEW -->
					<!--<div id="userInfo" > App Name: </div>-->
					<div id="userInfo">App Name:</div>
					<div id="AppName"><?php echo $userName; ?></div><br /><br />
					<div id="userInfo2">Last Updated:</div>
					<div id="LastPublished"><?php echo $lastPublished; ?></div>
				</div>
			<div  onClick="changeSettings()"></div>

		</div>
		<div id = "app_settings" style = "display:none">
			<form name="audio" action="processportal.php" method="POST" enctype="multipart/form-data" target="uploadframeapp" onSubmit="return uploadapp(this)">
				<div style="font-size:18px; color:#6d6e70; margin-top:10px; margin-left:10px;" ><b><u>Settings</u></b></div>
				<div style="margin-left:20px; margin-top:10px;">
				<table width="250px" >
					<tr>
						<td style="font-size:14px; font-weight:bold; color:#6d6e70;">Upload App Icon:</td>
					</tr>
					<tr>
						<td>
						<input type="file" id="uploadedicon" name="uploadedicon" tabindex="1"   onChange = "instantuploadicon()"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
						<input type="submit" value="Save" tabindex="2" id = "settings" style = "visibility:hidden"/>
						<input type="hidden" id="formTXT" name="formTXT" value="settings" />
						</td>
					</tr>
				</table>
				</div>
			</form>
			<iframe id="uploadframeapp" name="uploadframeapp" src="processportal.php" width="8" height="8" scrolling="no" frameborder="0"></iframe>
		</div>
		<div id="TipBg">
			<div id="tipHolder" ></div>
		</div>
	</div>
	</div>
	</div>
</div>

<div id="controls">
<div id="controlBar" class="wrapper" >
<!--srs_bg-->
		<div class="ToolbarLeft" id="ThemesButton" onClick="show_bg_color_form();">
			<img class="ToolbarButtonsLeft" src="PortalImages/Background.png"/>
		</div>
		<div class="ToolbarLeft" id="EditIconButton" onClick="changeSettings()">
			<img class="ToolbarButtonsLeft" src="PortalImages/EditIcon.png"/>
		</div>


		<div class="ToolbarLeft" id="StatsButton" onClick="launchPopUp('', '', 664, 'sharmeemTest/new_app_stats_test15_graph.php')">
			<img class="ToolbarButtonsLeft" src="PortalImages/Stats.png"/>
		</div>
		<!--FRANCIS: Changed app store popup-->
		<div class="ToolbarLeft" id="AppStoreButton" onClick="launchPopUp('', '', 750, 'AppStorePopUp/AS_popUp_Francis.php?app_name=<?php echo $userName ?>&userId=<?php echo $userId ?>&loadedURLs=')">
			<img class="ToolbarButtonsLeft" src="PortalImages/AppStores.png"/>
		</div>
		<div class="ToolbarLeft" id="ManageUsersButton" class="ToolbarButtonsLeft" onClick = "show_page('', 'new_manageusers.php')">
			<?php if ($login == 1) { ?>
		   <img class="ToolbarButtonsLeft" src="PortalImages/ManageUsers.png" style="cursor: pointer; cursor: hand;" />
			<?php } ?>
		</div>

    	<div class="ToolbarRight" id="PublishButton">
			<img id="publishButton" src="PortalImages/publish_button.png"  onClick = "checkPublish();"/>
		</div>
		<div class="ToolbarRight" id ="fbpublish">
			<img class="ToolbarButtonsSocial" src="PortalImages/fb_publish.png"/>
		</div>
		<div class="ToolbarRight" id="TwitterShareButton" >
				<a href="https://twitter.com/share?url=https://www.launchliveapp.com/<?php echo $userName; ?>/&text=Download+the+<?php echo $userName?>+App+by+visiting+this+link+from+your+mobile+phone!" target="_blank" data-url="https://www.launchliveapp.com/<?php echo $userName; ?>" data-text="Download the <?php echo $userName?> App by visiting this link from your mobile phone!"  data-hashtags="LiveApp">
					<img class="ToolbarButtonsSocial" src="PortalImages/twitter_publish.png"/>
				</a>
		</div>
</div>
</div>
<div id="mainContent">
	<div id='overlay_spinner' style="display:none; position:absolute"></div>
	<div id="EditorWrapper">
		<div class="ColumnHeader" id="EditorHeader">
				<div id = "pageNameText"></div>
		</div>
		<div id="portalHolder">
			<!-- Put Portal Here-->
			<div id="doc-editor-content">
				<ul id="item-holder">
				</ul>
			</div>
		</div>
	</div>

	<!-- Start of the element list -->
	<div id="ElementMenu">
		<div class="ColumnHeader" id="ElementsHeader">
				Elements
		</div>
		<div id="doc-editor-options">
		<div id="accordionMenuHolder" class="scrollable">
			<style>
			 .scrollable {
				overflow-y: scroll;
				overflow-x:hidden;
			}
			.subMenu{
				width: 230px;
			}
			.new-item{
				margin:auto;
			}

			/* ZAC - NEW */
			/* Hide all the basic elements except Custom Image */
			.header.new-item {
				display:none;
			}
			.textbox.new-item {

			}
			.seperator.new-item {
				display:none;
			}
			.phone.new-item {
				display:none;
			}
			.sms.new-item {
				display:none;
			}
			.email.new-item {
				display:none;
			}
			.web.new-item {
				display:none;
			}
			.maps.new-item {
				display:none;
			}
			.footer.new-item {
				display:none;
			}
			</style>
			<div id="subMenu1" class="subMenu">
				<div class="headerClosed menuHeaderTitle" onclick="openContent(this)">Custom</div>
					<div class="menuContent">
						<ul><!-- ZAC - NEW / Added 'display:none' style to spacing div tags -->
							<li class="header new-item" type="header">
							<div class="header template">header</div>
							</li>
							<div style="height:2px;"></div>
							<li class="textbox new-item" type="textbox">
							<div class="textbox template">Text Box</div>
							</li>
							<div style="height:2px;display:none;"></div>
							<li class="seperator new-item" type="seperator" style="font-family: Verdana, Geneva, sans-serif; background-color: rgba(0, 0, 0, 0); width: 115px; height: 18px; font-size: 16px; line-height: normal; text-align: center; color: rgb(0, 0, 0); ">
							<div class="seperator template">------------</div>
							</li>
							<div style="height:2px;display:none;"></div>
							<li class="phone new-item" type="phone">
							<div class="phone template">phone</div>
							</li>
							<div style="height:2px;display:none;"></div>
							<li class="sms new-item" type="sms">
							<div class="sms template">sms</div>
							</li>
							<div style="height:2px;display:none;"></div>
							<li class="email new-item" type="email">
							<div class="email template">email</div>
							</li>
							<div style="height:2px;display:none;"></div>
							<li class="web new-item" type="web">
							<div class="web template">web</div>
							</li>
							<div style="height:2px;display:none;"></div>
							<li class="maps new-item" type="maps">
							<div class="maps template">map</div>
							</li>
							<div style="height:2px;display:none;"></div>
							<li class="footer new-item" type="footer">
							<div class="footer template">footer</div>
							</li>
							<div style="height:2px;display:none;">
							</div>
							<!--SRS-->
							<li class="img new-item" type="img">Custom Image</li>
							<div style="height:2px"></div>
							<li class="two_column new-item" type="column"></li>
							<div style = "height:2px"></div>
							<li class="three_column new-item" type="column"></li>
							<div style = "height:2px"></div>
							<li class="four_column new-item" type="column"></li>
							<div style = "height:2px"></div>
							<!--SRS-->
							<!--<li class="findme new-item" type="findme"></li>-->
							<div style = "height:2px">
							</div>
						         <li class="html new-item" type="html"></li>
							<div style = "height:2px">
							</div>
							</ul>
						</div>
			</div>
			<div id="subMenu1" class="subMenu">
				<div class="headerClosed menuHeaderTitle" onclick="openContent(this)">Social Media</div>
				<div class="menuContent">
					<ul>
					<!-- <li class="fb new-item" type="fb"><span class="itemIndent"></span></li><div style = "height:2px"></div> -->
						<li class="twitter new-item" type="twitter"><span class="itemIndent"></span></li><div style="height:2px"></div>
						<li class="li new-item" type="li"></li><div style="height:2px"></div>
					</ul>
				</div>
			</div>
			<div id="subMenu1" class="subMenu">
				<div class="headerClosed menuHeaderTitle" onclick="openContent(this)">Multi Media</div>
				<div class="menuContent">
					<ul>
						<li class="yt new-item" type="yt"><span class="itemIndent"></span></li><div style="height:2px"></div>
						<div style="height:2px"></div>
						<li class="audio new-item" type="audio"><span class="itemIndent"></span></li><div style="height:2px"></div>
						<div style="height:2px"></div>
                        <li class="filebrowser new-item" type="filebrowser">
                        <div class="filebrowser template"></div>
                        </li>
						<!-- <div style="height:2px"></div> -->
						<!-- <li class="video new-item" type="video"><span class="itemIndent">Video</span></li><div style = "height:2px"></div>  -->
						<div style="height:2px"></div>
						<li class="photogallery new-item" type="photogallery">
                        <div class="photogallery template"></div>
                        </li>
                        <div style="height:2px"></div>
                        <li class="videogallery new-item" type="videogallery">
                        <div class="videogallery template"></div>
                        </li>
                        <div style="height:2px"></div>
					</ul>
				</div>
			</div>
			<!--<div id="subMenu1" class="subMenu">
				<div class="headerClosed menuHeaderTitle" onclick="openContent(this)">Revenue</div>
				<div class="menuContent">
					<span style="font-size:12px;">Revenue features coming soon!</span>
				</div>
			</div>-->
		</div>
		</div>
	</div>
<!-- End of the element list -->

	<!-- Start of pages menu -->
	<div id = "pagesMenu">
		<div class="ColumnHeader" id="PagesHeader">
			My Pages
		</div>

		<div id='pageBoxMid'>
		<div id='addPage' onClick = "addPage();"></div>
            <div  id='pageBoxDescription'>Drag and Drop your pages to re-order them in your LiveApp.</div>
            <ul id = "pagesMenuList">
			<hr  style="margin-left:-20px;opacity:0.5;"/>
            <?php
            $selectQuery = "SELECT * FROM pages WHERE user_id = '$userId' ORDER BY page_order ASC";
            $selectResult = mysql_query($selectQuery);
            $numberOfRows = mysql_num_rows($selectResult);
            for($i = 0; $i < $numberOfRows; $i++)
            {

                    $pagename = mysql_result($selectResult, $i, "page_name");
                    $pageId = mysql_result($selectResult, $i, "page_id");
					$hidden = mysql_result($selectResult, $i, "hidden");
            ?>
                <li class = "pageItem"  id = "page<?php echo $pageId?>" >
                    <!-- Please itirate through items and change their class name to  pageNameClosed then with whatever page is open change it to pageNameOpen-->
                    <div class="pageNameClosed" id = "title<?php echo $pageId;?>" align = "left" onClick = "changePage(<?php echo $pageId?>)"><div class='pageTitle' id = "name<?php echo $pageId?>"><?php echo $pagename?></div></div>
                    <div class ="pageOptionsBtn" onclick= "openPage(<?php echo $pageId?>);"></div>
                    <div class="deletePageBtn" onclick="deletePage(<?php echo $pageId?>);"></div>
					<input id = "hidden<?php echo $pageId?>" value = "<?php echo $hidden?>" type = "hidden" />
                </li>
            <?php
            }
            ?>
		</ul>

        </div>
        <div id='pageBoxBot'></div>
	</div>
	<!-- End of pages menu -->

<!--<div id="copyright">LiveApp is copyright <strong></strong>SiliconW</div>-->

<!-- bacon  The Pages menu list -->


<div id = "facebook_form" style = "display:none">
  <form name = "facebook" action = "https://www.launchliveapp.com/processportal.php">
    <table class="wizardTable">
	 <tr>
        <td colspan="3"><input type="hidden" id="urlTXTfb" name="urlTXTfb" tabindex="2" class="text-input"/></td>
	 </tr>
	 <tr>
      <span style="margin-top:10px;">
      <center><br>
		<img id = "fblogin" src = "PortalImages/facebook-login-button.png"/>
        </center>
       </span>
      </tr>
	  <tr>
      <span style="margin-top:10px;">
      <center><br>
		<img id = "fblogout" src = "PortalImages/facebook-logout.jpg"/>
       </center>
       </span>
	  </tr>

      <tr>
		<td><input type="hidden" id="commentTXTfb" name="commentTXTfb" class="text-input" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTfb" name="numTXTfb"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="facebook" value="Save" style="visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="facebook" />
          <input type="hidden" id="liveapp_idfb" name="liveapp_idfb"  value=""/></td>
      </tr>
    </table>
  </form>
</div>
<div id = "twitter_form" style = "display:none">
  <form name = "twitter" action = "https://www.launchliveapp.com/processportal.php">
    <table class="wizardTable">
      <tr>
        <td colspan="3"><input type="hidden" id="urlTXTtwitter" name="urlTXTtwitter" tabindex="2" class="text-input" /></td>
      </tr>
      <tr>
        <td><input type="hidden" id="commentTXTtwitter" name="commentTXTtwitter" class="text-input" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTtwitter" name="numTXTtwitter"  class="text-input"/></td>
      </tr>
	  <tr>
		<td><img src = "https://www.launchliveapp.com/TwitterTest/twitterSignIn.png" onClick = "openTwitter();" /></td>
	  </tr>
      <tr>
        <td colspan="6"><input type="submit" id="twitter" value="Save" style="visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="facebook" />
          <input type="hidden" id="liveapp_idtwitter" name="liveapp_idtwitter"  value=""/>
		</td>
      </tr>
    </table>
  </form>
</div>
<!--srs_bg-->
<div id = "app_bg_form_holder">
	<form id="app_bg_form" name="app_bg_form" action="https://www.launchliveapp.com/processportal.php" method="post" enctype="multipart/form-data" target="uploadbg">
		<iframe id="uploadbg" name="uploadbg" src="https://www.launchliveapp.com/processportal.php" width="1" height="1" scrolling="no" frameborder="0"></iframe>
		<label style="font-size:11px;" for="appBgColor">Background-color:</label>
		<input class="color app_color" id="appBgColor" name="appBgColor"></br></br>
		<label style="font-size:11px;" for="appBgImage">Background-image:</label>
		<input type="file" id="appBgImage" name="appBgImage">
		<input type="hidden" name="formTXT" value="set_bg"></br>
		<input id="appUserIDBg" type="hidden" name="userId">
	</form>
	<div id="remove_bg_btn_holder">
		<button id="remove_bg_btn" onclick="removeBGImage()">Remove the Background Image</button>
		<div id="remove_bg_msg_holder" ><span id="remove_bg_msg"></span></div>
	</div>
	<div id="bg_caution">**Please note that the background-image will look different in an app. It will be fullscreen and fixed. A scroller will be added for overflowing content.</div>
</div>

<div id = "image_form" style = "display:none">
  <form name="image" action="https://www.launchliveapp.com/processportal.php" method="post" enctype="multipart/form-data" target="uploadframe" onSubmit="return uploadimg(this)">
    <table class="wizardTable">
      <tr>
        <td colspan="10"><input type="radio" id="noactionimg" name="group1" value="image" onchange="radioButtonLogic('image')" />
          Image
          <input type="radio" id="mapimg" name="group1" value="maps" onchange="radioButtonLogic('maps')" />
          Maps
          <input type="radio" id="webimg" name="group1" value="web" onchange="radioButtonLogic('web')" />
          Web<br>
          <input type="radio" id="smsimg" name="group1" value="sms" onchange="radioButtonLogic('sms')" />
          Sms
          <input type="radio" id="phoneimg" name="group1" value="phone" onchange="radioButtonLogic('phone')" />
          Phone
          <input type="radio" id="emailimg" name="group1" value="email" onchange="radioButtonLogic('email')" />
          Email </td>
      </tr>
	  <tr>
		  <td>Background-color:</td>
		  <td><input class="color {required:false}" id="bg-color" name="bg-color"></td>
	  </tr>
	  <tr>
		<td></td>
		<td style="text-align:left"><button onclick="makeItemBgTransparent()" id="bg-color-trans-btn" name="bg-color-trans-btn">Remove Background Color</button></td>
	</tr>
      <tr>
        <td>Upload Image:</td>
        <td><input type="file" id="uploadedfile" name="uploadedfile" tabindex="1" onChange = "instantupload()"/>
      </tr>
      <tr id = "imgweb">
		<td colspan = "10">
			<select id = "linkdropdownimg" class = "linkdropdown" name = "link" onChange = "linkDropDown(this.options[this.selectedIndex].value)">
				<option value = "externallink">External Link</option>
				<option value = "liveappshare">Share Page</option>
				<?php
				$selectQuery = "SELECT * FROM pages WHERE user_id = '$userId'";
				$selectResult = mysql_query($selectQuery);
				while($pages = mysql_fetch_array($selectResult))
				{
				?>
				<option value = <?php echo $pages["page_id"]?>><?php echo $pages["page_name"]?></option>
				<?php
				}
				?>
			</select>
			<div class = 'weburl'>
			URL:
			<input type="text" id="urlTXTimg" name="urlTXTimg" tabindex="2" class="text-input" />
			</div>
		</td>
		</tr>
		<!--
		<tr>
		<td class = "weburl">URL:</td>
        <td class = "weburl"colspan="3"></td>
      </tr>
	  -->
      <tr id = "imgsms">
        <td>SMS#:</td>
        <td><input type="text" id="smsTXTimg" name="smsTXTimg" class="text-input" /></td>
      </tr>
      <tr id = "imgphone">
        <td>Phone #:</td>
        <td><input type="text" id="phoneTXTimg" name="phoneTXTimg" class="text-input" /></td>
      </tr>
      <tr id = "imgemail">
        <td>E-mail:</td>
        <td><input type="text" id="emailTXTimg" name="emailTXTimg" class="text-input" /></td>
      </tr>
      <tr id = "imgcomment">
        <td>Comment:</td>
        <td><input type="text" id="commentTXTimg" name="commentTXTimg" class="text-input" /></td>
      </tr>
	 <tr id = "imgstreet">
        <td>Street:</td>
        <td><input type="text" id="streetTXTimg" name="streetTXTimg" class="text-input" /></td>
      </tr>
      <tr id = "imgcity">
        <td>City:</td>
        <td><input type="text" id="cityTXTimg" name="cityTXTimg" class="text-input" /></td>
      </tr>
      <tr id = "imgprovince">
        <td>Province / State:</td>
        <td><input type="text" id="provinceTXTimg" name="provinceTXTimg" class="text-input" /></td>
      </tr>
      <tr id = "imgpostal">
        <td>Postal / Zip Code:</td>
        <td><input type="text" id="postalTXTimg" name="postalTXTimg" class="text-input" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTimg" name="numTXTimg"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="image" value="Upload" style = "visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="image" />
          <input type="hidden" id="liveapp_idimg" name="liveapp_idimg"  value=""/></td>
		  <input type = "hidden" id = "imgbold" name = "bold" value = 'off'/>
		  <input type = "hidden" id = "imgunderline" name = "underline" value = 'off'/>
		  <input type = "hidden" id = "imgitalic" name = "italic" value = 'off'/>
		  <input type = "hidden" id = "imgcolor" name = "color" value = 'off'/>
		  <input type = "hidden" id = "imgfont" name = "font" value = 'off'/>
		  <input type = "hidden" id = "imgsize" name = "size" value = 'off'/>
		  <iframe id="uploadframe" name="uploadframe" src="https://www.launchliveapp.com/processportal.php" width="1" height="1" scrolling="no" frameborder="0"></iframe>
      </tr>
    </table>
  </form>
</div>
<div id="youtube_form" style = "display:none">
  <form name = "youtube" action = "https://www.launchliveapp.com/processportal.php">
    <table class="wizardTable">
      <tr>
        <td colspan="4"><b><u>Add Youtube</u></b></td>
      </tr>
      <tr>
        <td>URL:</td>
        <td colspan="3"><input type="text" id="urlTXTyt" name="urlTXTyt" tabindex="2" class="text-input" /></td>
      </tr>
      <tr>
        <td><input type="hidden" id="commentTXTyt" name="commentTXTyt" class="text-input" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTyt" name="numTXTyt"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="youtube" value="Save" style="visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="youtube" />
          <input type="hidden" id="liveapp_idyt" name="liveapp_idyt"  value=""/></td>
      </tr>
    </table>
  </form>
</div>
<div id="sms_form" style = "display:none">
  <form name = "sms" action = "https://www.launchliveapp.com/processportal.php">
    <table class="wizardTable">
      <tr>
        <td colspan="4"><b><u>Add SMS</u></b></td>
      </tr>
      <tr>
        <td>Number:</td>
        <td colspan="3"><input type="text" id="urlTXTsms" name="urlTXTsms" tabindex="2" class="text-input" /></td>
      </tr>
      <tr>
        <td><input type="hidden" id="commentTXTsms" name="commentTXTsms" class="text-input" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTsms" name="numTXTsms"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="sms" value="Save" style="visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="sms" />
          <input type="hidden" id="liveapp_idsms" name="liveapp_idsms"  value=""/></td>
      </tr>
    </table>
  </form>
</div>
<div id="web_form" action = "https://www.launchliveapp.com/processportal.php" style = "display:none">
  <form name = "web" >
    <table class="wizardTable">
	  <tr>
		<td colspan="4"><b><u>Add Web</u></b></td>
	  </tr>
	  <tr>
		<td colspan = "4">
			<select id = "linkdropdown" class = "linkdropdown" onChange = "linkDropDown(this.options[this.selectedIndex].value)">
				<option value = "externallink">External Link</option>
				<?php
				$selectQuery = "SELECT * FROM pages WHERE user_id = '$userId'";
				$selectResult = mysql_query($selectQuery);
				while($pages = mysql_fetch_array($selectResult))
				{
				?>
				<option value = <?php echo $pages["page_id"]?>><?php echo $pages["page_name"]?></option>
				<?php
				}
				?>
			</select>
		</td>
	  </tr>
	<tr class = "weburl">
		<td>URL:</td>
		<td colspan="3"><input type="text" id="urlTXTweb" name="urlTXTweb" tabindex="2" class="text-input" /></td>
	</tr>

	<tr>
		<td><input type="hidden" id="commentTXTweb" name="commentTXTweb" class="text-input" /></td>
	</tr>
	<tr>
		<td colspan="3"><input type="hidden" id="numTXTweb" name="numTXTweb"  class="text-input"/></td>
	</tr>
	<tr>
		<td colspan="6"><input type="submit" id="web" value="Save" style="visibility:hidden"/>
		<input type="hidden" id="formTXT" name="formTXT" value="web" />
		<input type="hidden" id="liveapp_idweb" name="liveapp_idweb"  value=""/></td>
	</tr>
    </table>
  </form>
</div>
<div id="phone_form" style = "display:none">
  <form name = "phone" action = "https://www.launchliveapp.com/processportal.php">
    <table class="wizardTable">
      <tr>
        <td colspan="4"><b><u>Add phone</u></b></td>
      </tr>
      <tr>
        <td>Phone #:</td>
        <td colspan="3"><input type="text" id="urlTXTphone" name="urlTXTphone" tabindex="2" class="text-input" /></td>
      </tr>
      <tr>
        <td><input type="hidden" id="commentTXTphone" name="commentTXTphone" class="text-input" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTphone" name="numTXTphone"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="phone" value="Save" style="visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="phone" />
          <input type="hidden" id="liveapp_idphone" name="liveapp_idphone"  value=""/></td>
      </tr>
    </table>
  </form>
</div>
<div id="email_form" style = "display:none">
  <form name = "email" action = "https://www.launchliveapp.com/processportal.php">
    <table class="wizardTable">
      <tr>
        <td colspan="4"><b><u>Add email</u></b></td>
      </tr>
      <tr>
        <td>Email:</td>
        <td colspan="3"><input type="text" id="urlTXTemail" name="urlTXTemail" tabindex="2" class="text-input" /></td>
      </tr>
      <tr>
        <td><input type="hidden" id="commentTXTemail" name="commentTXTemail" class="text-input" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTemail" name="numTXTemail"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="email" value="Save" style="visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="email" />
          <input type="hidden" id="liveapp_idemail" name="liveapp_idemail"  value=""/></td>
      </tr>
    </table>
  </form>
</div>
<div id="linkedin_form" style = "display:none">
  <form name = "linkedin" action = "https://www.launchliveapp.com/processportal.php">
    <table class="wizardTable">
	  <!-- ZAC -->
	  <tr>
		<td>
		<br />
		<br />
		<script type="IN/Login" data-onAuth="authorizeLinkedIn" data-onLogout="logoutLinkedIn">
			<button id="linkedinLogout" type="button" value="Logout" onClick="IN.User.logout()"><img src = "portalGraphics/LogOut_Btn.jpg" /></button>
		</script>
		</td>
	  </tr>
	  <!--<tr>
        <td>LinkedIn Public Profile URL:</td>
	  </tr>-->
	  <tr>
        <td><input style="width:75%" type="hidden" id="urlTXTli" name="urlTXTli" tabindex="2" class="text-input" onChange="loadLinkedInData()" /></td>
      </tr>
      <tr>
        <td><input type="hidden" id="commentTXTli" name="commentTXTli" class="text-input" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTli" name="numTXTli"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="linkedin" value="Save" style="visibility:hidden"/>
		  <input type="hidden" id="restToken" name="linkedinToken" value="" />
		  <input type="hidden" id="restToken" name="linkedinTokenSecret" value="" />
          <input type="hidden" id="formTXT" name="formTXT" value="linkedin" />
          <input type="hidden" id="liveapp_idli" name="liveapp_idli"  value=""/></td>
      </tr>
    </table>
  </form>
</div>
<div id="hf_form" style = "display:none">
   <form name="hf" action="https://www.launchliveapp.com/processportal.php" method="post" enctype="multipart/form-data" target="uploadframehf" onSubmit="return uploadhf(this)">
    <table class="wizardTable">
      <tr>
        <td>Upload Image:</td>
        <td><input type="file" id="uploadedfilehf" name="uploadedfilehf" tabindex="1"  onChange = "instantuploadhf()" />
      </tr>
      <tr>
        <td><input type="hidden" id="commentTXThf" name="commentTXThf" class="text-input" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXThf" name="numTXThf"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="hf" value="Upload" onClick = "showFileSize()" style = "visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="header" />
          <input type="hidden" id="liveapp_idhf" name="liveapp_idhf"  value=""/></td>
		  <input type = "hidden" id = "hfbold" name = "bold" value = 'off'/>
		  <input type = "hidden" id = "hfunderline" name = "underline" value = 'off'/>
		  <input type = "hidden" id = "hfitalic" name = "italic" value = 'off'/>
		  <input type = "hidden" id = "hfcolor" name = "color" value = 'off'/>
		  <input type = "hidden" id = "hffont" name = "font" value = 'off'/>
		  <input type = "hidden" id = "hfsize" name = "size" value = 'off'/>
		  <iframe id="uploadframehf" name="uploadframehf" src="https://www.launchliveapp.com/processportal.php" width="8" height="8" scrolling="no" frameborder="0"></iframe>
      </tr>
    </table>
  </form>
</div>
<?php
$pro = '';
if($paid == 0)
{
$pro = '<img src = "portalGraphics/goPro.png" onClick = "openAlert(\'upgrade\');">';
}
?>
<div id="audio_form" style = "display:none">
  <form id = 'formaudio' name = "audio" action = "https://www.launchliveapp.com/processportal.php" enctype="multipart/form-data" method = "POST" onsubmit = "return showFileSize('audio')">
    <table class="wizardTable">
      <tr>
        <td>Upload Audio:</td>
		<!--
        <td><input type="hidden" id="uploadedfileaudio" name="uploadedfileaudio" tabindex="1"/>
	  -->
      </tr>
	  <tr>
	  <td>
		<div id="uploadedfileaudio" align = "middle">
			<noscript>
				<p>Please enable JavaScript to use file uploader.</p>
				<!-- or put a simple form for upload here -->
			</noscript>
		</div>
	  </td>
	  </tr>
      <tr>
        <td><input type="hidden" id="commentTXTaudio" name="commentTXTaudio" class="text-input" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTaudio" name="numTXTaudio"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="audio" value="Upload" onClick = "showFileSize()" style = "visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="audio" />
          <input type="hidden" id="liveapp_idaudio" name="liveapp_idaudio"  value=""/></td>
      </tr>
    </table>
  </form>
 <center> <?php echo $pro;?> </center>
</div>
<div id="video_form" style = "display:none">
  <form id = 'formvideo' name = "video" action = "https://www.launchliveapp.com/processportal.php" enctype="multipart/form-data" method = "POST" onsubmit = "return showFileSize('video')">
    <table class="wizardTable">
	  <tr>
        <td colspan="10">
		 <input type="radio" id="linkvideo" name="group2" value="link" onchange="radioButtonVideo('link')" />
          Link to a Video
          <input type="radio" id="uploadvideo" name="group2" value="upload" onchange="radioButtonVideo('upload')" />
          Upload Your own Video
		 </td>
      </tr>
	  <tr id = 'videoupload'>
		 <td>
			<div id="uploadedfilevideo" align = "middle">
				<noscript>
					<p>Please enable JavaScript to use file uploader.</p>
					<!-- or put a simple form for upload here -->
				</noscript>
			</div>
		</td>
	<!--
        <td>Upload Video:</td>
        <td><input type="file" id="uploadedfilevideo" name="uploadedfilevideo" tabindex="1" /></td>
      -->
	  </tr>
	  <tr id = 'videoupload1'>
		<td colspan = '10'>(Max File size : 20M)</td>
		</tr>
	  <tr id = 'videolink'>
		<td>Link To Video:</td>
		<td><input type = 'text' id = "linkTXTvideo" name = "linkTXTvideo" class = "text-input" /></td>
	  </tr>
	  <tr id = 'videolink1'>
		<td colspan = '10'>(Links must be of file type '.mp4')</td>
	 </tr>
      <tr>
        <td><input type="hidden" id="commentTXTvideo" name="commentTXTvideo" class="text-input" /></td>
      </tr>

      <tr>
        <td colspan="3"><input type="hidden" id="numTXTvideo" name="numTXTvideo"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="video" value="Upload" onClick = "showFileSize()" style = "visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="video" />
          <input type="hidden" id="liveapp_idvideo" name="liveapp_idvideo"  value=""/></td>
      </tr>
    </table>
  </form>
  <center><?php echo $pro;?></center>
</div>
<div id="text_form" style = "display:none">
  <form name = "text" action = "https://www.launchliveapp.com/processportal.php" enctype="multipart/form-data" method = "POST">
    <table class="wizardTable">
      <tr>
        <td><input type="hidden" id="commentTXTtext" name="commentTXTtext" class="text-input" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTtext" name="numTXTtext"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="text" value="Save" style="visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="header" />
          <input type="hidden" id="liveapp_idtext" name="liveapp_idtext"  value=""/></td>
      </tr>
    </table>
  </form>
</div>
<div id="html_form" style = "display:none">
  <form name = "html" action = "processportal.php" enctype="multipart/form-data" method = "POST">
    <table class="wizardTable">
      <tr>
        <td><input type="hidden" id="commentTXThtml" name="commentTXThtml" class="text-input" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXThtml" name="numTXThtml"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="html" value="Save" style="visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="html" />
          <input type="hidden" id="liveapp_idhtml" name="liveapp_idhtml"  value=""/></td>
      </tr>
    </table>
  </form>
</div>
<div id="maps_form" style = "display:none">
  <form method="post"  action="https://www.launchliveapp.com/processportal.php">
    <table  width="250px">
      <tr>
        <td colspan="4"><b><u>Add Maps</u></b></td>
      </tr>
      <tr>
        <td>Name:</td>
        <td colspan="3"><input type="text" id="nameTXTmaps" name="nameTXTmaps" tabindex="1" /></td>
      </tr>
      <tr>
        <td>Street:</td>
        <td colspan="3"><input type="text" id="streetTXTmaps" name="streetTXTmaps" tabindex="2" /></td>
      </tr>
      <tr>
        <td>City:</td>
        <td colspan="3"><input type="text" id="cityTXTmaps" name="cityTXTmaps" tabindex="4" /></td>
      </tr>
      <tr>
        <td>Postal Code / Zip:</td>
        <td colspan="3"><input type="text" id="postalTXTmaps" name="postalTXTmaps"tabindex="5" /></td>
      </tr>
      <tr>
        <td>Province / State:</td>
        <td colspan="3"><input type="text" id="provinceTXTmaps" name="provinceTXTmaps" tabindex="6" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTmaps" name="numTXTmaps" tabindex="16" value="" /></td>
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="commentTXTmaps" name="commentTXTmaps" tabindex="16" value="" /></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="maps" value="Save" style="visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="maps" />
          <input type="hidden" id="liveapp_idmaps" name="liveapp_idmaps"  value=""/></td>
      </tr>
    </table>
  </form>
</div>

<div id = "photogallery_form" style = "display:none">
	<form name = "photogallery" action = "https://www.launchliveapp.com/processportal.php">
    <table width="250px" class='wizardTable'>
      <tr>
		<td align="right">Interactive</td>
        <td align="left" style=""><input type="checkbox" id="interactive" name="interactive" checked="true" onChange = "galleryType('none');"/></td>
	  </tr>
	  <tr>
		<td align = "right">Title Bar</td>
		<td align = "left"><input type="checkbox" id="titlebar" name="titlebar" checked="true" onChange = "toggleTitleBar('image');"/></td>
	  </tr>
	  <tr>
		<td align = "right">Randomize Order</td>
		<td align = "left"><input type="checkbox" id="randompg" name="randompg" checked="true" /></td>
	  </tr>
	  <tr class = "interactive">
		<td align="right">Autoplay</td>
		<td align="left" style="padding-left:3px;">
			<select id="speedDDL" name="speedDDL" style="width:120px" onChange="">
				<option value="off">Off</option>
				<option value="3 seconds">3 seconds</option>
				<option value="5 seconds">5 seconds</option>
				<option value="10 seconds">10 seconds</option>
			</select>
		</td>
	  </tr>
	  <tr class = "nointeractive">
		<td align="right">Frequency</td>
		<td align="left" style="padding-left:10px;">
			<select id="freqDDL" name="freqDDL" style="width:120px" onChange="">
				<option value="Hourly">Hourly</option>
				<option value="Daily">Daily</option>
				<option value="Weekly">Weekly</option>
				<option value="Monthly">Monthly</option>
			</select>
		</td>
	  </tr>
	  <tr>
		<td colspan="6"><br/><input type="button" id="addImagesBTN" name="addImagesBTN" value="Add Images" /></td>
	  </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTpg" name="numTXTpg"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="photogallery" value="Save" style="visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="photogallery" />
          <input type="hidden" id="liveapp_idpg" name="liveapp_idpg"  value=""/></td>
      </tr>
    </table>
  </form>
</div>

<div id = "videogallery_form" style = "display:none">
	<form name = "videogallery" action = "https://www.launchliveapp.com/processportal.php">
    <table width="250px" class='wizardTable'>
      <tr>
		<td align="right">Interactive</td>
        <td align="left" style="width:160px"  ><input type="checkbox" id="interactivevg" name="interactivevg" checked="true" onChange = "galleryType('none');"/></td>
	  </tr>
	  <tr>
		<td align = "right">Title Bar</td>
		<td align = "left"><input type="checkbox" id="titlebarvg" name="titlebarvg" checked="true" onChange = "toggleTitleBar('video');"/></td>
	  </tr>
	  <tr>
		<td align = "right">Randomize Order</td>
		<td align = "left"><input type="checkbox" id="randomvg" name="randomvg" checked="true" /></td>
	  </tr>
	  <tr>
		<td colspan="3"><br/><input type="button" id="addVideosBTN" name="addVideosBTN" value="Add Videos" /></td>
	  </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTvg" name="numTXTvg"  class="text-input"/></td>
      </tr>
      <tr>
        <td ><input type="submit" id="videogallery" value="Save" style="visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="videogallery" />
          <input type="hidden" id="liveapp_idvg" name="liveapp_idvg"  value=""/></td>
      </tr>
    </table>
  </form>
</div>
<div id="seperator_form" style = "display:none">
	<form name="seperator" action ="https://www.launchliveapp.com/processportal.php" enctype="multipart/form-data" method="POST">
		<table class="wizardTable">
			<tr>
				<td><input type="hidden" id="commentTXTseperator" name="commentTXTseperator" class="text-input" /></td>
			</tr>
			<tr>
				<td colspan="3"><input type="hidden" id="numTXTseperator" name="numTXTseperator"  class="text-input"/></td>
			</tr>
			<tr>
				<td colspan="6"><input type="submit" id="seperator" value="Save" style="visibility:hidden"/>
					<input type="hidden" id="formTXT" name="formTXT" value="seperator" />
					<input type="hidden" id="liveapp_idseperator" name="liveapp_idseperator"  value=""/>
				</td>
			</tr>
		</table>
	</form>
</div>
<div id="findme_form" style = "display:none">
   <form name="findme" action="https://www.launchliveapp.com/processportal.php" method="post" enctype="multipart/form-data" target="uploadframefindme" onSubmit="return uploadfindme(this)">
    <table class="wizardTable">
      <tr>
        <td>Upload Image:</td>
        <td><input type="file" id="uploadedfilefindme" name="uploadedfilefindme" tabindex="1"  onChange = "instantuploadfindme()" />
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTfindme" name="numTXTfindme"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="findme" value="Upload" onClick = "showFileSize()" style = "visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="findme" />
          <input type="hidden" id="liveapp_idfindme" name="liveapp_idfindme"  value=""/></td>
		  <iframe id="uploadframefindme" name="uploadframefindme" src="https://www.launchliveapp.com/processportal.php" width="8" height="8" scrolling="no" frameborder="0"></iframe>
      </tr>
    </table>
  </form>
</div>

<div id="vcard_form" style = "display:none">
   <form name="vcard" action="https://www.launchliveapp.com/processportal.php" method="post" enctype="multipart/form-data" target="uploadframevcard" onSubmit="return uploadvcard(this)">
    <table class="wizardTable">
      <tr>
        <td>First Name:</td>
        <td><input type="text" id="firstname" name="firstname" tabindex="1"/>
      </tr>
      <tr>
        <td>Last Name:</td>
        <td><input type="text" id="lastname" name="lastname" tabindex="1"/>
      </tr>
      <tr>
        <td>Company:</td>
        <td><input type="text" id="company" name="company" tabindex="1" />
      </tr>
      <tr>
        <td>Mobile #:</td>
        <td><input type="text" id="mobile" name="mobile" tabindex="1" />
      </tr>
      <tr>
        <td>Work #:</td>
        <td><input type="text" id="work" name="work" tabindex="1" />
      </tr>
      <tr>
        <td>Home #:</td>
        <td><input type="text" id="home" name="home" tabindex="1" />
      </tr>
      <tr>
        <td>Email:</td>
        <td><input type="text" id="vemail" name = "email" tabindex="1" />
      </tr>
      <tr>
        <td>Home Address:</td>
        <td><input type="text" id="homeadd" name="homeadd" tabindex="1" />
      </tr>
      <tr>
        <td>Work Address:</td>
        <td><input type="text" id="workadd" name="workadd" tabindex="1" />
      </tr>
      <tr>
        <td>Upload Image:</td>
        <td><input type="file" id="uploadedfilevcard" name="uploadedfilevcard" tabindex="1"  onChange = "instantuploadvcard()" />
      </tr>
      <tr>
        <td colspan="3"><input type="hidden" id="numTXTvcard" name="numTXTvcard"  class="text-input"/></td>
      </tr>
      <tr>
        <td colspan="6"><input type="submit" id="vcard" value="Upload" onClick = "showFileSize()" style = "visibility:hidden"/>
          <input type="hidden" id="formTXT" name="formTXT" value="vcard" />
          <input type="hidden" id="liveapp_idvcard" name="liveapp_idvcard"  value=""/></td>
		  <input type = "hidden" id = "imgbold" name = "bold" value = 'off'/>
		  <input type = "hidden" id = "imgunderline" name = "underline" value = 'off'/>
		  <input type = "hidden" id = "imgitalic" name = "italic" value = 'off'/>
		  <input type = "hidden" id = "imgcolor" name = "color" value = 'off'/>
		  <input type = "hidden" id = "imgfont" name = "font" value = 'off'/>
		  <input type = "hidden" id = "imgsize" name = "size" value = 'off'/>
		  <iframe id="uploadframevcard" name="uploadframevcard" src="https://www.launchliveapp.com/processportal.php" width="8" height="8" scrolling="no" frameborder="0"></iframe>
      </tr>
    </table>
  </form>
</div>

<div id = "filebrowser_form" style = "display:none">
<form name = "filebrowser" action = "https://www.launchliveapp.com/processportal.php" method="POST" target="uploadframefd" onSubmit="return uploadfilebrowser(this)">
		<table class='wizardTable' style = "width:100%; white-space:nowrap;">
			<tr><td></br></td></tr>
			<tr>
				<td colspan="2">
					<label>Choose existing directory: </label>
				</td>
				<td colspan="1">
					<select type="dropdown" name="dirSelect" id="dirSelect" style = "width:100%">
						<option value="default">default</option>
					</select>
					<script>
						<?php
						$path = "./users-folder/".$appname."/liveapp/FTP";
						$dirArray = array();
						$js_array;
						if(is_dir($path)){
							$result = scandir($path);
							foreach($result as $dir){
								if($dir === '.' or $dir === '..'){
									continue;
								}
								if(is_dir($path.'/'.$dir)){
									array_push($dirArray, $dir);
								}
							}
							$js_array = json_encode($dirArray);
							echo "var dirArray = ".$js_array.";\n";
							?>
							var select = document.getElementById('dirSelect');
							for(var i = 1; i < dirArray.length; i++){
								var option = document.createElement('option');
								option.innerHTML = dirArray[i];
								option.value = dirArray[i];
								select.add(option);
							}
							<?php
						}
						?>
					</script>
				</td>
			</tr>
			<tr>
				<td colspan="2"  style = "text-align: center;">
					--OR--
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label>Specify new folder name: </label>
				</td>
				<td colspan="1">
					<input type="text" id="newFolder" name="newFolder" style = "width:calc(100% - 4px);">
				</td>
			</tr>
			<tr><td></br></td></tr>
			<tr>
				<td colspan="2">
					<label>Specify file browser height: </label>
				</td>
				<td colspan="1"  style = "vertical-align:top;">
					<input type="text" id="fbHeight" name="fbHeight" placeholder="100%" style = "width:calc(100% - 4px);">
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<span id="fbHeight_error" style= "color:red;"></span> (valid units: %, px, vh, vw)
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" id="filebrowser" value="Save" style = "visibility:hidden" />
					<input type="hidden" id="formTXT" name="formTXT" value="filebrowser" />
					<input type="hidden" id="userId" name="userId" value="<?php echo $userId; ?>" />
					<input type="hidden" id="liveapp_idfd" name="liveapp_idfd"  value=""/>
					<iframe id="uploadframefd" name="uploadframefd" src="https://www.launchliveapp.com/processportal.php" width="1" height="1" scrolling="no" frameborder="0"></iframe>
				</td>
			</tr>
		</table>
	</form>
</div>

<div id = "text_formatting" style="overflow:hidden; white-space:nowrap">
	<form>
    	<div id="text_formatting_div" style="height:31px; awidth:380px; position:relative;amargin-top: 3px; amargin-left:3px; ">
			<div id = "textedit"  onClick="doEdit(editdelete)" style = "width:320px;float:right">
				<div style="float:left;">
					<select id="fontDDL" name="fontDDL" style = "width:120px"  onChange = "textFont();">
						<option value="Arial">Arial</option>
						<option value="Verdana">Verdana</option>
						<option value="Georgia">Georgia</option>
						<option value="Times New Roman">Times New Roman</option>
						<option value="Impact">Impact</option>
					</select>
				</select>
				</div>
            	<!--div style="position:relative; width:300px;"-->
                <div style="float:left; margin-left:4px;">
					<img src="https://www.launchliveapp.com/texteditorimages/sml_up.png" alt="Home" border="0" onClick = "textSmallerfunc()" id = "textSmaller">
				</div>
                <div style="float:left;"><img src="https://www.launchliveapp.com/texteditorimages/lrg_up.png" alt="Home" border="0" onClick = "textLargerfunc()" id = "textLarger"></div>
                <div style="float:left;">
					<input type = "hidden" id = "textSize" value = "30" />
					<img src="https://www.launchliveapp.com/texteditorimages/textcolor_up.png" alt="Home" border="0" onClick="pickerOpen('text')" id = "textButton">
				</div>
                <div style="float:left;">
					<img src="https://www.launchliveapp.com/texteditorimages/bgcolor_up.png" alt="Home" border="0" onClick="pickerOpen('bg')" id = "bgButton">
				</div>
                <div style="float:left;">
					<img src="https://www.launchliveapp.com/texteditorimages/bold_up.png" alt="Home" border="0" onClick="textBoldfunc();" id = "boldButton">
                </div>
                <div style="float:left;">
					<img src="https://www.launchliveapp.com/texteditorimages/italic_up.png" alt="Home" border="0" onClick="textItalicfunc();" id = "italicButton">
				</div>
				<div style="float:left;">
					<img src="https://www.launchliveapp.com/texteditorimages/underline_up.png" alt="Home" border="0" onClick="textUnderlinefunc()" id = "underlineButton" >
				</div>
				<div style="float:left;">
					<img src="https://www.launchliveapp.com/texteditorimages/TextBox-Normal.png" alt="Home" border="0" onClick="toggleText()" id = "texttogglebutton" >
				</div>
				 <!-- Buttons to adjust height of separator bar -->
				<input type = "hidden" id = "barSize" value = "50" />
				<div style="float:left; margin-left:4px;">
					<img src="https://www.launchliveapp.com/texteditorimages/sml_up.png" alt="Home" border="0" onClick = "barSmallerFunc()" id = "barSmaller">
				</div>
				<div style="float:left;"><img src="https://www.launchliveapp.com/texteditorimages/lrg_up.png" alt="Home" border="0" onClick = "barLargerFunc()" id = "barLarger"></div>
					<!-- Drop down list to adjust separator bar width -->
				<div style="float:left;" >
					<select id="barWidthDDL" name="barWidthDDL" style = "width:120px"  onChange = "barWidth();" value="100">
						<option value="100">100%</option>
						<option value="90">90%</option>
						<option value="75">75%</option>
						<option value="60">60%</option>
						<option value="50">50%</option>
					</select>
				</div>
					<!-- Button to toggle showing the separator bar -->
				<div style="float:left;">
					<img src="https://www.launchliveapp.com/texteditorimages/bold_up.png" alt="Home" border="0" onClick="updateBar()" id = "showBarButton">
				</div>
			</div>
			<div class="closeItemBox" onClick="removeItem(deleteedit)"></div>
			<div class="addColumnBox" onClick="addColumn(deleteedit)" title="Add a Column"></div>
			<div class="editItemBox" onClick="doEdit(editdelete)"></div>
			<input type="submit" value="Save" tabindex="2" id = "textedit" style = "visibility:hidden; display:none"/>
			<input type="hidden" value="" tabindex="2" id = "textid"/>
			<input type = "hidden" id = "textUnderline"  value = "off" />
			<input type = "hidden" id = "textItalic"  value = "off" />
			<input type = "hidden" id = "textBold" value = "off" />
			<input type = "hidden" id = "hideBar" value = "off" />
			<input type = "hidden" id = "toggleTextForm" value = "off" />
            <input type="text" id="txtcolorTXT" name="txtcolorTXT" readonly="readonly"  style="visibility:hidden;display:none" onChange="textColor('text')"/>
            <input type="text" id="bgcolorTXT" name="bgcolorTXT" readonly="readonly" style="visibility:hidden;display:none;" onChange="textColor('bg')"/>
		</div>
	</form>
</div>
<div id = "pagesettings" style = "text-align:center;font-size:1.0em">
	<form>
		<input type = "text" name = "pagename" id = "pagename"  value = ""></input></br>
		<input type = "checkbox" name="hidepage" id = "hidepage" value="1"><span style='font-size:11px;'>Hide page in Menu?</span></input>
		<input type = "button" name = "savepage" id = "savepage" value = "Save" onClick = "savePage('popup');"></input>
	</form>
</div>
<div id="applyTemplateBox"></div>
<div id="uploadImage">
<div style="position:absolute; bottom:30px; width:300px; text-align:center;"><img src="portalGraphics/loadingBar.gif"/></div>
</div>



<div id="settingsHolder">
  <div id="settingsTop">
    <div id="closeBtn" onclick = "closeSettings()"></div>
  </div>
  <div id="settingsMiddle">
    <div id="settingsFrameHolder">
      <IFRAME SRC="" id="settingsFrame" FRAMEBORDER="0" width = "1000px">
      <p>Your browser does not support iframes.</p>
      </IFRAME>
    </div>
  </div>
  <div id="settingsBottom"></div>
</div>
<div id = "invisColorForm">
	<form id="publishApp" method="post"  action="processimages.php" >
		<input type = "submit" onclick="recompilingNotice()" id = "submitpublish" value = "submit" style = "visibility:hidden"/>
	</form>
</div>
<div id="darker"></div>
<div id="popupHolder"></div>
</body>
</html>
