# Changes to be made #

### GENERAL (portal + processliveappXML) ###
------------------------------------------------------
Some variables don't have appropriate names which 
accurately represent the functioning of the variables.
These variables need appropriate and representative 
names for the sake of readability and maintainability.
-- completed for portal.php ---
------------------------------------------------------ 
Commenting style is inconsistent and comments are 
sparse. Providing a consistent style with an abundance
of comments will inevitably make the code more 
maintainable.
------------------------------------------------------
Indentation is inconsistent and makes the code more 
difficult to interpret. Proper indentation will make 
the code easier to understand and maintain.
-- completed for portal.php --
------------------------------------------------------