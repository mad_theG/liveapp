<?php
session_start();
ini_set('display_errors', 1);
error_reporting(0);
if (!isset($_SESSION['user_id']))
{
	header("location:index.php");
	session_destroy();
	exit;
}

include 'config.php';
include 'common/functions.php';
include 'processingWebApp/login.php';
include 'processingWebApp/miscfunctions.php';
include 'processingWebApp/processgallery.php';
include 'processingWebApp/findme.php';
include 'processingWebApp/vCard.php';
include 'processingWebApp/downloadFile.php';

include 'processingWebApp/search_includes.php';
$userId = $_SESSION['user_id'];
$folderUserName = $_SESSION['user_name'];
$autonew = 0;
$comment = '';

//Create objects here
//Temp
$loyalty = 0;
$miscfunctions = new miscfunctions($folderUserName, $userId);



if(isset($_GET['user_name']) && isset($_GET['user_id']))
{
	$autonew = 1;
	$userId = $_GET['user_id'];
	$folderUserName = $_GET['user_name'];
}
//for updating the device code
$version_query = "SELECT version FROM user_settings WHERE user_id = $userId";
$ver_result = mysql_query($version_query);
$version = mysql_fetch_array($ver_result);

$liveQuery = "SELECT * FROM user_settings WHERE user_id = '$userId'";
$liveResult = mysql_query($liveQuery);
$row2 = mysql_fetch_array($liveResult);
$iconChange = $row2['icon_change'];
$menuHide = $row2['menu_hide'];
$sub_version = $version['version'];

if($iconChange == 1){
	$sub_version = $sub_version + 1;
}

$app_version = '1.' . $sub_version;

// START FILE VERSION LOGIC
// get current file version
$fileQuery = "SELECT current_version FROM file_version WHERE user_id = $userId AND module_name = 'liveapp'";
$result = mysql_query($fileQuery);
$numberOfRows = mysql_num_rows($result);
$currentVersion = "";
$nextVersion = "";
$nameOfFile = "";
$prevVersion = "";

function url_get_contents ($Url) {
    if (!function_exists('curl_init')){
        die('CURL is not installed!');
    }
    $ch = curl_init();
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $Url);

    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function getURL($url, $includeContents = false)
{
	if($includeContents)
	{
		return @file_get_contents($url);
	}
	else
	{
		return (url_get_contents($url) !== false);
	}
}
/*
function getURL($url, $includeContents = false)
{
	if($includeContents)
	{
		return @file_get_contents($url);
	}
	else
	{
		return ( @file_get_contents($url, null, null, 0, 0) !== false);
	}
}*/

// Added by Zak (Nov 2014)
// Function to generate a call to the pageForward() function in javascript.
// This work-around ensures that the function always receives two valid parameters.
function getPageForwardCall($link)
{
	global $login; // determines whether the login widget is enabled for this app; '1' = enabled, '0' = disabled
	//global $pagesArray; // holds the IDs for every page in the app
	global $pagesLogin; // holds data for pages that require a login
	global $userId;

	$output = "";

	// If the app has a Login Widget, then pass on an extra parameter related to that.
	if ($login == 1)
	{
		// Don't let an invalid page number reach the MySQL query.
		// $link is actually the page that we're navigating to. This isn't always a number.
		// Some links specify it as a word, so make sure it's a valid number.
		if (is_numeric($link) && $link >= 1)
		{
			// Check if the page even uses the login widget.
			$pageLoginQuery = "SELECT requirelogin FROM pages WHERE user_id=$userId AND page_id=$link";
			$pageLoginResult = mysql_query($pageLoginQuery) or die("MySQL Error: " . mysql_error());
			$pageLoginRow = mysql_fetch_array($pageLoginResult);

			if ($pageLoginRow["requirelogin"] == 1)
			{
				$output = "pageForward(\"$link\", 1)";
			}
			else
			{
				// Because there's no login for this page, we send a '0' to the pageForward function.
				// This tells the script to just ignore user logins for now, and prevents a nasty error.
				$output = "pageForward(\"$link\", 0)";
			}
		}
		else
		{
			$output = "pageForward(\"$link\", 0)";
		}
	}
	else
	{
		// No login widget in this app, so pass in a '0' that tells the pageForward
		// function to ignore login checking.
		$output = "pageForward(\"$link\", 0)";
	}

	return $output;
}

if($numberOfRows > 0)
{
    while($row = mysql_fetch_array($result))
    {
        $currentVersion = $row["current_version"];
        $nextVersion = $currentVersion + 1;
        $prevVersion = $currentVersion - 1;
    }
}

$nameOfFile = "liveapp_current.html";


$empty = "HELLO!";
//Create useless file to upate manifest if no items have changed.
$handlefile = fopen("users-folder/$folderUserName/liveapp/".$nameOfFile,"w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
fputs($handlefile,$empty); // Write the contents of $config to the file opened in $handle
fclose($handlefile); // Close the opened file
//Features Select Query
$selectQuery = "SELECT * FROM features WHERE user_id = '$userId'";
$selectResult = mysql_query($selectQuery);
$row = mysql_fetch_array($selectResult);
//if($currentVersion == "0")
//{
    // first file

//}
////else
//{
    $oldFile = "users-folder/$folderUserName/liveapp/liveapp_current.html";
    $newFile = "users-folder/$folderUserName/liveapp/liveapp_v1.$prevVersion" . ".html";
    rename($oldFile,$newFile);
    $nameOfFile = "liveapp_current.html";
//}
$login = $row["login"];


//srsbg add app background image and color
$app_bg_query = "SELECT app_bg_image, appBgColor FROM user_settings WHERE user_id = $userId";
$app_bg_res = mysql_query($app_bg_query);
if(!$app_bg_res) {
	echo mysql_error();
} else {
	$app_bg_arr = mysql_fetch_array($app_bg_res);
	$app_bg_image = $app_bg_arr["app_bg_image"];
	$app_bg_color = $app_bg_arr["appBgColor"];
}

//PAGES ARRAY
$pagesHidden = array();
$pagesArray = array();
$pagesNames = array();
$pagesLogin = array();
$i = 0;
$selectQuery = "SELECT * FROM pages WHERE user_id = $userId ORDER BY page_order ASC";
$selectResult = mysql_query($selectQuery);

while($pages = mysql_fetch_array($selectResult))
{
	$pagesHidden[$i] = $pages["hidden"];
	$pagesLogin[$i] = $pages["requirelogin"]; // track if each page requires a login or not
	if($login == 0)
	{
		$pagesLogin[$i] = 0; // holds the loginwidget status for every page in the app
	}
	$pagesArray[$i] = $pages["page_id"]; // holds the IDs for every page in the app
	$pagesNames[$i] = $pages["page_name"]; // holds the names for every page in the app
	$i++;
}

$youtubeItems = array();
$youtubeIDs = array();

//var_dump($login);

if($login == 1)
{
	$type = "true";
	$selectQuery = "SELECT * FROM appusersettings WHERE user_id = $userId AND checkbox = 0";
	$selectResult = mysql_query($selectQuery);
	$row = mysql_fetch_array($selectResult);
	$loginmenu = $row["menu"];
	$loginver = $row["verification"];
	$logincaptcha = $row["captcha"];

	if (isset($row["input1"])) {
		$inputs[1] = $row['input1'];
		$inputs[2] = $row['input2'];
		$inputs[3] = $row['input3'];
		$inputs[4] = $row['input4'];
		$inputs[5] = $row['input5'];
		$inputs[6] = $row['input6'];
		$inputs[7] = $row['input7'];
	}

	$selectQuery = "SELECT * FROM appusersettings WHERE user_id = $userId AND checkbox = 1";
	$selectResult = mysql_query($selectQuery);
	$row = mysql_fetch_array($selectResult);

	if (isset($row["input1"])) {
		$inputr[1] = $row['input1'];
		$inputr[2] = $row['input2'];
		$inputr[3] = $row['input3'];
		$inputr[4] = $row['input4'];
		$inputr[5] = $row['input5'];
		$inputr[6] = $row['input6'];
		$inputr[7] = $row['input7'];
	}

}
else
{
	$loginver = '';
	$loginmenu = 0;
	$type = "false";
	$inputs[1] = "None";
	$inputs[2] = "None";
	$inputs[3] = "None";
	$inputs[4] = "None";
	$inputs[5] = "None";
	$inputs[6] = "None";
	$inputs[7] = "None";
	$inputr[1] = 0;
	$inputr[2] = 0;
	$inputr[3] = 0;
	$inputr[4] = 0;
	$inputr[5] = 0;
	$inputr[6] = 0;
	$inputr[7] = 0;
}

$loginobject = new login($folderUserName, $userId, $type, $loginmenu, $loginver,$menuHide);

$i = 0;
$selectQuery = "SELECT * FROM liveapp WHERE user_id = $userId AND type = 'youtube' ORDER BY page_number ASC, number ASC";
$selectResult = mysql_query($selectQuery);

while($youtube = mysql_fetch_array($selectResult))
{
	$youtubeItems[$i] = $youtube["text"];
	$youtubeIDs[$i] = $youtube["liveapp_id"];
	$i++;
}

//Checking for find me item + Create findme object if it exists

$isFindMe = false;
$selectQuery = "SELECT * FROM liveapp WHERE user_id = $userId AND type = 'findme' ORDER BY page_number ASC, number ASC";
$selectResult = mysql_query($selectQuery);
$numRows = mysql_num_rows($selectResult);

if($numRows > 0)
{
	$isFindMe = true;
	$findmeobj = new findMe($folderUserName, $userId);
	$findmeobj -> setJs();
}



$isVcard = false;
$selectQuery = "SELECT * FROM liveapp WHERE user_id = $userId AND type = 'vcard' ORDER BY page_number ASC, number ASC";
$selectResult = mysql_query($selectQuery);
$numRows = mysql_num_rows($selectResult);

if($numRows > 0)
{
	$isVcard = true;
	$vcardobj = new vCard($folderUserName, $userId);
	$vcardobj -> setJs();
}



// UPDATE File Versioning
$numberofyoutube = count($youtubeItems);
$numberofpages = count($pagesArray);
$updateQuery = "UPDATE file_version SET current_version = $nextVersion WHERE module_name = 'liveapp' AND user_id = $userId";


if(!mysql_query($updateQuery))
{
    die('Error: ' . mysql_error());
}

$cache = "CACHE MANIFEST
";
$cache .="\nNETWORK:";
$cache .="\n*";
$cache .= "\nhttp://www.launchliveapp.com/scripts/bookmark_bubble_bb10.js";
$cache .= "\nhttp://www.launchliveapp.com/common/example_bb10.js";
$cache .= "\nhttp://www.launchliveapp.com/scripts/bookmark_bubble_test.js";
$cache .= "\nhttp://www.launchliveapp.com/common/example.js";
$cache .="\nhttp://www.launchliveapp.com/images/BB10_download_screen.png";
$cache .="\nhttp://www.launchliveapp.com/images/BB10_download_screen_landscape.png";
$cache .="\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/appicon72.png";
$cache .="\nhttp://www.launchliveapp.com/newimages/gBubble_iPhone_vertical_ios6.png";
$cache .="\nhttp://www.launchliveapp.com/newimages/gBubble_iPhone_horizontal_ios6.png";
$cache .="\nhttp://www.launchliveapp.com/newimages/gBubble_iPhone_vertical_ios7.png";
$cache .="\nhttp://www.launchliveapp.com/newimages/gBubble_iPhone_horizontal_ios7.png";



if($userId == "776"){
$cache .= "\nhttp://memberservices.membee.com/feeds/scripts/events.js";
$cache .= "\nhttp://memberservices.membee.com/feeds/Service.ashx";
}

$cache .= "\nhttp://www.jeppii.com/images/upload/";

$cache .="\nhttp://www.jeppii.com/images/account/";

$cacheipad = "CACHE MANIFEST
";
$cacheipad .="\nNETWORK:";
$cacheipad .="\n*";
if($userId == "776"){
$cacheipad .= "\nhttp://memberservices.membee.com/feeds/scripts/events.js";
$cacheipad .= "\nhttp://memberservices.membee.com/feeds/Service.ashx";
}
$cacheipad .= "\nhttp://www.jeppii.com/images/upload/";

$cacheipad .="\nhttp://www.jeppii.com/images/account/";
$cacheipad .= "\nhttp://www.launchliveapp.com/scripts/bookmark_bubble_bb10.js";
$cacheipad .= "\nhttp://www.launchliveapp.com/common/example_bb10.js";
$cacheipad .= "\nhttp://www.launchliveapp.com/scripts/bookmark_bubble_test.js";
$cacheipad .= "\nhttp://www.launchliveapp.com/common/example.js";
$cacheipad .="\nhttp://www.launchliveapp.com/images/BB10_download_screen.png";
$cacheipad .="\nhttp://www.launchliveapp.com/images/BB10_download_screen_landscape.png";
$cacheipad .="\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/appicon72.png";
$cacheipad .="\nhttp://www.launchliveapp.com/newimages/gBubble_iPad_vertical_ios6.png";
$cacheipad .="\nhttp://www.launchliveapp.com/newimages/gBubble_iPad_horizontal_ios6.png";
$cacheipad .="\nhttp://www.launchliveapp.com/newimages/gBubble_iPad_vertical_ios7.png";
$cacheipad .="\nhttp://www.launchliveapp.com/newimages/gBubble_iPad_horizontal_ios7.png";

$cachebb = "CACHE MANIFEST
";
$cachebb .="\nNETWORK:";
//srsrs srsbg
$cachebb .= "\n*";

$cachebbtext = "CACHE MANIFEST
";
$cachebbtext .="\nNETWORK:";
//srsrs srsbg
$cachebbtext .= "\n*";

$cachebb1 = "CACHE MANIFEST
";
$cachebb1 .="\nNETWORK:";
//srsrs srsbg
$cachebb1 .= "\n*";

$cachebb1text = "CACHE MANIFEST
";
$cachebb1text .="\nNETWORK:";
//srsrs srsbg
$cachebb1text .= "\n*";

//audio counting
$audioCounter = 0; // used for when we're actually rendering out the audio jPlayers
$audioQuery ="SELECT * FROM liveapp WHERE user_id= '$userId' AND type='audio' ORDER BY number";
$audioResult = mysql_query($audioQuery);
$numberOfAudio = mysql_num_rows($audioResult);
$audioArray = array();

if($numberOfAudio !== 0)
{
	$aCounter = 0;
	while($row1 = mysql_fetch_array($audioResult))
	{
		$textinput = $row1['textinput'];
		$comment = $row1['comment'];
		if($textinput == "off")
		{
			$comment = '';
		}
		// Note: Names in the audioArray[] will be +1 of their index in the array. Ask Mikael if you don't get what I mean.
		$textinput = $row1['textinput'];
               $comment = $row1['comment'];
               if($textinput == "off")
               {
                       $comment = '';
               }


		$aCounter += 1;

		$audioArray[] = array(0 => "jquery_audio_".$aCounter, 1 => $row1['location'], 2 => $comment, 3 => "jp_acontainer_".$aCounter);
		$cache .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$row1['location']."";
		$cacheipad .= "\n".$row1['location']."";
		$cachebb .= "\n".$row1['location']."";

		$cachebb1 .= "\n".$row1['location']."";
	}
}

// Edited by Zak (September 2015)
// If the listVideos script is linked to the html widget's content, then don't cache it.
// This prevents the files displayed in the widget from being cached, it's mandatory!
if (strpos($comment, "listVideos") === false)
{
	// to cache links included in HTML section of app
	$cache .= "\n". SearchForIncludes::searchString($comment);
	$cacheipad .= "\n". SearchForIncludes::searchString($comment);
	$cachebb .= "\n". SearchForIncludes::searchString($comment);
	$cachebb1 .= "\n". SearchForIncludes::searchString($comment);
	$cachebb1text .= "\n". SearchForIncludes::searchString($comment);
	$cachebbtext .= "\n". SearchForIncludes::searchString($comment);
}

//video counting
$videoCount = 0; //used when we actually render out the video Players
$videoQuery ="SELECT * FROM liveapp WHERE user_id='$userId'  AND type='video' ORDER BY number";
$videoResult = mysql_query($videoQuery);
$numberOfVideo = mysql_num_rows($videoResult);

$videoArray = array();

if($numberOfVideo !== 0)
{
	$aCounter = 0;
	while($row1 = mysql_fetch_array($videoResult))
	{
		// Note: Names in the audioArray[] will be +1 of their index in the array. Ask Mikael if you don't get what I mean.
		$aCounter += 1;
		$videoArray[] = array(0 => "jquery_video_".$aCounter, 1 => $row1['location'], 2 => $row1['comment'], 3 => "jp_vcontainer_".$aCounter, 4 => $row1['url'], 5 => $row1["liveapp_id"]);
		if($row1['location'] == "" && $row1['url']!= "")
		{
			$cache .= "\n".$row1['url']."";
			$cacheipad .= "\n".$row1['url']."";
			$cachebb .= "\n".$row1['url']."";
			$cachebb1 .= "\n".$row1['url']."";
		}
		else
		{
			$cache .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$row1['location']."";
			$cacheipad .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$row1['location']."";
			$cachebb .= "\n".$row1['location']."";
			$cachebb1 .= "\n".$row1['location']."";
		}
	}
}

/* BEGINING of IMAGE GALLERY JAVASCRIPT ---------------------------------------------------------------------------------------------------------------------------------------
 -This javascript is inserted right AFTER the $(document).Ready method of each web app file.
 -Referenced with the php variable $imageGalleryJS
 */

$cache .= "\nCACHE:";
$cachebb .= "\nCACHE:";
$cachebb1 .= "\nCACHE:";
$cacheipad .= "\nCACHE:";
$cachebb1text .= "\nCACHE:";
$cachebbtext .= "\nCACHE:";

// Special font for Filmotechnic app
if($folderUserName == "Filmotechnic") {
$cache .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.eot";
$cacheipad .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.eot";
$cachebb .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.eot";
$cachebb1 .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.eot";
$cachebb1text .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.eot";
$cachebbtext .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.eot";

$cache .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.otf";
$cacheipad .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.otf";
$cachebb .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.otf";
$cachebb1 .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.otf";
$cachebb1text .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.otf";
$cachebbtext .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.otf";

$cache .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.woff";
$cacheipad .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.woff";
$cachebb .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.woff";
$cachebb1 .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.woff";
$cachebb1text .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.woff";
$cachebbtext .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.woff";

$cache .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.ttf";
$cacheipad .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.ttf";
$cachebb .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.ttf";
$cachebb1 .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.ttf";
$cachebb1text .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.ttf";
$cachebbtext .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.ttf";

$cache .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.svg";
$cacheipad .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.svg";
$cachebb .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.svg";
$cachebb1 .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.svg";
$cachebb1text .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.svg";
$cachebbtext .="\nhttps://www.launchliveapp.com/fonts/Handel%20Gothic.svg";
}

//srsbg
if($app_bg_image != "") {
$cache .="\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$app_bg_image;
$cacheipad .="\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$app_bg_image;
$cachebb .="\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$app_bg_image;
$cachebb1 .="\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$app_bg_image;
//$cachebb1text .= makeTextFile("\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$app_bg_image);
//$cachebbtext .= makeTextFile("\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$app_bg_image);
$cachebb1text .= makeTextFile("\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$app_bg_image);
$cachebbtext .= makeTextFile("\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$app_bg_image);
}
$imageGalleryQuery ="SELECT * FROM liveapp WHERE user_id='$userId'  AND type='photogallery' ORDER BY number";
$imageGalleryResult = mysql_query($imageGalleryQuery);
$numOfImageGallery = mysql_num_rows($imageGalleryResult);
if($numOfImageGallery > 0)
{
	$photogallery = new photoGallery($folderUserName, $userId);
	$photogallery -> setJs($userId, $folderUserName);

	$cache .= $photogallery -> getRegCache();
	$cacheipad .= $photogallery -> getRegCache();
	$cachebb1 .= $photogallery -> getCache();
	$cachebb1text .= $photogallery -> getTextCache();
	$cachebbtext .= $photogallery -> getTextCache();
	$galleryheight = $photogallery -> getHeightArray();
}

$videoGalleryQuery ="SELECT * FROM liveapp WHERE user_id='$userId'  AND type = 'videogallery' ORDER BY number";
$videoGalleryResult = mysql_query($videoGalleryQuery);
$numOfVideoGallery = mysql_num_rows($videoGalleryResult);
if($numOfVideoGallery > 0)
{
	$videogallery = new videoGallery($folderUserName, $userId);
	$videogallery -> setJs($userId, $folderUserName);
	$cache .= $videogallery -> getRegCache();
	$cacheipad .= $videogallery -> getRegCache();
	$cachebb1 .= $videogallery -> getCache();
	$cachebb1text.= $videogallery -> getTextCache();
	$cachebbtext .= $photogallery -> getTextCache();
}
//end of audio/video checking
$filename = "users-folder/$folderUserName/liveapp/$nameOfFile";
// END OF FILE VERSION LOGIC

function js_array($array)
{
    $temp = array_map("js_str", $array);
    return "[" . implode(",", $temp) . "]";
}


$cache .= "\nliveapp_current.php";
$cache .= "\nqrcode.jpg";
$cache .= "\nhttp://www.launchliveapp.com/liveappmenucss/themes/liveapp.css";
$cache .= "\nhttp://www.launchliveapp.com/liveappmenucss/jquery.mobile.structure-1.0.1.min.css";
$cache .= "\nhttp://www.launchliveapp.com/liveappmenucss/themes/images/ajax-loader.png";
$cache .= "\n../../../testskins/jplayer.blue.monday.css";
$cache .="\n../../../testskins/LiveAppJplayer.png";
$cache .= "\n../../../testskins/jplayer.blue.monday.video.play.png";
$cache .= "\nhttp://code.jquery.com/jquery-1.6.4.min.js";
$cache .= "\nhttp://code.jquery.com/mobile/1.0/jquery.mobile-1.0.min.js";
$cache .="\n../../../js/jquery.jplayer.min.js";
$cache .="\nhttp://www.launchliveapp.com/Logo.png";
$cache .="\nhttp://www.launchliveapp.com/images/playbutton.jpg";
$cache .= "\n../../../testskins/jplayer_bg.jpg";
$cache .= "\n../../../testskins/jplayer_titleBG.jpg";
$cache .= "\n../../../scripts/jquery.validate.js";
$cache .= "\n../../../scripts/jquery-ui-1.8.22.custom.min.js";
$cache .= "\n../../../scripts/jquery-ui-1.8.21.custom.css";
$cache .= "\nhttps://connect.facebook.net/en_US/all.js#xfbml=1&appId=396518167046244";
$cache .= "\nhttp://www.launchliveapp.com/images/loading_iPhone.png";
$cache .="\nhttp://fonts.googleapis.com/css?family=Droid+Sans:400,700";
//$cache .="\nhttp://www.launchliveapp.com/images/MealBG.png";


if($userId == "776"){

$cache .= "\nhttp://www.cambridgechamber.com/_graphics/icon-upcoming_events.gif";
$cache .= "\nhttp://www.cambridgechamber.com/images/date-img.jpg";
$cache .= "\nhttp://www.cambridgechamber.com/images/directory-search.png";
  $cache .= "\nhttp://www.cambridgechamber.com/images/linkedin-logo_002.jpg";
$cache .= "\nhttp://www.cambridgechamber.com/images/social-me-vimeo-logo.jpg";
$cache .= "\nhttp://www.cambridgechamber.com/images/facebook_logo_002.jpg";
$cache .= "\nhttp://www.cambridgechamber.com/images/grey-m_003.jpg";


}



// ZAC
$cache .= "\n../../../portalFunctionsTest/linkedin.js";

$cache .= "\n../../../images/linkedin_profile_button.png";

// Mikael - New share menu page
$cache .= "\n../../../images/facebook_share.png";
$cache .= "\n../../../images/twitter_share.png";
$cache .= "\n../../../images/email.png";
$cache .= "\n../../../images/qr_bg.png";

//For the new about page 6.29.2012
$cache .= "\nhttp://www.launchliveapp.com/images/about_logo.png";
$cache .= "\nhttp://www.launchliveapp.com/images/about_top.png";
$cache .= "\nhttp://www.launchliveapp.com/images/about_bg.png";
$cache .= "\nhttp://www.launchliveapp.com/images/about_bottom.png";
$cache .=  "\n../../../jquerymobile/datebox.css";
$cache .= "\n../../../jquerymobile/datebox.js";
$cache .=  "\n../../../jquerymobile/dialog.js";

$cache .="\nhttp://www.launchliveapp.com/images/loading_iPhone_Landscape.png";
$cache .="\nhttp://www.launchliveapp.com/images/loading_iPhone.png";

// iPad cache manifest

$cacheipad .= "\nliveapp_currentipad.php";
$cacheipad .= "\nqrcode.jpg";
$cacheipad .= "\nhttp://www.launchliveapp.com/liveappmenucss/themes/liveapp.css";
$cacheipad .= "\nhttp://www.launchliveapp.com/liveappmenucss/jquery.mobile.structure-1.0.1.min.css";
$cacheipad .= "\nhttp://www.launchliveapp.com/liveappmenucss/themes/images/ajax-loader.png";
$cacheipad .= "\n../../../testskins/jplayer.blue.monday.css";
$cacheipad .="\n../../../testskins/LiveAppJplayer.png";
$cacheipad .= "\n../../../testskins/jplayer.blue.monday.video.play.png";
$cacheipad .= "\nhttp://code.jquery.com/jquery-1.6.4.min.js";
$cacheipad .= "\nhttp://code.jquery.com/mobile/1.0/jquery.mobile-1.0.min.js";
$cacheipad .="\n../../../js/jquery.jplayer.min.js";
$cacheipad .="\nhttp://www.launchliveapp.com/Logo.png";
$cacheipad .="\nhttp://www.launchliveapp.com/images/loading_ipad1.png";
$cacheipad .="\nhttp://www.launchliveapp.com/images/loading_ipad2.png";
$cacheipad .="\nhttp://www.launchliveapp.com/images/playbutton.jpg";
$cacheipad .="\n../../../testskins/jplayer_bg.jpg";
$cacheipad .="\n../../../testskins/jplayer_titleBG.jpg";
$cacheipad .= "\n../../../scripts/jquery.validate.js";
$cacheipad .= "\n../../../scripts/jquery-ui-1.8.22.custom.min.js";
$cacheipad .= "\n../../../scripts/jquery-ui-1.8.21.custom.css";
// ZAC
$cacheipad .= "\n../../../portalFunctionsTest/linkedin.js";

$cacheipad .= "\n../../../images/linkedin_profile_button.png";

$cacheipad .="\nhttp://fonts.googleapis.com/css?family=Droid+Sans:400,700";

if($userId == "776"){
  $cacheipad .= "\nhttp://memberservices.membee.com/feeds/scripts/events.js";
$cacheipad .= "\nhttp://www.cambridgechamber.com/_graphics/icon-upcoming_events.gif";
$cacheipad .= "\nhttp://www.cambridgechamber.com/images/date-img.jpg";
$cacheipad .= "\nhttp://www.cambridgechamber.com/images/directory-search.png";
  $cacheipad .= "\nhttp://www.cambridgechamber.com/images/linkedin-logo_002.jpg";
$cacheipad .= "\nhttp://www.cambridgechamber.com/images/social-me-vimeo-logo.jpg";
$cacheipad .= "\nhttp://www.cambridgechamber.com/images/facebook_logo_002.jpg";
$cacheipad .= "\nhttp://www.cambridgechamber.com/images/grey-m_003.jpg";


}

// MIKAEL - new share page
$cacheipad .= "\n../../../images/facebook_share.png";
$cacheipad .= "\n../../../images/twitter_share.png";
$cacheipad .= "\n../../../images/email.png";
$cacheipad .= "\n../../../images/qr_bg.png";

$cacheipad .=  "\n../../../jquerymobile/datebox.css";
$cacheipad .= "\n../../../jquerymobile/datebox.js";
$cacheipad .=  "\n../../../jquerymobile/dialog.js";

//For the new about page 6.29.2012
$cacheipad .= "\nhttp://www.launchliveapp.com/images/about_logo.png";
$cacheipad .= "\nhttp://www.launchliveapp.com/images/about_top.png";
$cacheipad .= "\nhttp://www.launchliveapp.com/images/about_bg.png";
$cacheipad .= "\nhttp://www.launchliveapp.com/images/about_bottom.png";
$cacheipad .= "\nhttps://connect.facebook.net/en_US/all.js#xfbml=1&appId=396518167046244";
//$cacheipad .="\nhttp://www.launchliveapp.com/images/MealBG.png";
//Start cache bb----------------------------------------->
$cachebb .= "\nCACHE:";


$cachebb .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/qrcode.jpg";
$cachebb .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/qr.html";
$cachebb .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/liveappmenu.html";
$cachebb .= "\nhttp://www.launchliveapp.com/liveappmenucss/themes/liveapp.css";
$cachebb .= "\nhttp://www.launchliveapp.com/liveappmenucss/jquery.mobile.structure-1.0.1.min.css";
$cachebb .= "\nhttp://www.launchliveapp.com/testskins/jplayer.blue.monday.css";
$cachebb .="\nhttp://www.launchliveapp.com/testskins/LiveAppJplayer.png";
$cachebb .= "\nhttp://www.launchliveapp.com/testskins/jplayer.blue.monday.video.play.png";
$cachebb .= "\nhttp://www.launchliveapp.com/testskins/jplayer_bg.jpg";
$cachebb .= "\nhttp://www.launchliveapp.com/testskins/jplayer_titleBG.jpg";
$cachebb .= "\nhttp://www.launchliveapp.com/scripts/jquery-1.6.4.min.js";
$cachebb .= "\nhttp://www.launchliveapp.com/scripts/jquery.mobile-1.0.1.min.js";
$cachebb .="\nhttp://www.launchliveapp.com/js/jquery.jplayer.min.js";
$cachebb .="\nhttp://www.launchliveapp.com/Logo.png";
$cachebb .="\nhttp://www.launchliveapp.com/images/playbutton.jpg";
// ZAC
$cachebb .= "\nhttp://www.launchliveapp.com/portalFunctionsTest/linkedin.js";

$cachebb .= "\nhttp://www.launchliveapp.com/images/linkedin_profile_button.png";


// blackberry 6.0 cache


$cachebb1 .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/qrcode.jpg";
/*
$cachebb1 .= "\nhttp://www.launchliveapp.com/liveappmenucss/themes/liveapp.css";
$cachebb1 .= "\nhttp://www.launchliveapp.com/liveappmenucss/jquery.mobile.structure-1.0.1.min.css";
$cachebb1 .= "\nhttp://www.launchliveapp.com/testskins/jplayer.blue.monday.css";
$cachebb1 .="\nhttp://www.launchliveapp.com/testskins/LiveAppJplayer.png";
$cachebb1 .= "\nhttp://www.launchliveapp.com/testskins/jplayer.blue.monday.video.play.png";
$cachebb1 .= "\nhttp://www.launchliveapp.com/testskins/jplayer_bg.jpg";
$cachebb1 .= "\nhttp://www.launchliveapp.com/testskins/jplayer_titleBG.jpg";
$cachebb1 .= "\nhttp://www.launchliveapp.com/scripts/jquery-1.6.4.min.js";
$cachebb1 .= "\nhttp://www.launchliveapp.com/scripts/jquery.mobile-1.0.1.min.js";
$cachebb1 .="\nhttp://www.launchliveapp.com/js/jquery.jplayer.min.js";
$cachebb1 .="\nhttp://www.launchliveapp.com/Logo.png";
$cachebb1 .="\nhttp://www.launchliveapp.com/images/playbutton.jpg";
$cachebb1 .= "\nhttp://www.launchliveapp.com/scripts/jquery.validate.js";
// ZAC
$cachebb1 .= "\nhttp://www.launchliveapp.com/portalFunctionsTest/linkedin.js";

$cachebb1 .= "\nhttp://www.launchliveapp.com/images/linkedin_profile_button.png";
*/






// For the new share page 6.29.2012
$cachebb1text .= "\nhttp://www.launchliveapp.com/images/facebook_share.png.txt";
$cachebb1text .= "\nhttp://www.launchliveapp.com/images/twitter_share.png.txt";
$cachebb1text .= "\nhttp://www.launchliveapp.com/images/email.png.txt";
$cachebb1text .= "\nhttp://www.launchliveapp.com/images/qr_bg.png.txt";

//For the new about page 6.29.2012
$cachebb1text .= "\nhttp://www.launchliveapp.com/images/about_logo.png.txt";
$cachebb1text .= "\nhttp://www.launchliveapp.com/images/about_top.png.txt";
$cachebb1text .= "\nhttp://www.launchliveapp.com/images/about_bg.png.txt";
$cachebb1text .= "\nhttp://www.launchliveapp.com/images/about_bottom.png.txt";
$cachebb1text .= makeTextFile("users-folder/".$folderUserName."/liveapp/qrcode.jpg");
$cachebb1text .="\nhttp://www.launchliveapp.com/scripts/jquery-ui-1.8.22.custom.min.js";
$cachebb1text .="\nhttp://www.launchliveapp.com/scripts/jquery-ui-1.8.21.custom.css";

$cachebbtext = $cachebb1text;
//For code versions previous to 7-19-2012
//adds all of the resources to the manifest for the older blackberry code versions
$cachebb1text .= "\nhttp://www.launchliveapp.com/liveappmenucss/themes/liveapp.css";
$cachebb1text .= "\nhttp://www.launchliveapp.com/liveappmenucss/jquery.mobile.structure-1.0.1.min.css";
$cachebb1text .= "\nhttp://www.launchliveapp.com/testskins/jplayer.blue.monday.css";
$cachebb1text .= "\nhttp://www.launchliveapp.com/scripts/jquery-1.6.4.min.js";
$cachebb1text .= "\nhttp://www.launchliveapp.com/scripts/jquery.mobile-1.0.1.min.js";
$cachebb1text .="\nhttp://www.launchliveapp.com/js/jquery.jplayer.min.js";


$cachebb1text .="\nhttp://www.launchliveapp.com/testskins/LiveAppJplayer.png.txt";
$cachebb1text .="\nhttp://www.launchliveapp.com/testskins/jplayer.blue.monday.video.play.png.txt";
$cachebb1text .="\nhttp://www.launchliveapp.com/testskins/jplayer_bg.jpg.txt";
$cachebb1text .="\nhttp://www.launchliveapp.com/testskins/jplayer_titleBG.jpg.txt";
$cachebb1text .="\nhttp://www.launchliveapp.com/Logo.png.txt";
$cachebb1text .="\nhttp://www.launchliveapp.com/images/playbutton.jpg.txt";

$config =
'
<?php
	session_start();
	$email = "";
	if(isset($_SESSION["email"]))
	{
		$email = $_SESSION["email"];
	}

	$app_user_id = "";
	if(isset($_SESSION["app_user_id"]))
	{
		$app_user_id = $_SESSION["app_user_id"];
	}

	$key = "";
	if(isset($_SESSION["key"]))
	{
		$key = $_SESSION["key"];
	}
	$message = "";

	if(isset($_GET["message"]))
	{
		{
			$message = $_GET["message"];
		}
	}
	if(isset($_GET["post_id"]))
	{
		header("Location:http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/liveapp_current.php");
	}
	include("../../../tweetLoader3.php");
?>

<!DOCTYPE html>
<html  manifest='.$folderUserName.'.manifest>
<head>
<title>'.$folderUserName.'</title>


<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=0;" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<META HTTP-EQUIV="expires" CONTENT="Wed, 19 Feb 2003 08:00:00 GMT">
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<link rel="apple-touch-icon-precomposed" href="appicon72.png" />

<!--<link rel="apple-touch-startup-image" href="../../../images/loading_iPhone.png" media="screen and (max-device-width: 320px)" />-->
<!-- iPhone -->
<link href="../../../images/apple-touch-startup-image-320x460.png"
      media="(device-width: 320px) and (device-height: 480px)
         and (-webkit-device-pixel-ratio: 1)"
      rel="apple-touch-startup-image">

<!-- iPhone (Retina) -->
<link href="../../../images/apple-touch-startup-image-640x920.png"
      media="(device-width: 320px) and (device-height: 480px)
         and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image">

<!-- iPhone 5 -->
<link href="../../../images/apple-touch-startup-image-640x1096.png"
      media="(device-width: 320px) and (device-height: 568px)
         and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image">

<!-- iOS 8 iPhone 6 (portrait) -->
<link href="../../../images/apple-touch-startup-image-750x1294.png"
      media="(device-width: 375px) and (device-height: 667px) and (orientation: portrait)
         and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image">
<!-- iOS 8 iPhone 6 (landscape) -->
<link href="../../../images/apple-touch-startup-image-710x1334.png"
      media="(device-width: 375px) and (device-height: 667px) and (orientation: landscape)
         and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image" >
<!-- iOS 8 iPhone 6 Plus (portrait) -->
<link href="../../../images/apple-touch-startup-image-1242x2148.png" media="(device-width: 414px) and (device-height: 736px) and (orientation: portrait)
         and (-webkit-device-pixel-ratio: 3)"
      rel="apple-touch-startup-image">
<!-- iOS 8 iPhone 6 Plus (landscape) -->
<link href="../../../images/apple-touch-startup-image-1182x2208.png" media="(device-width: 414px) and (device-height: 736px) and (orientation: landscape)
         and (-webkit-device-pixel-ratio: 3)"
      rel="apple-touch-startup-image">

<!-- iPad -->
<link href="../../../images/apple-touch-startup-image-768x1004.png"
      media="(device-width: 768px) and (device-height: 1024px)
         and (orientation: portrait)
         and (-webkit-device-pixel-ratio: 1)"
      rel="apple-touch-startup-image">
<link href="../../../images/apple-touch-startup-image-748x1024.png"
      media="(device-width: 768px) and (device-height: 1024px)
         and (orientation: landscape)
         and (-webkit-device-pixel-ratio: 1)"
      rel="apple-touch-startup-image">

<!-- iPad (Retina) -->
<link href="../../../images/apple-touch-startup-image-1536x2008.png"
      media="(device-width: 768px) and (device-height: 1024px)
         and (orientation: portrait)
         and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image">
<link href="../../../images/apple-touch-startup-image-1496x2048.png"
      media="(device-width: 768px) and (device-height: 1024px)
         and (orientation: landscape)
         and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image">

<style type="text/css">
/* srsbg*/
html, body {
	overflow: hidden !important;
}
.ui-page.ui-body-c{
	background: none;
	height: 100% !important;
	overflow: hidden !important;
}

#appDiv {
	height: 100%;
	width: 100%;
	overflow-y: auto !important;
	adisplay: flex;
	aflex-direction: column;
	position: absolute;
	top: 0px;
}
#backbtn{
	position: absolute;
    top: 0;
    left: initial;
    right: 1vh;
    height: 10vh;
    width: 9vh;
    background: url("../../../images/back_button.jpg")no-repeat;
    background-size: contain;
    background-position: 50% 50%;
}
#openlogout{
	height:7vh;
	width:85px;
	margin-top:2vh;
	left:93px;
	display:none;
	background-size:95%;
	background:url("../../../images/open_logout.png")no-repeat;
	background-size:24vw,10vh;
	background-size:contain;
}
#openlogin{
	height:7vh;
	width:90px;
	margin-top:2vh;
	left:93px;
	display:none;
	background-size:95%;
	background:url("../../../images/open_login.png")no-repeat;
	background-size:24vw,10vh;
	background-size:contain;
}
#menu{
    position: fixed;
    top: 10vh;
    left: 0;
    width: 100%;
    margin-left: -100%;
    background-color: #938a8a;
    z-index: 1001;
}

.menuItemBtn{
	background-color: #938a8a;
	color: #fff;
	height: 7vh;
	display: block;
	font-size: 1.3em;
	font-weight: normal;
	text-align: center;
	border-bottom: 2px solid #444;
	padding-top:2.3vh;
	margin-left: -15px;
	margin-right:-15px;
	text-shadow: initial;
}
#menuIcon{
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    height: 10vh;
    width: 10vh;
    background: url("../../../images/menu_icon.jpg")no-repeat;
    background-size: contain;
    background-position: 50% 50%;
}
#header {
	aflex: none;
	height:10vh;
	opacity: 1;
	background-color: #1d1414;
	border-color: #1d1414;
	background-image:initial;';

if ($menuHide > 0){
	$config .= '
	display: none !important;
	';
}

$config .= '
}

#main {
	aoverflow-y: scroll !important;
	aheight: 100%;
	aflex: 1;
	position: relative;';

if ($menuHide > 0){
	$config .= '
	top: 0px;
    height: 100%;
	';
} else {
	$config .= '
	top: 10vh;
    height: calc(100% - 10vh);
	';
}

$config .= '
}

#main > div {
    height: 100%;
	aoverflow-y: scroll;
}

.ui-body-c, .ui-overlay-c { background: none;}

.ui-page .ui-header {
	aposition: fixed !important;
}

.ui-header .ui-btn {
	aposition: fixed !important;
}

#app_bground {
	height: 100%;
	width: 100%;
	background: url('.$app_bg_image.');
	background-color: #'.$app_bg_color.';
	background-position: bottom center;
	background-repeat: no-repeat;
	background-size: cover;
	overflow-y: hidden;
	position: fixed;
	bottom: 0px;
	left: 0px;
}
.appNameB{
    position: absolute;
    top: 0;
    left: 0;
    font-size: 1.3em;
    color: #fff;
    text-shadow: initial;
    line-height: 10vh;
    text-align: center;
    margin: 0 10vh;
    width: calc(100vw - 20vh);
    text-overflow: ellipsis;
}

img
{
max-width: 100%;
marginwidth: "0";
marginheight: "0";
leftmargin: "0";
topmargin: "0";
}

table
{
padding:0px;
cellpadding:0px;
margin:0px;
border:0px;
border-collapse:collapse;

}

.error
{
color:red;
}

.buttonImage{
   display:block;
}

.rowHolder{
	//background-color: #'.$app_bg_color.';
	position: relative;
	width: 100%;
	overflow-x: hidden;
}

</style>
<link type="text/css" href="../../../testskins/jplayer.blue.monday.css" rel="stylesheet" />
<link rel="stylesheet" href="http://www.launchliveapp.com/liveappmenucss/themes/liveapp.css" />
<link rel="stylesheet" href="http://www.launchliveapp.com/liveappmenucss/jquery.mobile.structure-1.0.1.min.css" />
<link rel="stylesheet" href="../../../scripts/jquery-ui-1.8.21.custom.css" />

<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.0/jquery.mobile-1.0.min.js"></script>
<script type="text/javascript" src="../../../js/jquery.jplayer.min.js"></script>
<script type = "text/javascript" src = "../../../scripts/jquery.validate.js"></script>
<script type = "text/javascript" src = "../../../scripts/jquery-ui-1.8.22.custom.min.js"></script>
<!-- <script type="text/javascript" src = "https://connect.facebook.net/en_US/all.js#xfbml=1&appId=396518167046244"></script> -->

<!-- ZAC -->
<script type="text/javascript" src="../../../portalFunctionsTest/linkedin.js"></script>
<script type="text/javascript" src="http://www.launchliveapp.com/scripts/bookmark_bubble_test.js"></script>
<script type="text/javascript" src="http://www.launchliveapp.com/common/example.js"></script>



<script type="text/javascript">
var uagent = navigator.userAgent.toLowerCase();
var loggedin = false;
var key = "<?php echo $key;?>";
var email = "<?php echo $email;?>";
var app_user_id =  "<?php echo $app_user_id;?>";
var message = "<?php echo $message;?>";
var userId = '.$userId.';
var fancyboxOpen = false;
var pagesRow = 0;
var pagesLogin = 0; // is a page password protected?
var pagesArray = 0; // ID of every page

if (window.location.hash == "#_=_") {

    window.location = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/liveapp_current.php";
    window.location.hash = "";
    history.pushState("", document.title, window.location.pathname);
    e.preventDefault();
}

	// Add a wrapper function so we can use the css Animate library in an easier way.
    $.fn.extend({
        slideAnimate: function(direction, action, duration, complete) {
            if (typeof duration == "undefined") {
                duration = 500;
            }
            switch (direction) {
                case "left":
                    if (action == "show") {
                        $(this).show();
                        $(this).animate({
                            "margin-left": "0px"
                        }, function() {
                            if (typeof complete == "function") {
                                complete();
                            }
                        });

                    } else if (action == "hide") {
                        $(this).animate({
                            "margin-left": "-100%"
                        }, function() {
                            $(this).hide();
                            if (typeof complete == "function") {
                                complete();
                            }
                        });
                    }
                    break;

                default:
                    break;
            }
        }
    });


    '.$miscfunctions -> getBookingFormLogic().'
     var formBooking;
     var formReferring;

    $(document).ready(function(){

		';
	/*
	$i = 0;
	while($pages = mysql_fetch_array($selectResult))
	{
		$config .= 'pagesRow['.$i.'] = '.js_array($pages).';';
		$i++;
	}

	for ($i = 0; $i < $pagesLogin; $i++)
	{
		$config .= 'pagesLogin['.$i.'] = '.js_array($pagesLogin[$i]).';';
	}
	for ($i = 0; $i < $pagesArray; $i++)
	{
		$config .= 'pagesArray['.$i.'] = '.js_array($pagesArray[$i]).';';
	}
	*/

	$config .= '
	  formBooking = new BookingForm("LiveAppBooking/PHP_mailer.php",".liveapp-book-form",1);
      formBooking.initialize();

	  formReferring = new BookingForm("LiveAppReferrals/PHP_mailer.php",".liveapp-ref-form",2);
	  formReferring.initialize();

	  var width = window.width;
	  var height = 0;
	  if(width > 1000)
	  {
	    height = 600;
	  }
	  else
	  {
	    height = 300;
	  }
';
if(sizeof($audioArray) != 0)
{
   foreach($audioArray as &$aud)
   {

	  $audioURL = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$aud[1];
	  //$audioURL = "";
      $config.=
	  ' $("#'.$aud[0].'").jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", {
            mp3: "'.$audioURL.'",

          });
        },
        swfPath: "../../../js",
        supplied: "mp3",
		cssSelectorAncestor: "#'.$aud[3].'"

      });

  ';
   }
}


if(sizeof($videoArray) != 0)
{
   foreach($videoArray as &$vid){

      if($vid[4] == "")
      {
	  $videoURL = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$vid[1];
	   $posterImage = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$vid[5].".jpg";
	  //$videoURL = "";
	  } else
	  {

	  $posterImage = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$vid[5].".jpg";
	  $videoURL = $vid[4];
	 // $videoURL = "";
	  }

      $config .=
	  '
      $("#'.$vid[0].'").jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", {
            m4v: "'.$videoURL.'",
			poster: "'.$posterImage.'"
          });
        },
        swfPath: "../../../js",
        supplied: "m4v",
		cssSelectorAncestor: "#'.$vid[3].'",
		fullScreen:false,
		size:{

		    "width":"100%",
			"height": height + "px"

		}

      });

  ';
   }
}
$config .='
 $("#verification").validate();
	  $("#signupform").validate();
	  $("#loginform").validate();
});
';

if($numOfImageGallery > 0 )
{

	$config .= $photogallery -> getJs();
}

if($numOfVideoGallery > 0 )
{
	$config .= $videogallery -> getJs("config");
}



$config .='

</script>
<style>
@media screen and (orientation:portrait){.ui-mobile,.ui-mobile .ui-page{min-height:100%}}
</style>
<script type="text/javascript">
$(window).load(function(){
	      $("body").children(":first").css("min-height","100%");
	 });

// Outdated Android code
// The following functions are used to handle the case of a user running native code that are missing the javascript interfaces required for the
// Android app to work properly.

function outdatedAndroidCode()
{
    if (typeof Android != "undefined" ) {
    	$.mobile.hidePageLoadingMsg();
    	if(!Android.nativePopup){
    		alert("Your app code needs to be updated to be able to access the latest features. Please update your application by navigating to the following link in your browser: http://www.launchliveapp.com/' .$folderUserName.'");
    	}else{
    		Android.nativePopup("link", "YES_NO", "There is a new version of this application available for download. Would you like to open the browser and download this update now?", "http://www.launchliveapp.com/'.$folderUserName.'/");
    	}
    }
}

var page_history = new Array();
var current_page = "1";
currentWidth = 320;
var isBubble = false;
height = window.height;
width = window.width;
isIpad = false;

var manualUpdating;   // flag that tells the app whether the user has attempted to update the app or not.
var canUpdate; // flag to tell whether the cache exists, If set to 0, the user will not be able to update the app manually. If 1, they can.
var isUpdating = 0;
var hasUpdate = 0;
var android_update = 0;
manualUpdating = 0;
canUpdate = 0;

	if(uagent.indexOf("ipad") != -1 && window.innerWidth < 770){
		var height = 1024;
		var width = 768;
		isIpad = true;

	}
	else if(uagent.indexOf("ipad") != -1 && window.innerWidth > 1020){
		var height = 1366;
		var width = 1024;
		isIpad = true;
	}
	else if(uagent.indexOf("iphone") != -1 && window.innerWidth == 320){
		var height = 480;
		var width = 320;

	}
	else if(uagent.indexOf("iphone") != -1 && window.innerWidth == 480){
		var height = 320;
		var width = 480;

	}
	else
	{
	var width = window.width;
	var height = window.height;
	}


	function featureNot()
	{
		if(navigator.onLine == false)
		{
			alert("You must have a data connection to visit this link");
			return false;
		}
		else
		{
			return true;
		}
	}


function playYoutube(videoID){
	var uagent = navigator.userAgent.toLowerCase();
	if(uagent.indexOf("android")!= -1)
	{
		window.location = "vnd.youtube:" + videoID;
	}
	else{

	  // window.location = "youtube:"+videoID;   //What?
	  //Do nothing
	}
}

function iosYouTube(ytheight, ytwidth){
     var uagent = navigator.userAgent.toLowerCase();
	 if(uagent.indexOf("iphone")!= -1)
	 {
	 ';
	  for($j = 0; $j < $numberofyoutube; $j++)
		{
			$config .=	'$("#youtube-playero'.$j.'").remove();
			';
		}
	  for($j = 0; $j < $numberofyoutube; $j++)
		{
			// $config .=	'$("<object class = \"ytindex\" id = \"youtube-playero'.$j.'\" width=\""+ytwidth+"\" height=\""+ytheight+"\" style = \"z-index:0\"><param name=\"movie\" value=\"http://www.youtube.com/v/oHg5SJYRHA0&f=gdata_videos&c=ytapi-my-clientID&d=nGF83uyVrg8eD4rfEkk22mDOl3qUImVMV6ramM\"></param><param name=\"wmode\" value=\"transparent\"></param><embed src=\"http://www.youtube.com/v/'.$youtubeItems[$j].'\" type=\"application/x-shockwave-flash\" wmode=\"transparent\" width=\""+ytwidth+"\" height=\""+ytheight+"\"></embed></object>").appendTo("#youtube-player'.$j.'");
			// ';

			// $config .=	'

			// $("<a href=\'http://www.youtube.com/embed/'. $youtubeItems[$j] .'\'><img id=\'imgYoutubeFrame'.$j.'\' /></a>").appendTo("#youtube-player'.$j.'");
			// console.log("added youtube");

			// $.getJSON("http://gdata.youtube.com/feeds/api/videos/"+"'. $youtubeItems[$j] .'"+"?v=2&alt=jsonc", function(json){
		 //        $("#imgYoutubeFrame'.$j.'").attr("src", json.data.thumbnail.hqDefault);
		 //    });

			// //----
			// ';

			// $config .=	'

			// $("<iframe id=\"youtube-playero'.$j.'\" src=\'http://www.youtube-nocookie.com/embed/'.$youtubeItems[$j].'?html5=1\' width=\"99%\" height=\""+ ytheight + "\" style = \"z-index:100\"></iframe>").appendTo("#youtube-player'.$j.'");
			// console.log("added youtube");
			// ';

			$config .=	'

			$("<div class=\"youtube-overlay\" id=\"youtube-overlay'. $j .'\"><iframe class=\"youtube-iframe\" id=\"youtube-playero'.$j.'\" src=\'https://www.youtube-nocookie.com/embed/'.$youtubeItems[$j].'?html5=1\' width=\"99%\" height=\""+ ytheight + "\" style = \"z-index:100; \"></iframe></div>").appendTo("#youtube-player'.$j.'");
			$("#youtube-overlay'. $j .'").click(function() {
				$("#youtube-playero'. $j .' .html5-video-container").click();
			});
			console.log("added youtube");
			';
		}
$config .=
	'}else if(uagent.indexOf("bb10") != -1){

	  ';
	    for($j = 0; $j < $numberofyoutube; $j++)
		{
			$config .=	'$("#youtube-playero'.$j.'").remove();
			';
		}
	  for($j = 0; $j < $numberofyoutube; $j++)
		{
			$config .=	'

			$("<iframe id=\"youtube-playero'.$j.'\" src=\'http://www.youtube.com/embed/'.$youtubeItems[$j].'?html5=1\' width=\"99%\" height=\""+ ytheight + "\" style = \"z-index:100\"></iframe>").appendTo("#youtube-player'.$j.'");
			console.log("added youtube");
			';


		}

		// for($j = 0; $j < $numberofyoutube; $j++)
		// {

		// 	$config .= '
		// 	$("#youtube-playero' . $j . '").replaceWith(\'<a href="http://www.youtube.com/watch?v='. $youtubeItems[$j] .'" id=\"youtube-playero'.$j.'\"><img id="imgYoutubeFrame"/></a>\');
		//     $.getJSON("http://gdata.youtube.com/feeds/api/videos/"+'. $youtubeItems[$j] .'+"?v=2&alt=jsonc&callback=?", function(json){
		//        $("#imgYoutubeFrame").attr("src", json.data.thumbnail.hqDefault);
		//     });

		// 	';
		// }
$config .=
'

	}


}


function removeYouTube(){
';

 for($j = 0; $j < $numberofyoutube; $j++)
		{
			$config .=	'$("#youtube-playero'.$j.'").remove();
			';
		}

$config .='

}

    function showLoadingAnimation()
    {
    	if(uagent.indexOf("iphone")!= -1 || uagent.indexOf("ipad")!= -1)
    	{
    		removeYouTube();

    	}

    	$.mobile.showPageLoadingMsg();

    }


    function hideLoadingAnimation()
    {
    	if(uagent.indexOf("iphone")!= -1 || uagent.indexOf("bb10") != -1){

    	     if(window.orientation == 0 || window.orientation == 180)
    	     {
			     $(".youtube-holder").css("height", "200px");
    	     	iosYouTube("200", "317");
    	     }else{
			  $(".youtube-holder").css("height", "300px");
    	     	iosYouTube("300","477");
    	     }
    	}
    	else if(uagent.indexOf("ipad")!= -1)
    	{
    	     if(window.orientation == 0 || window.orientation == 180)
    	    {
    	     	iosYouTube("200", "317");
    	     }else{
    	     	iosYouTube("300","477");
    	     }
    	}
    	$.mobile.hidePageLoadingMsg();
    }

	function showLoadingScreen(){
        $("#appDiv").hide();
    	$("#overlay").show();
	}

	function hideLoadingScreen(){
	    $("#overlay").hide();
		 $("#appDiv").show();

	}



function resize()
{



    if(isBubble == false){
        $(".youtube-holder").css("height", "200px");
       iosYouTube("200", "317");
    }




	';
if($numOfImageGallery > 0)
{
	$config .= '
	initGalleries();';
}

if($numOfVideoGallery > 0)
{
	$config .= '
	initVGalleries();';
}


	$config .='

	pagesLogic("1");

	if(uagent.indexOf("android")!= -1){
	    hideLoadingScreen();
	} else if (uagent.indexOf("iphone")!= -1) {
		if (uagent.indexOf("OS 8")!= -1) {
			showLoadingScreen();
		    setTimeout("hideLoadingScreen()",5000);
		} else {
			hideLoadingScreen();
		}
	} else {
	    showLoadingScreen();
	    setTimeout("hideLoadingScreen()",5000); //FRANCIS: Changed "Loading" screen timer from 3 to 2 seconds
	 }
	';
	if($login == 1)
	{
		$config .='
		loginLogic();
		messageLogic();';
	}
	else
	{
		$config .='
		removeLogin();
		';
	}
$config .='
}';

if($isFindMe == true)
{
	$config .= $findmeobj -> getconfig();
}

if($isVcard == true)
{
	$config .= $vcardobj -> getJs();
}

$config .= $loginobject -> getLogic();

$config .='

//Updating functions ----------------------------------------------------------------------->
//update test function



// Rest of Update functions
function checkForUpdates(){

	if(canUpdate == 1)
		{
			if(android_update == 1){
                if (typeof Android != "undefined" ) {
    				console.log("showing native update prompt from js");
    				if(!Android.updatePrompt){
    				    outdatedAndroidCode();
    				}else{
    					Android.updatePrompt()
    				}
                }

			}else{
				showLoadingAnimation();
				if(hasUpdate == 1){
					manualUpdating = 1;
					confirmReload();
				}else{
					manualUpdating = 1;
					menulogic("main");
					console.log("Checking for update");
					//no update has been previously detected, so run the android thread
					if(uagent.indexOf("android") != -1){
                        if (typeof Android != "undefined" ) {
    					    if(!Android.checkUpdates)
    						{
    					      outdatedAndroidCode();
    						}
    						else
    						{
    						Android.checkUpdates();
    						}
                        }
					}
					window.applicationCache.update();
				}
			}
		}
	else if(canUpdate == 0 && isUpdating == 1)
	{

		manualUpdating = 1;
		menulogic("main");
		showLoadingAnimation();
	    console.log("Update already in progress");
	}
	else
		{
			alert("Your application is up to date!");
		}
}

/*
function androidDialog()
 {
      if(loggedin == true){

		window.location = "liveapp_current.php?"+key;
	  }else{

	    window.location = "liveapp_current.php";

	  }

    }
	*/

function doUpdate(){
	hasUpdate = 0;
			window.applicationCache.swapCache();
			if(uagent.indexOf("android") != -1)
			{
				console.log("sending update post android");
				$.ajax({
					type: "POST",
					url: "../../../analytics_update.php",
					data: "userId='.$userId.'&formTXT=androidupdate&username='.$folderUserName.'",
					complete: function(html)
					{}
				});

			}

			if(uagent.indexOf("iphone") != -1)
			{
				console.log("sending update post android");
				$.ajax({
				type: "POST",
				url: "../../../analytics_update.php",
				data: "userId='.$userId.'&formTXT=iphoneupdate&username='.$folderUserName.'",
				complete: function(html)
				{
				}
				});
			}

			hideLoadingAnimation();


			if(loggedin == true){
				window.location = "liveapp_current.php?"+key;
			}else{
			     if(uagent.indexOf("bb10") != -1){
		  		window.location = "liveapp_currentbb10.php";
				}else{
				   window.location = "liveapp_current.php";
				}
		  	}
}

function confirmReload()
{

	if(manualUpdating == 1){
			doUpdate();
	}else{
			if(uagent.indexOf("android")!= -1)
			{
                if (typeof Android != "undefined" ) {
				    Android.webUpdatePrompt();
                }
			}
			else
			{

				if (confirm("There is an update available for this app. Would you like to update now?"))
				{
					doUpdate();
				}else{
					hideLoadingAnimation();
					isUpdating = 0;
					manualUpdating = 0;
					canUpdate = 1;
				}
			}
	}
}

function dontUpdate(){
	console.log("dont update called");
	hideLoadingAnimation();
	isUpdating = 0;
	manualUpdating = 0;
	canUpdate = 1;
}

function dontUpdate(android){
	android_update = android;
	console.log("android_update - " + android);
	console.log("dont update called");
	hideLoadingAnimation();
	isUpdating = 0;
	manualUpdating = 0;
	canUpdate = 1;
}

function upToDate(){
	if(manualUpdating){
		alert("Your application is up to date!");
	}
}

function showError(){
	hideLoadingAnimation();
	if(manualUpdating == 1){
		alert("Unable to update your application at this time. Please ensure that you have a working wireless connection and try again.");
	}
}

function logEvent(event)
{
	console.log(event.type);

	if(event.type == "updateready")
	{
		hasUpdate = 1;

		if(uagent.indexOf("android") != -1)
		{
            if (typeof Android != "undefined" ) {
    			Android.webAppDone("updateready");
            }
		}else{
			confirmReload();
		}

	}else if(event.type == "noupdate"){
		canUpdate = 1;
		isUpdating = 0;
		hideLoadingAnimation();
		if(uagent.indexOf("android") != -1)
		{
            if (typeof Android != "undefined" ) {
			    Android.webAppDone("noupdate");
            }
		}else{
			if(manualUpdating == 1)
			{
				upToDate();
			}
		}
	}else if(event.type == "cached"){
		hideLoadingAnimation();
		canUpdate = 1;
	}else if(event.type == "downloading"){
		isUpdating = 1;
	}else if(event.type == "checking"){
		if(uagent.indexOf("android") != -1)
		{
			//check for native code updates, checking will fire on every open of the app
            if (typeof Android != "undefined" ) {
			    Android.checkUpdates();
            }
		}



	}else if(event.type == "error"){
		isUpdating = 0;
		canUpdate = 1;


		if(uagent.indexOf("android") != -1)
		{
            if (typeof Android != "undefined" ) {
			    Android.webAppDone("error");
            }
		}else{
			if(manualUpdating == 1){
				showError();
			}
		}
	}
}

if((uagent.indexOf("iphone") != -1) || uagent.indexOf("android") != -1 || uagent.indexOf("ipad") != -1  || uagent.indexOf("bb10") != -1){
  window.applicationCache.addEventListener("checking",logEvent,false);
  window.applicationCache.addEventListener("noupdate",logEvent,false);
  window.applicationCache.addEventListener("downloading",logEvent,false);
  window.applicationCache.addEventListener("cached",logEvent,false);
  window.applicationCache.addEventListener("updateready",logEvent,false);
  window.applicationCache.addEventListener("obsolete",logEvent,false);
  window.applicationCache.addEventListener("error",logEvent,false);
  window.applicationCache.addEventListener("progress",logEvent,false);
  window.applicationCache.addEventListener("updateready", logEvent, false);

  }
function updateAndroidWebApp(){
	android_update = true;
	checkForUpdates();
}
//srs_maps
function showGoogleMap(href1, href2)
{
	if(navigator.onLine == false)
	{
		alert("You must have a data connection to visit this link");
		return false;
	}
	else
	{
		if(uagent.indexOf("android") != -1)
			window.location = href1;
		else
			window.location = href2;
	}
}
function checkLinks(href, check)
{
	if(navigator.onLine == false)
	{
		alert("You must have a data connection to visit this link");
		return false;
	}
	else
	{
		if(check == 1)
		{
			if(uagent.indexOf("android") != -1)
			{
                if (typeof Android != "undefined" ) {
				    Android.nativePopup("link", "YES_NO", "This link will open in your browser, would you like to continue?", href);
                }
			}
			else
			{
				window.location = href;
			}
		}
		if(check == "")
		{
			alert("Invalid Webpage")
			return false;
		}
		if(check == 2)
		{
			window.location = "mailto:"+href;
		}
		if(check == 3)
		{
			window.location = "sms:"+href;
		}
		if(check == 4)
		{
			window.location = "tel:"+href;
		}
	}

}

function updateGoogleBubble()
{
var orientation = window.orientation;
bubbleInner = document.getElementById("bubbleInner");
icon = document.getElementById("bubbleIcon");
close = document.getElementById("bubbleClose");
var windowWidth = (orientation==90 || orientation==-90) ? window.screen.height : window.screen.width;

var windowHeight = (orientation==90 || orientation==-90) ? window.screen.width : window.screen.height;
var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0) + "px";
var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0) + "px";
//alert(windowWidth);
		switch(orientation)
		{
			case 0:
			if(isIpad == true)
			{
				console.log("cant read my poker face7");
				icon.style.width = "150px";
				icon.style.height = "150px";
				icon.style.margin = "90px 7px 3px 90px";
				icon.style.WebkitBackgroundSize = "150px 150px";
				bubbleInner.style.width = "746px";
				bubbleInner.style.height = "906px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/images/BB10_download_screen.png)";

			}
			else if(uagent.indexOf("bb10")!= -1)
			{
				console.log("cant read my poker face6");
				bubbleInner.style.width = "340px";
				bubbleInner.style.height = "528px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/images/BB10_download_screen.png)";
				icon.style.width = "55px";
				icon.style.height = "55px";
				icon.style.margin = "40px 7px 3px 45px";
				icon.style.WebkitBackgroundSize = "55px 55px";
			}
         else if(uagent.indexOf("iphone")!= -1)
			{
				console.log("cant read my poker face6");

				bubbleInner.style.width = w; // "317px";
				bubbleInner.style.height = h; // "353px";
				bubbleInner.style.backgroundImage = "";
				//bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/gBubble_iPhone_vertical.png)";

				if (windowWidth >= 320) { // 640 // iphone 5 width is same
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPhone4/iphone4_vertical.png)";
				} else if (windowWidth >= 700) { // 750
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPhone6_/6_vertical.png)";
				} else if (windowWidth >= 1200) { // 1242
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPhone6+_/6plus_vertical.png)";
				}


				icon.style.width = "55px";
				icon.style.height = "55px";
				icon.style.margin = "10px 7px 3px 15px";
				icon.style.WebkitBackgroundSize = "55px 55px";
			}
			break;
			case 90:
			if(isIpad == true)
			{
				console.log("cant read my poker face5");
				icon.style.width = "100px";
				icon.style.height = "100px";
				icon.style.margin = "70px 7px 3px 150px";
				icon.style.WebkitBackgroundSize = "100px 100px";
				bubbleInner.style.width = "991px";
				bubbleInner.style.height = "642px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/images/BB10_download_screen.png)";

			}
			else if(uagent.indexOf("bb10")!= -1)
			{
				console.log("cant read my poker face4");
				bubbleInner.style.width = "99%";
				bubbleInner.style.height = "300px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/images/BB10_download_screen_landscape.png)";
				icon.style.width = "50px";
				icon.style.height = "50px";
				icon.style.margin = "8px 7px 3px 115px";
				icon.style.WebkitBackgroundSize = "50px 50px";
			}
			else if(uagent.indexOf("iphone")!= -1)
			{
				console.log("cant read my poker face4");

				bubbleInner.style.width = w; // "395px";
				bubbleInner.style.height = h; // "205px";
				bubbleInner.style.backgroundImage = "";
				//bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/gBubble_iPhone_horizontal.png)";

				if (windowWidth >= 320) { // 960
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPhone4/iphone4_horizontal.png)";
				} else if (windowWidth >= 1100) { // 1136
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPhone5/iphone5_horizontal.png)";
				} else if (windowWidth >= 1300) { // 1334
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPhone6_/6_horizontal.png)";
				} else if (windowWidth >= 2000) { // 2208
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPhone6+_/6plus_horizontal.png)";
				}


				icon.style.width = "50px";
				icon.style.height = "50px";
				icon.style.margin = "8px 7px 3px 110px";
				icon.style.WebkitBackgroundSize = "50px 50px";
			}
			break;
			case -90:
			if(isIpad == true)
			{
				console.log("cant read my poker face3");
				icon.style.width = "100px";
				icon.style.height = "100px";
				icon.style.margin = "70px 7px 3px 300px";
				icon.style.WebkitBackgroundSize = "100px 100px";
				bubbleInner.style.width = "991px";
				bubbleInner.style.height = "642px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/images/BB10_download_screen_landscape.png)";

			}
				else if(uagent.indexOf("bb10")!= -1)
			{
				console.log("cant read my poker face4");
				bubbleInner.style.width = "99%";
				bubbleInner.style.height = "300px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/images/BB10_download_screen_landscape.png)";
				icon.style.width = "50px";
				icon.style.height = "50px";
				icon.style.margin = "8px 7px 3px 115px";
				icon.style.WebkitBackgroundSize = "50px 50px";
			}
			else if(uagent.indexOf("iphone")!= -1)
			{
				console.log("cant read my poker face4");
				bubbleInner.style.width = w;
				bubbleInner.style.height = h;
				bubbleInner.style.backgroundImage = "";
				//bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/gBubble_iPhone_horizontal.png)";

				if (windowWidth >= 320) { // 960
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPhone4/iphone4_horizontal.png)";
				} else if (windowWidth >= 1100) { // 1136
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPhone5/iphone5_horizontal.png)";
				} else if (windowWidth >= 1300) { // 1334
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPhone6_/6_horizontal.png)";
				} else if (windowWidth >= 2000) { // 2208
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPhone6+_/6plus_horizontal.png)";
				}

				icon.style.width = "50px";
				icon.style.height = "50px";
				icon.style.margin = "8px 7px 3px 110px";
				icon.style.WebkitBackgroundSize = "50px 50px";
			}
			break;
			case 180:
			if(isIpad == true)
			{
				console.log("cant read my poker face1");
				icon.style.width = "150px";
				icon.style.height = "150px";
				icon.style.margin = "90px 7px 3px 90px";
				icon.style.WebkitBackgroundSize = "150px 150px";
				bubbleInner.style.width = "746px";
				bubbleInner.style.height = "906px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/images/BB10_download_screen.png)";

			}
			else
			{

			}
			break;
		}
}



function updateOrientation()
{
//centerButtonLabels();
 var uagent = navigator.userAgent.toLowerCase();

 var amountYoutube ='.$numberofyoutube.';
 var amountImageGallery = '.$numOfImageGallery.';

 var tubeIDs = new Array(amountYoutube);


 ';
 for($z = 0; $z < $numberofyoutube; $z++)
		{
			$config .=	'tubeIDs['.$z.'] ="'.$youtubeItems[$z].'";
			';
		}

$config.='
if(uagent.indexOf("android") == -1)
{
	if(isBubble == true)
	{
		updateGoogleBubble();
	}

	var orientation = window.orientation;
	var tds = document.getElementsByTagName("td");
	var i = 0;
	var j = 0;

		switch(orientation)
		{
			case 0:
			//here
			for(j = 0; j < tds.length; j++)
			{
				var z = tds[j].height;
				var a = width;
				newz = z.replace("px", "");
				tds[j].width = (newz/height)*height;
				tds[j].height = (width/height)*newz;
			}
			if(amountImageGallery > 0)
			{';
				if($numOfImageGallery > 0)
				{
					for($u = 0; $u < $numOfImageGallery; $u++)
					{
						$config .= "updateImageGallery".$u."(0);";
					}
				}
			$config .= '
			}

			if(amountYoutube > 0 && isBubble == false)
			{
				 $(".youtube-holder").css("height", "200px");
				iosYouTube("200","317");
			}
			overlayBG.src = "http://www.launchliveapp.com/images/loading_iPhone.png";
			break;

			case 90:
			for(j = 0; j < tds.length; j++)

			{
				var z = tds[j].height;
				var a = width;
				newz = z.replace("px", "");
				tds[j].width = (newz/width)*height;
				tds[j].height = (height/width)*newz;
			}

			if(amountImageGallery > 0)
			{';
				if($numOfImageGallery > 0)
				{
					for($u = 0; $u < $numOfImageGallery; $u++)
					{
						$config .= "updateImageGallery".$u."(90);";
					}
				}
			$config .= '
			}


			if(amountYoutube > 0 && isBubble == false)
			{
			     $(".youtube-holder").css("height", "300px");
				iosYouTube("300","477");
			}
            overlayBG.src = "http://www.launchliveapp.com/images/loading_iPhone_Landscape.png";
			break;

			case -90:
			for(j = 0; j < tds.length; j++)
			{
				var z = tds[j].height;
				var a = width;
				newz = z.replace("px", "");
				tds[j].width = (newz/width)*height;
				tds[j].height = (height/width)*newz;
			}
			if(amountYoutube > 0 && isBubble == false)
			{
			     $(".youtube-holder").css("height", "200px");
				iosYouTube("200","317");
			}

			if(amountImageGallery > 0)
			{';
				if($numOfImageGallery > 0)
				{
					for($u = 0; $u < $numOfImageGallery; $u++)
					{
						$config .= "updateImageGallery".$u."(90);";
					}
				}
			$config .= '
			}
			overlayBG.src = "http://www.launchliveapp.com/images/loading_iPhone_Landscape.png";
			break;

			case 180:
			for(j = 0; j < tds.length; j++)
			{
				var z = tds[j].height;
				var a = width;
				newz = z.replace("px", "");
				tds[j].width = (newz/height)*height;
				tds[j].height = (width/height)*newz;
			}
			overlayBG.src = "http://www.launchliveapp.com/images/loading_iPhone.jpg";
			break;
		}
	}
}';

$config .= $miscfunctions -> getMenuLogic($login, $loyalty);

$config .= '
function updateNow()
{
	if(loggedin == true)
	{
		window.location = "liveapp_current.php?"+key;
	}
	else
	{
		window.location = "liveapp_current.php";
	}
}';

$config .= '
function hideAllPages()
{
    document.getElementById("verificationdiv").style.display = "none";
    document.getElementById("signup").style.display = "none";
    document.getElementById("login").style.display = "none";
    document.getElementById("main").style.display = "none";
    document.getElementById("qr").style.display = "none";
    document.getElementById("about").style.display = "none";
    document.getElementById("forgotpass").style.display = "none";
    document.getElementById("videoplayerdiv").style.display = "none";
    document.getElementById("changepass").style.display = "none";';

if ($login == 1) {
    // Login widget is enabled, so add support for hiding its tags.
    $config .= '
    document.getElementById("openlogin").style.display = "none";
    document.getElementById("openlogout").style.display = "none";
    ';
}

if($loyalty != 0) {
    $config .= '
    document.getElementById("loyalty_rewards_dialogue").style.display = "none";
    document.getElementById("loyalty_redeem_dialogue").style.display = "none";
    document.getElementById("loyalty_current_points_dialogue").style.display = "none";
    if($("#loyalty_rewards_iframe").length > 0)
        $("#loyalty_rewards_iframe").remove();
    if($("#customer_points_iframe").length > 0)
        $("#customer_points_iframe").remove();
    if($("#slider").length > 0)
        $("#slider").remove();
    if($("#rr_image").children().length > 0)
        $("#rr_image").empty();
    if($("#loyalty_current_points").children().length > 0)
        $("#loyalty_current_points").empty();
    if($(".next_reward").data("events")) {
        if($(".next_reward").data("events").click.length > 0)
            $(".next_reward").unbind("click");
    }
    if($(".prev_reward").data("events")) {
        if($(".prev_reward").data("events").click.length > 0)
            $(".prev_reward").unbind("click");
    }';
}
$config .= '
}
';

$config .= '
//srs lnm
function pagesLogic(page, login)
{
    hideAllPages();
	if(login == 1 && email == "")
	{
		alert("You must be logged in to view this page");

		// redirect user back to login page
		menulogic("login");

		return false;
	}
	else
	{
		if(page === "liveappshare"){
		    menulogic("qr");
		} else if(page == "homePage" && (userId == 1496 || userId == 973)) {
			homeScreen();
		} else if (page == "searchResults" && (userId == 1496 || userId == 973)) {
			backSearch();
		}
		//added by Jacob (Nov 2014)
		else if(page == "rentalPage" && (userId == 1496 || userId == 973)){
			showRental();
		} else if(page == "lessonPage" && (userId == 1496 || userId == 973)){
			showLesson();
		} else if(page == "socialPage" && (userId == 1496 || userId == 973)){
			showSocial();
		} else if (page == "home_banner_1" && (userId == 1496 || userId == 973)) {
			showBanner1();
		} else if (page == "home_banner_2" && (userId == 1496 || userId == 973)) {
			showBanner2();
		} else if (page == "home_banner_3" && (userId == 1496 || userId == 973)) {
			showBanner3();
		} else if (page == "home_banner_4" && (userId == 1496 || userId == 973)) {
			showBanner4();
		} else if (page == "home_banner_5" && (userId == 1496 || userId == 973)) {
			showBanner5();
		}
		// added to fix the back button issue related to search pages
		else if (page == "searchPages" && (userId == 1496 || userId == 973)){
			searchPage--;
			ajaxSearch();
		}
		//added by Jacob on 2014-12-01
		else if (page == "apprise" && (userId == 1496 || userId == 973)){
			page_history.push("apprise");
		}
		else{


		menulogic("main");
	';
	for($i = 0; $i < $numberofpages; $i++)
	{
		$config .= "if(page == ".$pagesArray[$i].")
		{ \n";
		for($j = 0; $j < $numberofpages; $j++)
		{
			//$pagesLogin[array_search($pagesRow["page_id"],$pagesArray)];

			if($j == $i)
			{
				$config .= "document.getElementById(".$pagesArray[$j].").style.display = 'block';\n";
				// Added by Zak (July 2015)
				// When showing the password protected page, load it right away.
				$pageLoginQuery = "SELECT requirelogin FROM pages WHERE user_id=$userId AND page_id=".$pagesArray[$j];
				$pageLoginResult = mysql_query($pageLoginQuery) or die(mysql_error());
				if(mysql_num_rows($pageLoginResult) > 0)
				{
					$pageLoginRow = mysql_fetch_array($pageLoginResult);
					$protected = $pageLoginRow['requirelogin'];
					if ($protected == 1) // page is password protected
					{
						// This page is saved to a separate file, so load its content into the app.
						$config .= '
								console.log("page - after login check " + page); // never reached by page 19 & 21
								// If the page file is missing, jquery will ignore it.
								var filename = "page"+page+".php";
								$.get(filename, function(data, status) {
									//console.log("status: " + status)
									$("#"+page).html(data); // set div tag contents to data from saved page
								});';
					}
				}
			}
			else
			{
				$config .= "document.getElementById(".$pagesArray[$j].").style.display = 'none';\n";
				// Added by Zak (July 31, 2015)
				// Unload any password protected pages that aren't currently being viewed.
				$pageLoginQuery = "SELECT requirelogin FROM pages WHERE user_id=$userId AND page_id=".$pagesArray[$j];
				$pageLoginResult = mysql_query($pageLoginQuery) or die(mysql_error());
				if(mysql_num_rows($pageLoginResult) > 0)
				{
					$pageLoginRow = mysql_fetch_array($pageLoginResult);
					$protected = $pageLoginRow['requirelogin'];
					if ($protected == 1) // page is password protected
					{
						// This page is saved to a separate file, so it's safe to unload the content.
						$config .= '$("#'.$pagesArray[$j].'").html("");'; // erase all content from this app page
					}
				}
			}
		}
		$config .="}\n";
	}

	$config .= '
	 }
	}
	scroll(0,0);
	$("p").each(function(){ var color = $(this).parent().css("background-color");if( $(this).height() > 100){$(this).parent().attr("style","position:relative;background-color:"+ color +";padding:-7px;margin-top:-7px;");};});

}

function pageBack(){

	// to go back from the menu
    if((document.getElementById("menu").style.display != "none") ||
        (document.getElementById("signup").style.display != "none") ||
        (document.getElementById("login").style.display != "none") ||
        (document.getElementById("verificationdiv").style.display != "none") ||
        (document.getElementById("forgotpass").style.display != "none") ||
        (document.getElementById("changepass").style.display != "none") ||
        (document.getElementById("qr").style.display != "none")) {
        menulogic("main");

    } else if (document.getElementById("about").style.display != "none") {
        menulogic("menu");

	// navigate between pages in the app
	}else{
	    if(page_history.length > 0){
			// support fancybox image viewer
			// check if fancybox is open, if so then close it
			if (fancyboxOpen)
			{
				fancyboxOpen = false;
				$.fancybox.close();
			}
			else
			{
				// get the page to navigate too
				var goTo = page_history.pop();

				// hide the back button if there is no more history to navigate through
				if(page_history.length == 0){
					document.getElementById("backbtn").style.display = "none";
				}

                if (/^filebrowser\d+$/i.test(goTo)) {
                    id = goTo.replace("filebrowser", "");
                    fileBack(id);
                    return;
                }
				current_page = goTo;
				pagesLogic(goTo);
			}
		}else{
            if(uagent.indexOf("android")!= -1)
            {
                if (typeof Android != "undefined" ) {
                    Android.nativePopup("exit", "YES_NO", "Would you like to close this app?");
                }
            }
		}
	}
}

function pageForward(page, login){
	// If the user access a private page without being logged in, then do not remember the browser history.

	if(login == 1 && email == "")
	{
		//call the function to switch the active page
		pagesLogic(page, login);
	}
	else if (page == "liveappshare")
	{
		pagesLogic(page, login);
	}
	else
	{
		//were moving forward so there is gaurenteed to be a previous page
		document.getElementById("backbtn").style.display = "block";
		if(page != current_page)
		{
			//push the current page onto the history stack
			page_history.push(current_page);
			//set the current page to the new page that we are navigating too
			current_page = page;
		}
		//call the function to switch the active page
		pagesLogic(page, login);
	}
}
</script>
</head>
<body onload = "resize()" onorientationchange="updateOrientation();" style = "margin:0px; padding:0px" >
<!--srsrs srsbg-->
<div id="app_bground"></div>

<div id="app-page" data-role="page" tabindex="0" class="ui-page ui-body-c ui-page-active" style="min-height:10px;">
<div id="overlay" style="position:absolute; top:0px; z-index:80;background-color:white;width:100%;height:100%;">
		<img id="overlayBG" style="bottom:0px;margin:auto;width:100%;"/>

		<script>

		 var overlayBG = document.getElementById("overlayBG");
		 var orient = window.orientation;
		 if(uagent.indexOf("android") == -1)  {
			switch(orient) {
				case 0:
				case 180:
					overlayBG.src = "http://www.launchliveapp.com/images/loading_iPhone.png";
					break;
				case 90:
				case -90:
					overlayBG.src = "http://www.launchliveapp.com/images/loading_iPhone_Landscape.png";
			}
		 }

		</script>


	</div>
<div id="fb-root"></div>';
$config .= $loginobject -> getHeader();
$config .= '<div id="appDiv" style="display:none">
<script>
  // if((uagent.indexOf("android")!= -1) || (uagent.indexOf("iphone")!= -1))
	    // hideLoadingScreen();
</script>
';

//$config .= $loginobject -> getHeader();
$config .= '<div id="main" style="z-index: 0">';


$configipad =
'
<?php
	session_start();
	$email = "";
	if(isset($_SESSION["email"]))
	{
		$email = $_SESSION["email"];
	}
	$key = "";
	if(isset($_SESSION["key"]))
	{
		$key = $_SESSION["key"];
	}
	$message = "";

	if(isset($_GET["message"]))
	{
		{
			$message = $_GET["message"];
		}
	}
	include("../../../tweetLoader3.php");
?>

<!DOCTYPE html>
<html  manifest='.$folderUserName.'ipad.manifest>
<head>
<title>'.$folderUserName.'</title>

<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0" />
<META HTTP-EQUIV="expires" CONTENT="Wed, 19 Feb 2003 08:00:00 GMT">
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="apple-touch-icon-precomposed" href="appicon72.png" />
<!--<link rel="apple-touch-startup-image" href="http://www.launchliveapp.com/images/loading_ipad1.png" media="screen and (orientation:landscape)" />-->
<!--<link rel="apple-touch-startup-image" href="http://www.launchliveapp.com/images/loading_ipad2.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" />-->
<!-- iPhone -->
<link href="../../../images/apple-touch-startup-image-320x460.png"
      media="(device-width: 320px) and (device-height: 480px)
         and (-webkit-device-pixel-ratio: 1)"
      rel="apple-touch-startup-image">

<!-- iPhone (Retina) -->
<link href="../../../images/apple-touch-startup-image-640x920.png"
      media="(device-width: 320px) and (device-height: 480px)
         and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image">

<!-- iPhone 5 -->
<link href="../../../images/apple-touch-startup-image-640x1096.png"
      media="(device-width: 320px) and (device-height: 568px)
         and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image">

<!-- iOS 8 iPhone 6 (portrait) -->
<link href="../../../images/apple-touch-startup-image-750x1294.png"
      media="(device-width: 375px) and (device-height: 667px) and (orientation: portrait)
         and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image">
<!-- iOS 8 iPhone 6 (landscape) -->
<link href="../../../images/apple-touch-startup-image-710x1334.png"
      media="(device-width: 375px) and (device-height: 667px) and (orientation: landscape)
         and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image" >
<!-- iOS 8 iPhone 6 Plus (portrait) -->
<link href="../../../images/apple-touch-startup-image-1242x2148.png" media="(device-width: 414px) and (device-height: 736px) and (orientation: portrait)
         and (-webkit-device-pixel-ratio: 3)"
      rel="apple-touch-startup-image">
<!-- iOS 8 iPhone 6 Plus (landscape) -->
<link href="../../../images/apple-touch-startup-image-1182x2208.png" media="(device-width: 414px) and (device-height: 736px) and (orientation: landscape)
         and (-webkit-device-pixel-ratio: 3)"
      rel="apple-touch-startup-image">

<!-- iPad -->
<link href="../../../images/apple-touch-startup-image-768x1004.png"
      media="(device-width: 768px) and (device-height: 1024px)
         and (orientation: portrait)
         and (-webkit-device-pixel-ratio: 1)"
      rel="apple-touch-startup-image">
<link href="../../../images/apple-touch-startup-image-748x1024.png"
      media="(device-width: 768px) and (device-height: 1024px)
         and (orientation: landscape)
         and (-webkit-device-pixel-ratio: 1)"
      rel="apple-touch-startup-image">

<!-- iPad (Retina) -->
<link href="../../../images/apple-touch-startup-image-1536x2008.png"
      media="(device-width: 768px) and (device-height: 1024px)
         and (orientation: portrait)
         and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image">
<link href="../../../images/apple-touch-startup-image-1496x2048.png"
      media="(device-width: 768px) and (device-height: 1024px)
         and (orientation: landscape)
         and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image">

<style type="text/css">

.youtube-overlay {
	width: auto;
	height: auto;
	z-index: 110;

}

/* srsbg*/
html, body {
	aheight: 100%;
	overflow: hidden !important;
}

.ui-page.ui-body-c{
	background: none;
	height: 100% !important;
	overflow: hidden !important;
}

#appDiv {
	height: 100%;
	width: 100%;
	overflow-y: auto !important;
	adisplay: flex;
	aflex-direction: column;
	position: absolute;
	top: 0px;
}
#backbtn{
	position: absolute;
    top: 0;
    left: initial;
    right: 1vh;
    height: 8vh;
    width: 7vh;
    background: url("../../../images/back_button.jpg")no-repeat;
    background-size: contain;
    background-position: 50% 50%;
}
#openlogout{
	height:7vh;
	width:85px;
	margin-top:2vh;
	left:93px;
	display:none;
	background-size:95%;
	background:url("../../../images/open_logout.png")no-repeat;
	background-size:24vw,10vh;
	background-size:contain;
}
#openlogin{
	height:7vh;
	width:90px;
	margin-top:2vh;
	left:93px;
	display:none;
	background-size:95%;
	background:url("../../../images/open_login.png")no-repeat;
	background-size:24vw,10vh;
	background-size:contain;
}
#menu{
	position: fixed;
    top: 8vh;
    left: 0;
    width: 100%;
    margin-left: -100%;
    background-color: #938a8a;
    z-index: 1001;
}

.menuItemBtn{
	background-color: #938a8a;
	color: #fff;
	height: 7vh;
	display: block;
	font-size: 1.3em;
	font-weight: normal;
	text-align: center;
	border-bottom: 2px solid #444;
	padding-top:2.3vh;
	margin-left: -15px;
	margin-right:-15px;
	text-shadow: initial;
}
#menuIcon{
    display: block;
	position: absolute;
    left: 0;
    top: 0;
    height: 8vh;
    width: 8vh;
    background: url("../../../images/menu_icon.jpg")no-repeat;
    background-size: contain;
    background-position: 50% 50%;
}

#header {
	aflex: none;
	height:8vh;
	opacity: 1;
	background-color: #1d1414;
	border-color: #1d1414;
	background-image:initial;';

if ($menuHide > 0){
	$configipad .= '
	display: none !important;
	';
}

$configipad .= '
}

#main {
	aoverflow-y: scroll !important;
	aheight: 100%;
	aflex: 1;
	position: relative;';

if ($menuHide > 0){
	$configipad .= '
	top: 0px;
    height: 100%;
	';
} else {
	$configipad .= '
	top: 8vh;
    height: calc(100% - 8vh);
	';
}

$configipad .= '
}

#main > div {
    height: 100%;
	aoverflow-y: scroll;
}

.ui-body-c, .ui-overlay-c { background: none;}

.ui-page .ui-header {
	aposition: fixed !important;
}

.ui-header .ui-btn {
	aposition: fixed !important;
}

#app_bground {
	height: 100%;
	width: 100%;
	background: url('.$app_bg_image.');
	background-color: #'.$app_bg_color.';
	background-position: bottom center;
	background-repeat: no-repeat;
	background-size: cover;
	overflow-y: hidden;
	position: fixed;
	bottom: 0px;
}
.appNameB{
    position: absolute;
    top: 0;
    left: 0;
    font-size: 1.3em;
    color: #fff;
    text-shadow: initial;
    line-height: 8vh;
    text-align: center;
    margin: 0 10vh;
    width: calc(100vw - 20vh);
    text-overflow: ellipsis;
}

img
{
max-width: 100%;
marginwidth: "0";
marginheight: "0";
leftmargin: "0";
topmargin: "0";
}

table
{
padding:0px;
cellpadding:0px;
margin:0px;
border:0px;
border-collapse:collapse;

}

.buttonImage{
   display:block;
}
.rowHolder{
	//background-color: #'.$app_bg_color.';
	position: relative;
	width: 100%;
	overflow-x: hidden;
}

</style>
<link type="text/css" href="../../../testskins/jplayer.blue.monday.css" rel="stylesheet" />
<link rel="stylesheet" href="http://www.launchliveapp.com/liveappmenucss/themes/liveapp.css" />
<link rel="stylesheet" href="http://www.launchliveapp.com/liveappmenucss/jquery.mobile.structure-1.0.1.min.css" />
<link rel="stylesheet" href="../../../scripts/jquery-ui-1.8.21.custom.css" />

<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.0/jquery.mobile-1.0.min.js"></script>
<script type="text/javascript" src="../../../js/jquery.jplayer.min.js"></script>
<script type = "text/javascript" src = "../../../scripts/jquery.validate.js"></script>
<script type = "text/javascript" src = "../../../scripts/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="http://www.launchliveapp.com/portalFunctionsTest/linkedin.js"></script>
<!-- <script type="text/javascript" src="https://connect.facebook.net/en_US/all.js#xfbml=1&appId=396518167046244"></script> -->
<script type="text/javascript">
var loggedin = false;
var key = "<?php echo $key;?>";
var email = "<?php echo $email;?>";
var message = "<?php echo $message;?>";
var userId = '.$userId.';
var fancyboxOpen = false;


var manualUpdating;   // flag that tells the app whether the user has attempted to update the app or not.
var canUpdate; // flag to tell whether the cache exists, If set to 0, the user will not be able to update the app manually. If 1, they can.
var isUpdating = 0;
var hasUpdate = 0;
manualUpdating = 0;
canUpdate = 0;


if (window.location.hash == "#_=_") {
    window.location.hash = "";
    history.pushState("", document.title, window.location.pathname);
    e.preventDefault();
}

// Add a wrapper function so we can use the css Animate library in an easier way.
    $.fn.extend({
        slideAnimate: function(direction, action, duration, complete) {
            if (typeof duration == "undefined") {
                duration = 500;
            }
            switch (direction) {
                case "left":
                    if (action == "show") {
                        $(this).show();
                        $(this).animate({
                            "margin-left": "0px"
                        }, function() {
                            if (typeof complete == "function") {
                                complete();
                            }
                        });

                    } else if (action == "hide") {
                        $(this).animate({
                            "margin-left": "-100%"
                        }, function() {
                            $(this).hide();
                            if (typeof complete == "function") {
                                complete();
                            }
                        });
                    }
                    break;

                default:
                    break;
            }
        }
    });

   '.$miscfunctions -> getBookingFormLogic().'
     var formBooking;
     var formReferring;

    $(document).ready(function(){

	  formBooking = new BookingForm("LiveAppBooking/PHP_mailer.php",".liveapp-book-form",1);
      formBooking.initialize();

	  formReferring = new BookingForm("LiveAppReferrals/PHP_mailer.php",".liveapp-ref-form",2);
	  formReferring.initialize();

	  var width = window.width;
	  var height = 0;
	  if(width > 1000)
	  {
	    height = 600;

	  }
	  else
	  {
	    height = 300;


	  }
';
//echo "i got here";
if(sizeof($audioArray) != 0)
{
   foreach($audioArray as &$aud){

	  $audioURL = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$aud[1];
	  //$audioURL = "";
      $configipad.=
	  ' $("#'.$aud[0].'").jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", {
            mp3: "'.$audioURL.'",

          });
        },
        swfPath: "../../../js",
        supplied: "mp3",
		cssSelectorAncestor: "#'.$aud[3].'"

      });

  ';
   }
}


if(sizeof($videoArray) != 0)
{
   foreach($videoArray as &$vid){

	if($vid[4] == "")
	{
		$videoURL = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$vid[1];
		$posterImage = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$vid[5].".jpg";
	}
	else
	{
		$posterImage = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$vid[5].".jpg";
		$videoURL = $vid[4];
	}


      $configipad.=
	  '
      $("#'.$vid[0].'").jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", {
            m4v: "'.$videoURL.'",
            poster: "'.$posterImage.'"
          });
        },
        swfPath: "../../../js",
        supplied: "m4v",
		cssSelectorAncestor: "#'.$vid[3].'",
		fullScreen:false,
		size:{

		    "width":"100%",
			"height":"600px"

		}

      });

  ';
   }
}
$configipad .='
  $("#verification").validate();
	  $("#signupform").validate();
	  $("#loginform").validate();
});
';

if($numOfImageGallery > 0 )
{
	$configipad .= $photogallery -> getJs();
}


if($numOfVideoGallery > 0 )
{
	$configipad .= $videogallery -> getJs("configipad");
}
$configipad .='
</script>
<script type="text/javascript">
var page_history = new Array();
var current_page = "1";
currentWidth = 320;
var isBubble = false;
var isIpad = true;
var isHorizontal = false;
height = window.height;
width = window.width;
var uagent = navigator.userAgent.toLowerCase();

	if(uagent.indexOf("ipad") != -1 && window.innerWidth < 770){
		var height = 1024;
		var width = 768;
	}
	else if(uagent.indexOf("ipad") != -1 && window.innerWidth > 1020){
		var height = 1366;
		var width = 1024;
		isHorizontal = true;
	}
	else
	{
	var width = window.width;
	var height = window.height;
	}

 function featureNot(){
	if(navigator.onLine == false)
	{
		alert("You must have a data connection to visit this link");
		return false;
	}
	else
	{
		return true;
	}
}



function iosYouTube(ytheight, ytwidth){
     var uagent = navigator.userAgent.toLowerCase();

	 console.log("dogg");
	 ';
	  for($j = 0; $j < $numberofyoutube; $j++)
		{
			$configipad .=	'$("#youtube-playero'.$j.'").remove();
			';
		}

	  for($j = 0; $j < $numberofyoutube; $j++)
		{
			// $configipad .=	'$("<object class = \"ytindex\" id = \"youtube-playero'.$j.'\" width=\"100%\" height=\""+ytheight+"\" style = \"z-index:0\"><param name=\"movie\" value=\"http://www.youtube.com/v/oHg5SJYRHA0&f=gdata_videos&c=ytapi-my-clientID&d=nGF83uyVrg8eD4rfEkk22mDOl3qUImVMV6ramM\"></param><param name=\"wmode\" value=\"transparent\"></param><embed src=\"http://www.youtube.com/v/'.$youtubeItems[$j].'\" type=\"application/x-shockwave-flash\" wmode=\"transparent\" width=\"100%\" height=\""+ytheight+"\"></embed></object>").appendTo("#youtube-player'.$j.'");
			// ';
			$configipad .=	'

			$("<div class=\"youtube-overlay\" id=\"youtube-overlay'. $j .'\"><iframe class=\"youtube-iframe\" id=\"youtube-playero'.$j.'\" src=\'http://www.youtube-nocookie.com/embed/'.$youtubeItems[$j].'?html5=1\' width=\"99%\" height=\""+ ytheight + "\" style = \"z-index:100; \"></iframe></div>").appendTo("#youtube-player'.$j.'");
			$("#youtube-overlay'. $j .'").click(function() {
				$("#youtube-playero'. $j .' .html5-video-container").click();
			});
			console.log("added youtube");
			';
		}


		// for($j = 0; $j < $numberofyoutube; $j++)
		// {

		// 	$configipad .= '
		// 	$("#youtube-playero' . $j . '").replaceWith(\'<a href="http://www.youtube.com/watch?v=+'. $youtubeItems[$j] .'+" id="youtube-playero'.$j.'"><img id="imgYoutubeFrame"/></a>\');
		//     $.getJSON("http://gdata.youtube.com/feeds/api/videos/"+'. $youtubeItems[$j] .'+"?v=2&alt=jsonc&callback=?", function(json){
		//        $("#imgYoutubeFrame").attr("src", json.data.thumbnail.hqDefault);
		//     });

		// 	';
		// }


$configipad .= '


}

function removeYouTube(){
';

 for($j = 0; $j < $numberofyoutube; $j++)
		{
			$configipad .=	'$("#youtube-playero'.$j.'").remove();
			';
		}

$configipad .='

}

   function showLoadingAnimation()
    {
    	if(uagent.indexOf("iphone")!= -1 || uagent.indexOf("ipad")!= -1)
    	{
    		removeYouTube();

    	}

    	$.mobile.showPageLoadingMsg();

    }


    function hideLoadingAnimation()
    {
    	if(uagent.indexOf("iphone")!= -1 || uagent.indexOf("bb10") != -1){

    	     if(window.orientation == 0 || window.orientation == 180)
    	     {
			     $(".youtube-holder").css("height", "200px");
    	     	iosYouTube("200", "317");
    	     }else{
			  $(".youtube-holder").css("height", "300px");
    	     	iosYouTube("300","477");
    	     }
    	}
    	else if(uagent.indexOf("ipad")!= -1)
    	{
    	     if(window.orientation == 0 || window.orientation == 180)
    	    {
    	        $(".youtube-holder").css("height", "485px");
    	     	iosYouTube("485", "765");
    	     }else{
    	        $(".youtube-holder").css("height", "680px");
    	     	iosYouTube("680","1021");
    	     }
    	}
    	$.mobile.hidePageLoadingMsg();
    }

function resize()
{
document.getElementById("backbtn").style.display = "none";
if(isHorizontal == true)
	{

	    if(isBubble == false){
	        $(".youtube-holder").css("height", "680px");
       		iosYouTube("680", "1021");

        }

		$(".imagegallerytd").each(function(){
		var old =$(this).height();
		var newh = (786/1024)-170;
		$(this).attr("height", newh+"px");

		});
	}
	else
	{
	    if(isBubble == false){
	    $(".youtube-holder").css("height", "485px");
			iosYouTube("485", "765");

		}

	}
	menulogic("main");
	';
	if($numOfImageGallery > 0)
	{
		$configipad .= '
		initGalleries();';
	}
	if($numOfVideoGallery > 0 )
	{
		$configipad .= '
		initVGalleries();';
	}
	$configipad .='
	pagesLogic("1");
	';
	if($login == 1)
	{
		$configipad .='
		loginLogic();
		messageLogic();';
	}
	else
	{
		$configipad .='
		removeLogin();
		';
	}
	$configipad .='
	/*
	if(uagent.indexOf("ipad") != -1){

		var tds = document.getElementsByTagName("td");
		var i = 0;
		var j = 0;
		var buttonLabel;

			for(j = 0; j < tds.length; j++)
			{




				var z = tds[j].height;
				var a = 320;
				newz = z.replace("px", "");
				tds[j].height = (width/a)*newz + "px";



			}

		var imgs = document.getElementById("fbimage");
		if(imgs !== null)
		{
				var z = imgs.height;
				var y = imgs.width;
				var a = 320;
				//newz = z.replace("px", "");
				imgs.width = width/10;
				imgs.height = (width/a)*z;
		}
}
*/

}';

if($isFindMe == true)
{
	$configipad .= $findmeobj -> getconfigipad();
}
$configipad .= $loginobject -> getLogic();

if($isVcard == true)
{
	$configipad .= $vcardobj -> getJs();
}

 $configipad .='

function doUpdate(){
	hasUpdate = 0;
	window.applicationCache.swapCache();
	console.log("sending update post ipad");
	$.ajax({
	type: "POST",
	url: "../../../analytics_update.php",
	data: "userId='.$userId.'&formTXT=iphoneupdate&username='.$folderUserName.'",
	complete: function(html)
	{
	}
	});
	hideLoadingAnimation();

	if(loggedin == true){
		window.location = "liveapp_currentipad.php?"+key;
	}else{
		window.location = "liveapp_currentipad.php";
	}
}
function confirmReload()
{
	if(manualUpdating == 1){
		doUpdate();
	}else{
		if (confirm("There is an update available for this app. Would you like to update now?"))
		{
			doUpdate();
		}else{
			hideLoadingAnimation();
			isUpdating = 0;
			manualUpdating = 0;
			canUpdate = 1;
		}
	}
}

function logEvent(event)
{

	console.log(event.type);
	if(event.type == "updateready")
	{
			hasUpdate = 1;
			if(uagent.indexOf("android") != -1)
			{
                if (typeof Android != "undefined" ) {
				    Android.checkUpdates();
                }
			}else{
				confirmReload();
			}
		}else if(event.type == "noupdate"){
			 canUpdate = 1;
			 isUpdating = 0;
			hideLoadingAnimation();
			if(manualUpdating == 1)
			{
				alert("Your application is up to date!");
			}
		}else if(event.type == "cached"){
			hideLoadingAnimation();
			canUpdate = 1;

		}else if(event.type == "downloading"){

			isUpdating = 1;

		}else if(event.type == "error"){
		    isUpdating = 0;
		    canUpdate = 1;

			if(manualUpdating == 1){
				alert("Unable to update your application at this time. Please ensure that you have a working wireless connection and try again.");
				hideLoadingAnimation();
			}

		}
}

  window.applicationCache.addEventListener("checking",logEvent,false);
  window.applicationCache.addEventListener("noupdate",logEvent,false);
  window.applicationCache.addEventListener("downloading",logEvent,false);
  window.applicationCache.addEventListener("cached",logEvent,false);
  window.applicationCache.addEventListener("updateready",logEvent,false);
  window.applicationCache.addEventListener("obsolete",logEvent,false);
  window.applicationCache.addEventListener("error",logEvent,false);
  window.applicationCache.addEventListener("progress",logEvent,false);
  window.applicationCache.addEventListener("updateready",logEvent, false);


function updateGoogleBubble()
{

var orientation = window.orientation;
bubbleInner = document.getElementById("bubbleInner");
icon = document.getElementById("bubbleIcon");
close = document.getElementById("bubbleClose");
var windowWidth = (orientation==90 || orientation==-90) ? window.screen.height : window.screen.width;

var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0) + "px";
var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0) + "px";
//alert(windowWidth);

		switch(orientation)
		{
			case 0:
			if(isIpad == true)
			{
				icon.style.width = "150px";
				icon.style.height = "150px";
				icon.style.margin = "90px 7px 3px 90px";
				icon.style.WebkitBackgroundSize = "150px 150px";

				bubbleInner.style.width = w; // "746px";
				bubbleInner.style.height = h; // "906px";
				bubbleInner.style.backgroundImage = "";

				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/gBubble_iPad_vertical_"+iosVersion+".png)";

				if (windowWidth >= 700) { // 768
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPad/ipad_vertical.png)";
				} else if (windowWidth >= 1500) { // 1536
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPadAir_/ipadair_vertical.png)";
				}

			}
			else
			{
				console.log("cant read my poker face6");
				bubbleInner.style.width = "317px";
				bubbleInner.style.height = "413px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/gBubble_iPhone_vertical_"+iosVersion+".png)";
				icon.style.width = "55px";
				icon.style.height = "55px";
				icon.style.margin = "40px 7px 3px 35px";
				icon.style.WebkitBackgroundSize = "55px 55px";
			}
			break;
			case 90:
			if(isIpad == true)
			{
				console.log("cant read my poker face5");
				icon.style.width = "100px";
				icon.style.height = "100px";
				icon.style.margin = "70px 7px 3px 300px";
				icon.style.WebkitBackgroundSize = "100px 100px";

				bubbleInner.style.width = w; // "991px";
				bubbleInner.style.height = h; // "642px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/gBubble_iPad_horizontal_"+iosVersion+".png)";

				if (windowWidth >= 1000) { // 1024
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPad/ipad_horizontal.png)";
				} else if (windowWidth >= 2000) { // 2048
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPadAir_/ipadair_horizontal.png)";
				}



			}
			else
			{
				console.log("cant read my poker face4");
				bubbleInner.style.width = "395px";
				bubbleInner.style.height = "205px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/gBubble_iPhone_horizontal_"+iosVersion+".png)";

				icon.style.width = "50px";
				icon.style.height = "50px";
				icon.style.margin = "8px 7px 3px 110px";
				icon.style.WebkitBackgroundSize = "50px 50px";
			}
			break;
			case -90:
			if(isIpad == true)
			{
				console.log("cant read my poker face3");
				icon.style.width = "100px";
				icon.style.height = "100px";
				icon.style.margin = "70px 7px 3px 300px";
				icon.style.WebkitBackgroundSize = "100px 100px";

				bubbleInner.style.width = w; // "991px";
				bubbleInner.style.height = h; // "642px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/gBubble_iPad_horizontal_"+iosVersion+".png)";

				if (windowWidth >= 1000) { // 1024
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPad/ipad_horizontal.png)";
				} else if (windowWidth >= 2000) { // 2048
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPadAir_/ipadair_horizontal.png)";
				}

			}
			else
			{
				console.log("cant read my poker face2");
				bubbleInner.style.width = "395px";
				bubbleInner.style.height = "205px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/gBubble_iPhone_horizontal_"+iosVersion+".png)";
				icon.style.width = "50px";
				icon.style.height = "50px";
				icon.style.margin = "8px 7px 3px 110px";
				icon.style.WebkitBackgroundSize = "50px 50px";
			}
			break;
			case 180:
			if(isIpad == true)
			{
				console.log("cant read my poker face1");
				icon.style.width = "150px";
				icon.style.height = "150px";
				icon.style.margin = "90px 7px 3px 90px";
				icon.style.WebkitBackgroundSize = "150px 150px";

				bubbleInner.style.width = w; // "746px";
				bubbleInner.style.height = h; // "906px";
				bubbleInner.style.backgroundImage = "";
				bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/gBubble_iPad_vertical_"+iosVersion+".png)";
				if (windowWidth >= 700) { // 768
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPad/ipad_vertical.png)";
				} else if (windowWidth >= 1500) { // 1536
					bubbleInner.style.backgroundImage = "url(http://www.launchliveapp.com/newimages/installPrompts/iPadAir_/ipadair_vertical.png)";
				}
			}
			else
			{

			}
			break;
		}
}

function checkLinks(href, check)
{
	if(navigator.onLine == false)
	{
		alert("You must have a data connection to visit this link");
		return false;
	}
	else
	{
		if(check == 1)
		{
			window.location = href;
		}
		if(check == "")
		{
			alert("Invalid Webpage")
			return false;
		}
			if(check == 2)
		{
			window.location = "mailto:"+href;
		}
		if(check == 3)
		{
			window.location = "sms:"+href;
		}
		if(check == 4)
		{
			window.location = "tel:"+href;
		}
	}

}

$(document).ready(function () {
	// updateGoogleBubble();
});

//srsbg ipadbg
function updateOrientation()
{
 var uagent = navigator.userAgent.toLowerCase();

 var amountYoutube ='.$numberofyoutube.';
 var amountImageGallery = '.$numOfImageGallery.';

 var tubeIDs = new Array(amountYoutube);


 ';
 for($z = 0; $z < $numberofyoutube; $z++)
		{
			$configipad .=	'tubeIDs['.$z.'] ="'.$youtubeItems[$z].'";
			';
		}

$configipad .='

if(isBubble == true)
{
	updateGoogleBubble();
}
	var orientation = window.orientation;
	var tds = document.getElementsByTagName("td");
	var i = 0;
	var j = 0;
	if(document.images){

	    console.log("TROLOL");
		console.log(width);
		console.log(height);
	}
		switch(orientation)
		{

			case 0:
			for(j = 0; j < tds.length; j++)
			{
				var z = tds[j].height;
				var a = width;
				newz = z.replace("px", "");
				tds[j].width = (newz/height)*height;
				tds[j].height = (width/height)*newz;


			}

			if(amountYoutube > 0 && isBubble == false)
			{
			    $(".youtube-holder").css("height", "485px");
				//iosYouTube("485", "765");
				iosYouTube("485", "765");
			}

			if(amountImageGallery > 0)
			{';
				if($numOfImageGallery > 0)
				{
					for($u = 0; $u < $numOfImageGallery; $u++)
					{
						$configipad .= "updateImageGallery".$u."(0);";
					}
				}
			$configipad .= '
			}
			break;

			case 90:
			for(j = 0; j < tds.length; j++)
			{
				var z = tds[j].height;
				var a = width;
				newz = z.replace("px", "");
				tds[j].width = (newz/width)*height;
				tds[j].height = (height/width)*newz;
			}

			if(amountYoutube > 0 && isBubble == false)
			{
			    $(".youtube-holder").css("height", "680px");
				iosYouTube("680", "1021");
			}

			if(amountImageGallery > 0)
			{';
				if($numOfImageGallery > 0)
				{
					for($u = 0; $u < $numOfImageGallery; $u++)
					{
						$configipad .= "updateImageGallery".$u."(90);";
					}
				}
			$configipad .= '
			}
			break;

			case -90:
			for(j = 0; j < tds.length; j++)
			{
				var z = tds[j].height;
				var a = width;
				newz = z.replace("px", "");
				tds[j].width = (newz/width)*height;
				tds[j].height = (height/width)*newz;
			}
            if(amountYoutube > 0 && isBubble == false)
			{
			    $(".youtube-holder").css("height", "680px");
				iosYouTube("680", "1021");
			}

			if(amountImageGallery > 0)
			{';
				if($numOfImageGallery > 0)
				{
					for($u = 0; $u < $numOfImageGallery; $u++)
					{
						$configipad .= "updateImageGallery".$u."(90);";
					}
				}
			$configipad .= '
			}
			break;

			case 180:
			for(j = 0; j < tds.length; j++)
			{
				var z = tds[j].height;
				var a = width;
				newz = z.replace("px", "");
				tds[j].width = (newz/height)*height;
				tds[j].height = (width/height)*newz;
			}

			if(amountImageGallery > 0)
			{';
				if($numOfImageGallery > 0)
				{
					for($u = 0; $u < $numOfImageGallery; $u++)
					{
						$configipad .= "updateImageGallery".$u."(0);";
					}
				}
			$configipad .= '
			}

			if(amountYoutube > 0 && isBubble == false)
			{
                                $(".youtube-holder").css("height", "485px");
				iosYouTube("485", "765");
			}

			break;
		}
}';

$configipad .= $miscfunctions -> getMenuLogic($login, $loyalty);

$configipad .= '
function updateNow()
{
	if(loggedin == true)
	{
		window.location = "liveapp_currentipad.php?"+key;
	}
	else
	{
		window.location = "liveapp_currentipad.php";
	}
}';

$configipad .= '
function hideAllPages()
{
    document.getElementById("verificationdiv").style.display = "none";
    document.getElementById("signup").style.display = "none";
    document.getElementById("login").style.display = "none";
    document.getElementById("main").style.display = "none";
    document.getElementById("qr").style.display = "none";
    document.getElementById("about").style.display = "none";
    document.getElementById("forgotpass").style.display = "none";
    document.getElementById("videoplayerdiv").style.display = "none";
    document.getElementById("changepass").style.display = "none";';

if ($login == 1) {
    // Login widget is enabled, so add support for hiding its tags.
    $configipad .= '
    document.getElementById("openlogin").style.display = "none";
    document.getElementById("openlogout").style.display = "none";
    ';
}

if($loyalty != 0) {
    $configipad .= '
    document.getElementById("loyalty_rewards_dialogue").style.display = "none";
    document.getElementById("loyalty_redeem_dialogue").style.display = "none";
    document.getElementById("loyalty_current_points_dialogue").style.display = "none";
    if($("#loyalty_rewards_iframe").length > 0)
        $("#loyalty_rewards_iframe").remove();
    if($("#customer_points_iframe").length > 0)
        $("#customer_points_iframe").remove();
    if($("#slider").length > 0)
        $("#slider").remove();
    if($("#rr_image").children().length > 0)
        $("#rr_image").empty();
    if($("#loyalty_current_points").children().length > 0)
        $("#loyalty_current_points").empty();
    if($(".next_reward").data("events")) {
        if($(".next_reward").data("events").click.length > 0)
            $(".next_reward").unbind("click");
    }
    if($(".prev_reward").data("events")) {
        if($(".prev_reward").data("events").click.length > 0)
            $(".prev_reward").unbind("click");
    }';
}
$configipad .= '
}
';

$configipad .= '
// srs lnm
function pagesLogic(page, login)
{
    hideAllPages();
	if(login == 1 && email == "")
	{
		alert("You must be logged in to view this page");

		// redirect user back to login page
		menulogic("login");

		return false;
	}
	else
	{
		if(page === "liveappshare"){
		    menulogic("qr");
		} else if(page == "homePage" && (userId == 1496 || userId == 973)) {
			homeScreen();
		} else if (page == "searchResults" && (userId == 1496 || userId == 973)) {
			backSearch();
		}
		//added by Jacob (Nov 2014)
		else if(page == "rentalPage" && (userId == 1496 || userId == 973)){
			showRental();
		} else if(page == "lessonPage" && (userId == 1496 || userId == 973)){
			showLesson();
		} else if(page == "socialPage" && (userId == 1496 || userId == 973)){
			showSocial();
		} else if (page == "home_banner_1" && (userId == 1496 || userId == 973)) {
			showBanner1();
		} else if (page == "home_banner_2" && (userId == 1496 || userId == 973)) {
			showBanner2();
		} else if (page == "home_banner_3" && (userId == 1496 || userId == 973)) {
			showBanner3();
		} else if (page == "home_banner_4" && (userId == 1496 || userId == 973)) {
			showBanner4();
		} else if (page == "home_banner_5" && (userId == 1496 || userId == 973)) {
			showBanner5();
		}
		// added to fix the back button issue related to search pages
		else if (page == "searchPages" && (userId == 1496 || userId == 973)){
			searchPage--;
			ajaxSearch();
		}
		//added by Jacob on 2014-12-01
		else if (page == "apprise" && (userId == 1496 || userId == 973)){
			page_history.push("apprise");
		}
		else{
			menulogic("main");
	';
	for($i = 0; $i < $numberofpages; $i++)
	{
	$configipad .= "if(page == ".$pagesArray[$i].")
	{ \n";
	for($j = 0; $j < $numberofpages; $j++)
	{
	if($j == $i)
	{
	$configipad .= "document.getElementById(".$pagesArray[$j].").style.display = 'block';\n";
	// When showing the page, load it right away. Password protected pages are not loaded by default.
	$configipad .= '
		if (login == 1) // page is password protected, it is not loaded by default
		{
			var filename = "page'.$pagesArray[$j].'.php";
			$.get(filename, function(data) {
				$("#'.$pagesArray[$j].'").html(data); // set div tag contents to data from saved page
			});
		}';
	}
	else
	{
	$configipad .= "document.getElementById(".$pagesArray[$j].").style.display = 'none';\n";
	}
	}

	$configipad .="}\n";
	}

	$configipad .= '
		}
	}
	scroll(0,0);
	$("p").each(function(){ var color = $(this).parent().css("background-color");if( $(this).height() > 100){$(this).parent().attr("style","position:relative;background-color:"+ color +";padding:-7px;margin-top:-7px;");};});
}
//update test function
function checkForUpdates(){
	if(canUpdate == 1)
		{
			showLoadingAnimation();
			if(hasUpdate == 1){
				manualUpdating = 1;
				confirmReload();
			}else{
				manualUpdating = 1;
				menulogic("main");
				console.log("Checking for update");
				window.applicationCache.update();
			}
		}
	else if(canUpdate == 0 && isUpdating == 1)
	{
		manualUpdating = 1;
		menulogic("main");
		showLoadingAnimation();
	    console.log("Update already in progress");
	}
	else
		{
			alert("Your application is up to date!");
		}
}

function pageBack(){

	// to go back from the menu
    if((document.getElementById("menu").style.display != "none") ||
        (document.getElementById("signup").style.display != "none") ||
        (document.getElementById("login").style.display != "none") ||
        (document.getElementById("verificationdiv").style.display != "none") ||
        (document.getElementById("forgotpass").style.display != "none") ||
        (document.getElementById("changepass").style.display != "none") ||
        (document.getElementById("qr").style.display != "none")) {
        menulogic("main");

    } else if (document.getElementById("about").style.display != "none") {
        menulogic("menu");

	// navigate between pages in the app
	}else{
			// get the page to navigate too
		if(page_history.length > 0){
			// support fancybox image viewer
			// check if fancybox is open, if so then close it
			if (fancyboxOpen)
			{
				fancyboxOpen = false;
				$.fancybox.close();
			}
			else
			{
				// get the page to navigate too
				var goTo = page_history.pop();

				// hide the back button if there is no more history to navigate through
				if(page_history.length == 0){
					document.getElementById("backbtn").style.display = "none";
				}

                if (/^filebrowser\d+$/i.test(goTo)) {
                    id = goTo.replace("filebrowser", "");
                    fileBack(id);
                    return;
                }
				current_page = goTo;
				pagesLogic(goTo);
			}
		}
	}
}

function pageForward(page, login){
	// If the users access a private page without being logged in, then do not remember the browser history.

	if(login == 1 && email == "")
	{
		//call the function to switch the active page
		pagesLogic(page, login);
	}
	else if (page == "liveappshare")
	{
		pagesLogic(page, login);
	}
	else
	{
		//were moving forward so there is gaurenteed to be a previous page
		document.getElementById("backbtn").style.display = "block";
		//push the current page onto the history stack
		page_history.push(current_page);
		//set the current page to the new page that we are navigating too
		current_page = page;
		//call the function to switch the active page
		pagesLogic(page, login);
	}
}
</script>
<script type="text/javascript" src="../../../scripts/bookmark_bubble_test.js"></script>
<script type="text/javascript" src="../../../common/example.js"></script>
</head>

<body onload = "resize()" onorientationchange="updateOrientation();" style = "margin:0px; padding:0px" >
<!--srsrs srsbg srsbgipad-->
<div id="app_bground"></div>';

$configipad .= '
<div id="app-page" data-role="page" tabindex="0" class="ui-page ui-body-c ui-page-active" style="min-height:10px;">
<div id="fb-root"></div>
';

$configipad .= $loginobject -> getHeader();
$configipad .='	<div id="appDiv" >
<div id="main" style="z-index: 0">
';


$configbb =
'<!DOCTYPE html>
<html>
<head>
<title>'.$folderUserName.'</title>
<?php


?>
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<META HTTP-EQUIV="expires" CONTENT="Wed, 19 Feb 2003 08:00:00 GMT">

<style type="text/css">

/* srsbg*/
html, body {
	overflow: hidden !important;
}
.ui-page.ui-body-c{
	background: none;
	height: 100% !important;
	overflow: hidden !important;
}

#appDiv {
	height: 100%;
	overflow-y: auto !important;
	adisplay: flex;
	aflex-direction: column;
}
#backbtn{
	position: absolute;
    top: 0;
    left: initial;
    right: 1vh;
    height: 10vh;
    width: 9vh;
    background: url("../../../images/back_button.jpg")no-repeat;
    background-size: contain;
    background-position: 50% 50%;
}
#openlogout{
	height:7vh;
	width:85px;
	margin-top:2vh;
	left:93px;
	display:none;
	background-size:95%;
	background:url("../../../images/open_logout.png")no-repeat;
	background-size:24vw,10vh;
	background-size:contain;
}
#openlogin{
	height:7vh;
	width:90px;
	margin-top:2vh;
	left:93px;
	display:none;
	background-size:95%;
	background:url("../../../images/open_login.png")no-repeat;
	background-size:24vw,10vh;
	background-size:contain;
}
#menu{
    position: fixed;
    top: 10vh;
    left: 0;
    width: 100%;
    margin-left: -100%;
    background-color: #938a8a;
    z-index: 1001;
}

.menuItemBtn{
	background-color: #938a8a;
	color: #fff;
	height: 7vh;
	display: block;
	font-size: 1.3em;
	font-weight: normal;
	text-align: center;
	border-bottom: 2px solid #444;
	padding-top:2.3vh;
	margin-left: -15px;
	margin-right:-15px;
	text-shadow: initial;
}
#menuIcon{
    display: block;
	position: absolute;
    left: 0;
    top: 0;
    height: 10vh;
    width: 10vh;
    background: url("../../../images/menu_icon.jpg")no-repeat;
    background-size: contain;
    background-position: 50% 50%;
}

#header {
	aflex: none;
	height:10vh;
	opacity: 1;
	background-color: #1d1414;
	border-color: #1d1414;
	background-image:initial;';

if ($menuHide > 0){
	$configbb .= '
	display: none !important;
	';
}

$configbb .= '
}

#main {
	aoverflow-y: scroll !important;
	aheight: 100%;
	aflex: 1;';

if ($menuHide > 0){
	$configbb .= '
	top: 0px;
    height: 100%;
	';
} else {
	$configbb .= '
	top: 10vh;
    height: calc(100% - 10vh);
	';
}

$configbb .= '
}

#main > div {
    height: 100%;
	aoverflow-y: scroll;
}

.ui-body-c, .ui-overlay-c { background: none;}

.ui-page .ui-header {
	aposition: fixed !important;
}

.ui-header .ui-btn {
	aposition: fixed !important;
}

#app_bground {
	height: 100%;
	width: 100%;
	background: url('.$app_bg_image.');
	background-color: #'.$app_bg_color.';
	background-position: bottom center;
	background-repeat: no-repeat;
	background-size: cover;
	overflow-y: hidden;
	position: fixed;
	bottom: 0px;
}
.appNameB{
	position: absolute;
    top: 0;
    left: 0;
    font-size: 1.3em;
    color: #fff;
    text-shadow: initial;
    line-height: 10vh;
    text-align: center;
    margin: 0 10vh;
    width: calc(100vw - 20vh);
    text-overflow: ellipsis;
}

img
{
max-width: 100%;
marginwidth: "0";
marginheight: "0";
leftmargin: "0";
topmargin: "0";
}

table
{
padding:0px;
cellpadding:0px;
margin:0px;
border:0px;
border-collapse:collapse;

}


a:link    {text-decoration:none;}
a:visited {text-decoration:none;}
a:hover   {text-decoration:none;}
a:active  {text-decoration:none;}

</style>
<link type="text/css" href="../../../testskins/jplayer.blue.monday.css" rel="stylesheet" />
<link rel="stylesheet" href="../../../liveappmenucss/themes/liveapp.css" />
<link rel="stylesheet" href="../../../liveappmenucss/jquery.mobile.structure-1.0.1.min.css" />

<script type="text/javascript" src="../../../scripts/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="../../../scripts/jquery.mobile-1.0.1.min.js"></script>

<script type="text/javascript" src="../../../js/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="../../../portalFunctionsTest/linkedin.js"></script>
<script type="text/javascript">
function outdated(){

   alert("Please update your Mobile Operating System in order to use this feature");

}

</script>
<script type="text/javascript">
var fancyboxOpen = false;

	// Add a wrapper function so we can use the css Animate library in an easier way.
    $.fn.extend({
        slideAnimate: function(direction, action, duration, complete) {
            if (typeof duration == "undefined") {
                duration = 500;
            }
            switch (direction) {
                case "left":
                    if (action == "show") {
                        $(this).show();
                        $(this).animate({
                            "margin-left": "0px"
                        }, function() {
                            if (typeof complete == "function") {
                                complete();
                            }
                        });

                    } else if (action == "hide") {
                        $(this).animate({
                            "margin-left": "-100%"
                        }, function() {
                            $(this).hide();
                            if (typeof complete == "function") {
                                complete();
                            }
                        });
                    }
                    break;

                default:
                    break;
            }
        }
    });


    $(document).ready(function()
	{

	$("p").each(function(){ var color = $(this).parent().css("background-color");if( $(this).height() > 100){$(this).parent().attr("style","position:relative;background-color:"+ color +";padding:0px;margin:auto");};});
	  var width = window.width;
	  var height = 0;
	  if(width > 1000)
	  {
	    height = 600;

	  }
	  else
	  {
	    height = 300;


	  }
';
//echo "i got here";
if(sizeof($audioArray) != 0)
{
   foreach($audioArray as &$aud){

	  $audioURL = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$aud[1];
	  //$audioURL = "";
      $configbb.=
	  ' $("#'.$aud[0].'").jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", {
            mp3: "'.$audioURL.'",

          });
        },
        swfPath: "../../../js",
        supplied: "mp3",
		cssSelectorAncestor: "#'.$aud[3].'"

      });

  ';
   }
}


if(sizeof($videoArray) != 0)
{
   foreach($videoArray as &$vid){

	  //$videoURL = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$vid[1];
	  $videoURL = "";
      $configbb.=
	  '
      $("#'.$vid[0].'").jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", {
            m4v: "'.$videoURL.'"

          });
        },
        swfPath: "../../../js",
        supplied: "m4v",
		cssSelectorAncestor: "#'.$vid[3].'",
		fullScreen:false,
		size:{

		    "width":"100%",
			"height": height + "px"

		}

      });

  ';
   }
}
$configbb .='});
';



$configbb .='
</script>
<script type="text/javascript">
var page_history = new Array();
var current_page = "1";

var width = window.width;
var height = window.height;

var uagent = navigator.userAgent.toLowerCase();
var version = "'.$app_version.'";

if((uagent.indexOf("6.0") != -1 || uagent.indexOf("7.0") != -1 ) && uagent.indexOf("blackberry") != -1)
{
	window.location = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/liveapp_currentbb1.php";
}


function featureNot()
{
	alert("Feature not available. You must update your operating system in order to use this feature.");
}


function logEvent(event)
{
  console.log(event.type);
  if(event.type == "updateready")
  {
	window.applicationCache.swapCache();
	if (confirm(" new version of this site is available. Load it?"))
	{
	  window.location.reload();
	}
  }

}

 //
  window.applicationCache.addEventListener("checking",logEvent,false);
  window.applicationCache.addEventListener("noupdate",logEvent,false);
  window.applicationCache.addEventListener("downloading",logEvent,false);
  window.applicationCache.addEventListener("cached",logEvent,false);
  window.applicationCache.addEventListener("updateready",logEvent,false);
  window.applicationCache.addEventListener("obsolete",logEvent,false);
  window.applicationCache.addEventListener("error",logEvent,false);
  window.applicationCache.addEventListener("progress",logEvent,false);
  window.applicationCache.addEventListener("updateready", logEvent, false);

function checkLinks(href, check)
{
	if(navigator.onLine == false)
	{
		alert("You must have a data connection to visit this link");
		return false;
	}
	else
	{
		if(check == 1)
		{
			window.location = href;
		}
		if(check == "")
		{
			alert("Invalid Webpage")
			return false;
		}
	}

}

function updateBlackberry()
{
if(navigator.onLine == false)
	{
		alert("You must have a data connection to update this app.");
		return false;
	}
	else
	{




	   blackberry.update();
	}

}';
if($isFindMe == true)
{
	$configbb .= $findmeobj -> getconfigbb();
}

if($isVcard == true)
{
	$configbb .= $vcardobj -> getbbJs();
}

$configbb .= '
function resize(){
	document.getElementById("backbtn").style.display = "none";
	';
	if($numOfImageGallery > 0)
	{
		$configbb .= '
		initGalleries();';
	}
	$configbb .='
	if(uagent.indexOf("blackberry") != -1){
		//CheckUpdates(version);
	}

}

function updateOrientation() {

}

</script>

</head>
<!--srsbg bbbg-->
<body  onload = "resize();" onorientationchange="updateOrientation();" style = "margin:0px; padding:0px" >
<!--srsrs srsbg-->
<div id="app_bground"></div>

<div id="app-page" data-role="page" tabindex="0" class="ui-page ui-body-c ui-page-active" style="min-height:10px;">
<div data-role="header" data-position="fixed" data-theme="f" class = "ui-bar-b">
		<a onClick = "history.back()" data-role="button">Back</a>
		<h1>'.$folderUserName.'</h1>
		<!--<a href="liveappmenu.html" data-role="button" >Menu</a>-->
</div>

<div id="main" style="z-index: 0">
';


$configbb1 =
'
<?php
	session_start();
	$email = "";
	if(isset($_SESSION["email"]))
	{
		$email = $_SESSION["email"];
	}
	$key = "";
	if(isset($_SESSION["key"]))
	{
		$key = $_SESSION["key"];
	}
	$message = "";

	if(isset($_GET["message"]))
	{
		{
			$message = $_GET["message"];
		}
	}
include("../../../tweetLoader3.php");
?>

<!DOCTYPE html>
<html>
<head>
<title>'.$folderUserName.'</title>

<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<META HTTP-EQUIV="expires" CONTENT="Wed, 19 Feb 2003 08:00:00 GMT">

<style type="text/css">

/* srsbg*/
html, body {
	overflow: hidden !important;
}
.ui-page.ui-body-c{
	background: none;
	height: 100% !important;
	overflow: hidden !important;
}

#appDiv {
	height: 100%;
	overflow-y: auto !important;
	adisplay: flex;
	aflex-direction: column;
}
#backbtn{
    position: absolute;
    top: 0;
    left: initial;
    right: 1vh;
    height: 10vh;
    width: 9vh;
    background: url("../../../images/back_button.jpg")no-repeat;
    background-size: contain;
    background-position: 50% 50%;

}
#openlogout{
	height:7vh;
	width:85px;
	margin-top:2vh;
	left:93px;
	display:none;
	background-size:95%;
	background:url("../../../images/open_logout.png")no-repeat;
	background-size:24vw,10vh;
	background-size:contain;
}
#openlogin{
	height:7vh;
	width:90px;
	margin-top:2vh;
	left:93px;
	display:none;
	background-size:95%;
	background:url("../../../images/open_login.png")no-repeat;
	background-size:24vw,10vh;
	background-size:contain;
}
#menu{
	position: fixed;
    top: 10vh;
    left: 0;
    width: 100%;
    margin-left: -100%;
    background-color: #938a8a;
    z-index: 1001;
}

.menuItemBtn{
	background-color: #938a8a;
	color: #fff;
	height: 7vh;
	display: block;
	font-size: 1.3em;
	font-weight: normal;
	text-align: center;
	border-bottom: 2px solid #444;
	padding-top:2.3vh;
	margin-left: -15px;
	margin-right:-15px;
	text-shadow: initial;
}
#menuIcon{
    display: block;
	position: absolute;
    left: 0;
    top: 0;
    height: 10vh;
    width: 10vh;
    background: url("../../../images/menu_icon.jpg")no-repeat;
    background-size: contain;
    background-position: 50% 50%;
}

#header {
	aflex: none;
	height:10vh;
	opacity: 1;
	background-color: #1d1414;
	border-color: #1d1414;
	background-image:initial;
}

#main {
    height: calc(100% - 10vh);
	overflow-y: scroll !important;
	aheight: 100%;
	aflex: 1;';

if ($menuHide > 0){
	$configbb1 .= '
	display: none !important;
	';
}

$configbb1 .= '
}

#main {
    height: calc(100% - 10vh);
	aoverflow-y: scroll !important;
	aheight: 100%;
	aflex: 1;';

if ($menuHide > 0){
	$configbb1 .= '
	top: 0;
    height: 100%;
	';
} else {
	$configbb1 .= '
	top: 10vh;
    height: calc(100% - 10vh);
	';
}

$configbb1 .= '
}

#main > div {
    height: 100%;
	aoverflow-y: scroll;
}

.ui-body-c, .ui-overlay-c { background: none;}

.ui-page .ui-header {
	aposition: fixed !important;
}

.ui-header .ui-btn {
	aposition: fixed !important;
}

#app_bground {
	height: 100%;
	width: 100%;
	background: url('.$app_bg_image.');
	background-color: #'.$app_bg_color.';
	background-position: bottom center;
	background-repeat: no-repeat;
	background-size: cover;
	overflow-y: hidden;
	position: fixed;
	bottom: 0px;
}
.appNameB{
    position: absolute;
    top: 0;
    left: 0;
    font-size: 1.3em;
    color: #fff;
    text-shadow: initial;
    line-height: 10vh;
    text-align: center;
    margin: 0 10vh;
    width: calc(100vw - 20vh);
    text-overflow: ellipsis;
}

img
{
max-width: 100%;
marginwidth: "0";
marginheight: "0";
leftmargin: "0";
topmargin: "0";
}

table
{
padding:0px;
cellpadding:0px;
margin:0px;
border:0px;
border-collapse:collapse;

}


a:link    {text-decoration:none;}
a:visited {text-decoration:none;}
a:hover   {text-decoration:none;}
a:active  {text-decoration:none;}

center:hover {cursor:hand;}

</style>

<link type="text/css" href="local:///jplayer.blue.monday.css" rel="stylesheet" />
<link rel="stylesheet" href="local:///liveapp.css" />
<link rel="stylesheet" href="local:///jquery.mobile.structure-1.0.1.min.css"/>
<link rel="stylesheet" href="../../../scripts/jquery-ui-1.8.21.custom.css" />

<script type="text/javascript" src="local:///jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="local:///jquery.mobile-1.0.1.min.js" ></script>
<script type = "text/javascript" src = "../../../scripts/jquery.validate.js" ></script>
<script type="text/javascript" src="local:///jquery.jplayer.min.js" ></script>
<script type = "text/javascript" src = "../../../scripts/jquery-ui-1.8.22.custom.min.js"></script>
<!-- <script type="text/javascript" src = "https://connect.facebook.net/en_US/all.js#xfbml=1&appId=396518167046244"></script> -->
<!-- ZAC -->
<script type="text/javascript" src="../../../portalFunctionsTest/linkedin.js"></script>
';

$configbb1.='
<script type="text/javascript">
alert("Your Blackberry code needs to be updated to be able to access the latest features. Please update your application by navigating to the following link in your browser: http://www.launchliveapp.com/' .$folderUserName.'");
var loggedin = false;
var key = "<?php echo $key;?>";
var email = "<?php echo $email;?>";
var message = "<?php echo $message;?>";
var userId = '.$userId.';

    // Add a wrapper function so we can use the css Animate library in an easier way.
    $.fn.extend({
        slideAnimate: function(direction, action, duration, complete) {
            if (typeof duration == "undefined") {
                duration = 500;
            }
            switch (direction) {
                case "left":
                    if (action == "show") {
                        $(this).show();
                        $(this).animate({
                            "margin-left": "0px"
                        }, function() {
                            if (typeof complete == "function") {
                                complete();
                            }
                        });

                    } else if (action == "hide") {
                        $(this).animate({
                            "margin-left": "-100%"
                        }, function() {
                            $(this).hide();
                            if (typeof complete == "function") {
                                complete();
                            }
                        });
                    }
                    break;

                default:
                    break;
            }
        }
    });

    '.$miscfunctions -> getBookingFormLogic().'
     var formBooking;
     var formReferring;

    $(document).ready(function(){

	  formBooking = new BookingForm("LiveAppBooking/PHP_mailer.php",".liveapp-book-form",1);
      formBooking.initialize();

	  formReferring = new BookingForm("LiveAppReferrals/PHP_mailer.php",".liveapp-ref-form",2);
	  formReferring.initialize();';

		if($numOfImageGallery > 0)
		{
			for($u = 0; $u < $numOfImageGallery; $u++)
			{
				$configbb1 .= "updateImageGallery".$u."(0);";
			}
		}
		if($userId == '785')
		{
				$configbb1 .="

				$('#startDate' ).datepicker({
				beforeShow: function(input, inst) {
				$('#PartySize').hide();
				$('#ResTime').hide();
				$('#RestaurantID').hide();
				},
				onClose : function(dateText, inst){
				$('#PartySize').show();
				$('#ResTime').show();
				$('#RestaurantID').show();
				}
				});
			}";
		}

		if($userId == '776')
		{
		        $configbb1 .='

		        function aa(){

		          aax = document.getElementById("Event").innerHTML;

		           document.getElementById("Event").innerHTML = aax.replace(/ target=\"_blank\"/g,"");

	            }
		      	EventWidgets.Events($("div#Event"),"ClientID=232&EventURL=irmevents.aspx&Count=10");

		       setTimeout("aa()",1000);

		       ';
		}

				$configbb1 .="
				if ( $('.datepicker').length ){
                    $('.datepicker').datepicker({minDate: '0', dateFormat: 'yy-mm-dd'});
			}";

		$configbb1 .='

	  $("p").each(function(){ var color = $(this).parent().css("background-color");if( $(this).height() > 100){$(this).parent().attr("style","position:relative;background-color:"+ color +";padding:-5px;margin-top:-5px");};});



	  var width = window.width;
	  var height = 0;
	  if(width > 1000)
	  {
	    height = 600;

	  }
	  else
	  {
	    height = 300;


	  }
';
//echo "i got here";
if(sizeof($audioArray) != 0)
{
   foreach($audioArray as &$aud){

	  $audioURL = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$aud[1];
	  //$audioURL = "";
      $configbb1 .=
	  ' $("#'.$aud[0].'").jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", {
            mp3: "'.$audioURL.'",

          });
        },
        swfPath: "../../../js",
        supplied: "mp3",
		cssSelectorAncestor: "#'.$aud[3].'"

      });

  ';
   }
}


if(sizeof($videoArray) != 0)
{
   foreach($videoArray as &$vid){

	  //$videoURL = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$vid[1];
	  $videoURL = "";
      $configbb1.=
	  '
      $("#'.$vid[0].'").jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", {
            m4v: "'.$videoURL.'"

          });
        },
        swfPath: "../../../js",
        supplied: "m4v",
		cssSelectorAncestor: "#'.$vid[3].'",
		fullScreen:false,
		size:{

		    "width":"100%",
			"height": height + "px"

		}

      });

  ';
   }
}
$configbb1 .='

	  $("#verification").validate();
	  $("#signupform").validate();
	  $("#loginform").validate();
});
';

if($numOfImageGallery > 0 )
{
	$configbb1 .= $photogallery -> getJs();
}

if($numOfVideoGallery > 0 )
{
	$configbb1 .= $videogallery -> getJs("bb1");
}

if($isVcard == true)
{
	$configbb1 .= $vcardobj -> getbbJs();
}

$configbb1 .='
</script>
<script type="text/javascript">
var page_history = new Array();
var current_page = "1";

var height = window.height;
var width = window.width;

var uagent = navigator.userAgent.toLowerCase();
var version = "'.$app_version.'";

function featureNot()
{
	if(navigator.onLine == false)
	{
		alert("You must have a data connection to visit this link");
		return false;
	}
	else
	{
		return true;
	}
}


function resize()
{


	menulogic("main");


	document.getElementById("backbtn").style.display = "none";
	var version = "'.$app_version.'";



	if(uagent.indexOf("blackberry") != -1){
		//CheckUpdates(version);
	}
	';
	if($numOfImageGallery > 0)
	{
		$configbb1 .= '
		initGalleries();';
	}
	if($numOfVideoGallery > 0)
	{
		$configbb1 .= '
		initVGalleries();';
	}
	$configbb1 .='
	pagesLogic("1");
	';
	if($login == 1)
	{
		$configbb1 .='
		loginLogic();
		messageLogic();';
	}
	else
	{
		$configbb1 .='
		removeLogin();
		';
	}
$configbb1 .='
document.getElementById("backbtn").style.display = "none";
}';

$configbb1 .= $loginobject -> getLogic();

if($isFindMe == true)
{
	$configbb1 .= $findmeobj -> getconfigbb1();
}


$configbb1 .='
function htmloffline()
{
	if(navigator.onLine == false)
	{
		alert("You must have a data connection to play this video");
		return false;
	}
	else
	{
		return true;
	}
}


function checkLinks(href, check)
{
	if(navigator.onLine == false)
	{
		alert("You must have a data connection to visit this link");
		return false;
	}
	else
	{
		if(check == 1)
		{
			if(uagent.indexOf("android") != -1){
                if (typeof Android != "undefined" ) {
				    Android.nativePopup("link", "YES_NO", "This link will open in your browser, would you like to continue?", href);
                }
			}
			else{
				window.location = href;
			}
		}
		if(check == "")
		{
			alert("Invalid Webpage")
			return false;
		}
		if(check == 2)
		{
			window.location = "mailto:"+href;
		}
		if(check == 3)
		{
			window.location = "sms:"+href;
		}
		if(check == 4)
		{
			window.location = "tel:"+href;
		}
	}

}
function updateBlackberry()
{
if(navigator.onLine == false)
	{
		alert("You must have a data connection to update this app.");
		return false;
	}
	else
	{




	   blackberry.update();
	}

}';

$configbb1 .= $miscfunctions -> getMenuLogic($login, $loyalty);

$configbb1 .= '
function hideAllPages()
{
    document.getElementById("verificationdiv").style.display = "none";
    document.getElementById("signup").style.display = "none";
    document.getElementById("login").style.display = "none";
    document.getElementById("main").style.display = "none";
    document.getElementById("qr").style.display = "none";
    document.getElementById("about").style.display = "none";
    document.getElementById("forgotpass").style.display = "none";
    document.getElementById("videoplayerdiv").style.display = "none";
    document.getElementById("changepass").style.display = "none";';

if ($login == 1) {
    // Login widget is enabled, so add support for hiding its tags.
    $configbb1 .= '
    document.getElementById("openlogin").style.display = "none";
    document.getElementById("openlogout").style.display = "none";
    ';
}

if($loyalty != 0) {
    $configbb1 .= '
    document.getElementById("loyalty_rewards_dialogue").style.display = "none";
    document.getElementById("loyalty_redeem_dialogue").style.display = "none";
    document.getElementById("loyalty_current_points_dialogue").style.display = "none";
    if($("#loyalty_rewards_iframe").length > 0)
        $("#loyalty_rewards_iframe").remove();
    if($("#customer_points_iframe").length > 0)
        $("#customer_points_iframe").remove();
    if($("#slider").length > 0)
        $("#slider").remove();
    if($("#rr_image").children().length > 0)
        $("#rr_image").empty();
    if($("#loyalty_current_points").children().length > 0)
        $("#loyalty_current_points").empty();
    if($(".next_reward").data("events")) {
        if($(".next_reward").data("events").click.length > 0)
            $(".next_reward").unbind("click");
    }
    if($(".prev_reward").data("events")) {
        if($(".prev_reward").data("events").click.length > 0)
            $(".prev_reward").unbind("click");
    }';
}
$configbb1 .= '
}
';

$configbb1 .='
// srs lnm
function pagesLogic(page, login)
{
    hideAllPages();
	if(login == 1 && email == "")
	{
		alert("You must be logged in to view this page");

		// redirect user back to login page
		menulogic("login");

		return false;
	}
	else
	{
	  if(page === "liveappshare"){
		    menulogic("qr");
		} else if(page == "homePage" && (userId == 1496 || userId == 973)) {
			homeScreen();
		} else if (page == "searchResults" && (userId == 1496 || userId == 973)) {
			backSearch();
		}
		//added by Jacob (Nov 2014)
		else if(page == "rentalPage" && (userId == 1496 || userId == 973)){
			showRental();
		} else if(page == "lessonPage" && (userId == 1496 || userId == 973)){
			showLesson();
		} else if(page == "socialPage" && (userId == 1496 || userId == 973)){
			showSocial();
		} else if (page == "home_banner_1" && (userId == 1496 || userId == 973)) {
			showBanner1();
		} else if (page == "home_banner_2" && (userId == 1496 || userId == 973)) {
			showBanner2();
		} else if (page == "home_banner_3" && (userId == 1496 || userId == 973)) {
			showBanner3();
		} else if (page == "home_banner_4" && (userId == 1496 || userId == 973)) {
			showBanner4();
		} else if (page == "home_banner_5" && (userId == 1496 || userId == 973)) {
			showBanner5();
		}
		// added to fix the back button issue related to search pages
		else if (page == "searchPages" && (userId == 1496 || userId == 973)){
			searchPage--;
			ajaxSearch();
		}
		//added by Jacob on 2014-12-01
		else if (page == "apprise" && (userId == 1496 || userId == 973)){
			page_history.push("apprise");
		}
		else{

			menulogic("main");
	';
	for($i = 0; $i < $numberofpages; $i++)
	{
	$configbb1 .= "if(page == ".$pagesArray[$i].")
	{ \n";
	for($j = 0; $j < $numberofpages; $j++)
	{
	if($j == $i)
	{
	$configbb1 .= "document.getElementById(".$pagesArray[$j].").style.display = 'block';\n";
	// When showing the page, load it right away. Password protected pages are not loaded by default.
	$configbb1 .= '
		if (login == 1) // page is password protected, it is not loaded by default
		{
			var filename = "page'.$pagesArray[$j].'.php";
			$.get(filename, function(data) {
				$("#'.$pagesArray[$j].'").html(data); // set div tag contents to data from saved page
			});
		}';
	}
	else
	{
	$configbb1 .= "document.getElementById(".$pagesArray[$j].").style.display = 'none';\n";
	}
	}

	$configbb1 .="}\n";
	}

	$configbb1 .= '
	  }
	}
	$("p").each(function(){ var color = $(this).parent().css("background-color");if( $(this).height() > 100){$(this).parent().attr("style","position:relative;background-color:"+ color +";padding:-5px;margin-top:-5px");};});
	scroll(0,0);
		if ( $(".datepicker").length ){
                    $(".datepicker").datepicker({minDate: "0", dateFormat: "yy-mm-dd"});
			}
}

function pageBack(){

	// to go back from the mmenu
	if((document.getElementById("menu").style.display != "none") ||
        (document.getElementById("signup").style.display != "none") ||
        (document.getElementById("login").style.display != "none") ||
        (document.getElementById("verificationdiv").style.display != "none") ||
        (document.getElementById("forgotpass").style.display != "none") ||
        (document.getElementById("changepass").style.display != "none") ||
        (document.getElementById("qr").style.display != "none")) {
        menulogic("main");

    } else if (document.getElementById("about").style.display != "none") {
        menulogic("menu");

	// navigate between pages in the app
	}else{
			// get the page to navigate too
		if(page_history.length > 0 ){
			// support fancybox image viewer
			// check if fancybox is open, if so then close it
			if (fancyboxOpen)
			{
				fancyboxOpen = false;
				$.fancybox.close();
			}
			else
			{
				// get the page to navigate too
				var goTo = page_history.pop();

				// hide the back button if there is no more history to navigate through
				if(page_history.length == 0){
					document.getElementById("backbtn").style.display = "none";
				}

                if (/^filebrowser\d+$/i.test(goTo)) {
                    id = goTo.replace("filebrowser", "");
                    fileBack(id);
                    return;
                }
				current_page = goTo;
				pagesLogic(goTo);
			}

		}else{
		 CheckUpdates("exit", "YES_NO","Would you like to close this app?");
		}
	}
}

function pageForward(page, login){
	// If the user access a private page without being logged in, then do not remember the browser history.
	if(login == 1 && email == "")
	{
		//call the function to switch the active page
		pagesLogic(page, login);
	}
	else if (page == "liveappshare")
	{
		pagesLogic(page, login);
	}
	else
	{
		//were moving forward so there is gaurenteed to be a previous page
		document.getElementById("backbtn").style.display = "block";
		//push the current page onto the history stack
		page_history.push(current_page);
		//set the current page to the new page that we are navigating too
		current_page = page;
		//call the function to switch the active page
		pagesLogic(page, login);
	}

}

function updateOrientation() {

}

</script>

</head>
<!--srsbg bb1bg-->
<body onload = "resize()" onorientationchange="updateOrientation();" style = "margin:0px; padding:0px" >
<!--srsrs srsbg-->
<div id="app_bground"></div>

<div id="app-page" data-role="page" tabindex="0" class="ui-page ui-body-c ui-page-active" style="min-height:10px;">
<div id="fb-root"></div>';

$configbb1 .= $loginobject -> getHeader();

$configbb1 .='
<div id="bb_error" style="display:none;">
<center><img src="http://www.launchliveapp.com/Logo.png" style="width:60%"/></center>
<p style="color:#000000;text-align:center;">Your Blackberry code needs to be updated to be able to access the latest features. Please update your application by copying the following link into your browser.</p>
<p style="color:#0000ff;text-align:center;">http://www.launchliveapp.com/' . $folderUserName . '</p>
</div>
<div id="main" style="z-index: 0">
';
$youtubenumber = 0;
$totalgalleries = 0;
$bbheader = $configbb;
$bbpages = 0;
$query = "SELECT * FROM users WHERE user_id = '$userId'";
$result = mysql_query($query);
$row = mysql_fetch_array($result);
$paid = $row['paid'];
$fbid = $row['fbid'];
$fbtoken = $row['accesstoken'];
$published = $row['published'];
$about = $row['about'];

$pagesQuery = "SELECT * FROM pages WHERE user_id = $userId ORDER BY page_order ASC";
$pagesResult = mysql_query($pagesQuery);
$numOfPages = mysql_num_rows($pagesResult);

$cache .= "\nhttp://www.launchliveapp.com/PortalImages/fbshare.jpg";
$cache .= "\nhttp://platform.twitter.com/widgets.js";
$cacheipad .= "\nhttp://www.launchliveapp.com/PortalImages/fbshare.jpg";
$cacheipad .= "\nhttp://platform.twitter.com/widgets.js";
$cachebb1 .= "\nhttp://www.launchliveapp.com/PortalImages/fbshare.jpg";
$cachebb1 .= "\nhttp://platform.twitter.com/widgets.js";
$cachebb1text .= "\nhttp://www.launchliveapp.com/PortalImages/fbshare.jpg";
$cachebb1text .= "\nhttp://platform.twitter.com/widgets.js";

$cachebbtext .= "\nhttp://www.launchliveapp.com/PortalImages/fbshare.jpg";
$cachebbtext .= "\nhttp://platform.twitter.com/widgets.js";

// Generate the html for the app pages.
while($pagesRow = mysql_fetch_array($pagesResult))
{
$configbb = $bbheader;
$pageNumber = $pagesRow["page_id"];

// Added by Zak (July 13, 2015)
// Altered the following code to store each page in its own .php file.
$page = "";
$pageipad = "";
$pagebb = "";
$pagebb1 = "";

// Track whether this page is password protected or not.
$requireLogin = $pagesLogin[array_search($pagesRow["page_id"],$pagesArray)]; // 0 = not protected, 1 = protected by login

$hidden = 'block';
if($pageNumber > 1){
	$hidden='none';
}else{
	$hidden='block';
}

$config .= "<div id='".$pageNumber."' class='page' >
			<div>
			</div>
			";

$configipad .= "<div id='".$pageNumber."' class='page' >

			";

$configbb1 .= "<div id='".$pageNumber."' style='display:$hidden;' class='page'>
			<div>
			</div>
			";

// Added by Zak (July 25, 2015)
// Add the youtube video functions to the top of every page that's password protected.
$hasYoutube = false;
$checkYoutubeQuery = "SELECT * FROM liveapp WHERE user_id=$userId AND type='youtube' AND page_number=$pageNumber ORDER BY page_number ASC, number ASC";
$checkYoutubeResult = mysql_query($checkYoutubeQuery);
$ytPageItemsCount = mysql_num_rows($checkYoutubeResult);

if($ytPageItemsCount > 0)
{
	$hasYoutube = true; // this page has at least one youtube widget on it
}

// Android/iPhone youtube code
if ($requireLogin == 1 && $hasYoutube) // don't add these functions unless the page has a youtube widget on it
{
	$ytPageItems = array();
	$ytPageItemIDs = array();

	$ii = 0;
	while ($checkYoutubeRow = mysql_fetch_array($checkYoutubeResult))
	{
		$ytPageItems[$ii] = $checkYoutubeRow["text"];
		$ytPageItemIDs[$ii] = $checkYoutubeRow["liveapp_id"];

		$ii++;
	}

	$page .= '
	<script>

	$(document).ready(function(){
		$(".youtube-holder").css("height", "200px");
		//iosYouTube'.$pageNumber.'("200", "317");
		updateOrientation'.$pageNumber.'();
	});

	function iosYouTube'.$pageNumber.'(ytheight, ytwidth){
     var uagent = navigator.userAgent.toLowerCase();
	 if(uagent.indexOf("iphone")!= -1)
	 {
	 ';
	for($j = 0; $j < $numberofyoutube; $j++)
	{
		for($k = 0; $k < $ytPageItemsCount; $k++)
		{
			if ($ytPageItemIDs[$k] == $youtubeIDs[$j])
			{
				$page .= '$("#youtube-playero'.$j.'").remove();
				';
			}
		}
	}
	for($j = 0; $j < $numberofyoutube; $j++)
	{
		for($k = 0; $k < $ytPageItemsCount; $k++)
		{
			if ($ytPageItemIDs[$k] == $youtubeIDs[$j])
			{
				$page .=	'

				$("<div class=\"youtube-overlay\" id=\"youtube-overlay'. $j .'\"><iframe class=\"youtube-iframe\" id=\"youtube-playero'.$j.'\" src=\'https://www.youtube-nocookie.com/embed/'.$youtubeItems[$j].'?html5=1\' width=\"99%\" height=\""+ ytheight + "\" style = \"z-index:100; \"></iframe></div>").appendTo("#youtube-player'.$j.'");
				$("#youtube-overlay'. $j .'").click(function() {
					$("#youtube-playero'. $j .' .html5-video-container").click();
				});
				console.log("added youtube");
				';
			}
		}
	}
	$page .=
		'}else if(uagent.indexOf("bb10") != -1){

		  ';
			for($j = 0; $j < $numberofyoutube; $j++)
			{
				for($k = 0; $k < $ytPageItemsCount; $k++)
				{
					if ($ytPageItemIDs[$k] == $youtubeIDs[$j])
					{
						$page .=	'$("#youtube-playero'.$j.'").remove();
						';
					}
				}
			}
			for($j = 0; $j < $numberofyoutube; $j++)
			{
				for($k = 0; $k < $ytPageItemsCount; $k++)
				{
					if ($ytPageItemIDs[$k] == $youtubeIDs[$j])
					{
						$page .=	'

						$("<iframe id=\"youtube-playero'.$j.'\" src=\'http://www.youtube.com/embed/'.$youtubeItems[$j].'?html5=1\' width=\"99%\" height=\""+ ytheight + "\" style = \"z-index:100\"></iframe>").appendTo("#youtube-player'.$j.'");
						console.log("added youtube");
						';
					}
				}
			}
	$page .=
	'
		}
	}

	function updateOrientation'.$pageNumber.'()
	{
	//srsbg
	//centerButtonLabels();
	 var uagent = navigator.userAgent.toLowerCase();

	 var amountYoutube ='.$ytPageItemsCount.';
	 var amountImageGallery = '.$numOfImageGallery.';

	 var tubeIDs = new Array(amountYoutube);


	 ';
	for($j = 0; $j < $numberofyoutube; $j++)
	{
		for($k = 0; $k < $ytPageItemsCount; $k++)
		{
			if ($ytPageItemIDs[$k] == $youtubeIDs[$j])
			{
				$page .= 'tubeIDs['.$k.'] ="'.$youtubeItems[$j].'";
				';
			}
		}
	}

	$page.='
	if(uagent.indexOf("android") == -1)
	{
		if(isBubble == true)
		{
			updateGoogleBubble();
		}

		var orientation = window.orientation;
		var tds = document.getElementsByTagName("td");
		var i = 0;
		var j = 0;

			switch(orientation)
			{
				case 0:
				//here
				for(j = 0; j < tds.length; j++)
				{
					var z = tds[j].height;
					var a = width;
					newz = z.replace("px", "");
					tds[j].width = (newz/height)*height;
					tds[j].height = (width/height)*newz;
				}
				if(amountImageGallery > 0)
				{';
					if($numOfImageGallery > 0)
					{
						for($u = 0; $u < $numOfImageGallery; $u++)
						{
							$page .= "updateImageGallery".$u."(0);";
						}
					}
				$page .= '
				}

				if(amountYoutube > 0 && isBubble == false)
				{
					 $(".youtube-holder").css("height", "200px");
					iosYouTube("200","317");
				}
				overlayBG.src = "http://www.launchliveapp.com/images/loading_iPhone.png";
				break;

				case 90:
				for(j = 0; j < tds.length; j++)
				{
					var z = tds[j].height;
					var a = width;
					newz = z.replace("px", "");
					tds[j].width = (newz/width)*height;
					tds[j].height = (height/width)*newz;
				}

				if(amountImageGallery > 0)
				{';
					if($numOfImageGallery > 0)
					{
						for($u = 0; $u < $numOfImageGallery; $u++)
						{
							$page .= "updateImageGallery".$u."(90);";
						}
					}
				$page .= '
				}


				if(amountYoutube > 0 && isBubble == false)
				{
					 $(".youtube-holder").css("height", "300px");
					iosYouTube("300","477");
				}
				overlayBG.src = "http://www.launchliveapp.com/images/loading_iPhone_Landscape.png";
				break;

				case -90:
				for(j = 0; j < tds.length; j++)
				{
					var z = tds[j].height;
					var a = width;
					newz = z.replace("px", "");
					tds[j].width = (newz/width)*height;
					tds[j].height = (height/width)*newz;
				}
				if(amountYoutube > 0 && isBubble == false)
				{
					 $(".youtube-holder").css("height", "200px");
					iosYouTube("200","317");
				}

				if(amountImageGallery > 0)
				{';
					if($numOfImageGallery > 0)
					{
						for($u = 0; $u < $numOfImageGallery; $u++)
						{
							$page .= "updateImageGallery".$u."(90);";
						}
					}
				$page .= '
				}
				overlayBG.src = "http://www.launchliveapp.com/images/loading_iPhone_Landscape.png";
				break;

				case 180:
				for(j = 0; j < tds.length; j++)
				{
					var z = tds[j].height;
					var a = width;
					newz = z.replace("px", "");
					tds[j].width = (newz/height)*height;
					tds[j].height = (width/height)*newz;
				}
				overlayBG.src = "http://www.launchliveapp.com/images/loading_iPhone.jpg";
				break;
			}
		}
	}

	</script>
	';

	$pageipad .= '
	<script>

	$(document).ready(function(){
		$(".youtube-holder").css("height", "200px");
		//iosYouTube'.$pageNumber.'("200", "317");
		updateOrientation'.$pageNumber.'();
	});

	function iosYouTube'.$pageNumber.'(ytheight, ytwidth){
     var uagent = navigator.userAgent.toLowerCase();

	 console.log("dogg");
	 ';
	  for($j = 0; $j < $numberofyoutube; $j++)
		{
			for($k = 0; $k < $ytPageItemsCount; $k++)
			{
				if ($ytPageItemIDs[$k] == $youtubeIDs[$j])
				{
					$pageipad .=	'$("#youtube-playero'.$j.'").remove();
					';
				}
			}
		}

	for($j = 0; $j < $numberofyoutube; $j++)
	{
		for($k = 0; $k < $ytPageItemsCount; $k++)
		{
			if ($ytPageItemIDs[$k] == $youtubeIDs[$j])
			{
				$pageipad .=	'

				$("<div class=\"youtube-overlay\" id=\"youtube-overlay'. $j .'\"><iframe class=\"youtube-iframe\" id=\"youtube-playero'.$j.'\" src=\'http://www.youtube-nocookie.com/embed/'.$youtubeItems[$j].'?html5=1\' width=\"99%\" height=\""+ ytheight + "\" style = \"z-index:100; \"></iframe></div>").appendTo("#youtube-player'.$j.'");
				$("#youtube-overlay'. $j .'").click(function() {
					$("#youtube-playero'. $j .' .html5-video-container").click();
				});
				console.log("added youtube");
				';
			}
		}
	}
	$pageipad .= '


	}

	function updateOrientation'.$pageNumber.'()
	{

	 var uagent = navigator.userAgent.toLowerCase();

	 var amountYoutube ='.$ytPageItemsCount.';
	 var amountImageGallery = '.$numOfImageGallery.';

	 var tubeIDs = new Array(amountYoutube);


	 ';
	for($j = 0; $j < $numberofyoutube; $j++)
	{
		for($k = 0; $k < $ytPageItemsCount; $k++)
		{
			if ($ytPageItemIDs[$k] == $youtubeIDs[$j])
			{
				$pageipad .= 'tubeIDs['.$k.'] ="'.$youtubeItems[$j].'";
				';
			}
		}
	}

	$pageipad .='

	if(isBubble == true)
	{
		updateGoogleBubble();
	}
		var orientation = window.orientation;
		var tds = document.getElementsByTagName("td");
		var i = 0;
		var j = 0;
		if(document.images){

			console.log("TROLOL");
			console.log(width);
			console.log(height);
		}
		switch(orientation)
		{

			case 0:
			for(j = 0; j < tds.length; j++)
			{
				var z = tds[j].height;
				var a = width;
				newz = z.replace("px", "");
				tds[j].width = (newz/height)*height;
				tds[j].height = (width/height)*newz;


			}

			if(amountYoutube > 0 && isBubble == false)
			{
				$(".youtube-holder").css("height", "485px");
				//iosYouTube("485", "765");
				iosYouTube("485", "765");
			}

			if(amountImageGallery > 0)
			{';
				if($numOfImageGallery > 0)
				{
					for($u = 0; $u < $numOfImageGallery; $u++)
					{
						$pageipad .= "updateImageGallery".$u."(0);";
					}
				}
			$pageipad .= '
			}
			break;

			case 90:
			for(j = 0; j < tds.length; j++)
			{
				var z = tds[j].height;
				var a = width;
				newz = z.replace("px", "");
				tds[j].width = (newz/width)*height;
				tds[j].height = (height/width)*newz;
			}

			if(amountYoutube > 0 && isBubble == false)
			{
				$(".youtube-holder").css("height", "680px");
				iosYouTube("680", "1021");
			}

			if(amountImageGallery > 0)
			{';
				if($numOfImageGallery > 0)
				{
					for($u = 0; $u < $numOfImageGallery; $u++)
					{
						$pageipad .= "updateImageGallery".$u."(90);";
					}
				}
			$pageipad .= '
			}
			break;

			case -90:
			for(j = 0; j < tds.length; j++)
			{
				var z = tds[j].height;
				var a = width;
				newz = z.replace("px", "");
				tds[j].width = (newz/width)*height;
				tds[j].height = (height/width)*newz;
			}
			if(amountYoutube > 0 && isBubble == false)
			{
				$(".youtube-holder").css("height", "680px");
				iosYouTube("680", "1021");
			}

			if(amountImageGallery > 0)
			{';
				if($numOfImageGallery > 0)
				{
					for($u = 0; $u < $numOfImageGallery; $u++)
					{
						$pageipad .= "updateImageGallery".$u."(90);";
					}
				}
			$pageipad .= '
			}
			break;

			case 180:
			for(j = 0; j < tds.length; j++)
			{
				var z = tds[j].height;
				var a = width;
				newz = z.replace("px", "");
				tds[j].width = (newz/height)*height;
				tds[j].height = (width/height)*newz;
			}

			if(amountImageGallery > 0)
			{';
				if($numOfImageGallery > 0)
				{
					for($u = 0; $u < $numOfImageGallery; $u++)
					{
						$pageipad .= "updateImageGallery".$u."(0);";
					}
				}
			$pageipad .= '
			}

			if(amountYoutube > 0 && isBubble == false)
			{
								$(".youtube-holder").css("height", "485px");
				iosYouTube("485", "765");
			}

			break;
		}
	}

	</script>
	';

	$pagebb1 .= '
	<script>

	$(document).ready(function(){
		$(".youtube-holder").css("height", "200px");
		updateOrientation'.$pageNumber.'();
	});

	function updateOrientation'.$pageNumber.'() {

	}

	</script>
	';
}

$query = "SELECT * FROM liveapp WHERE user_id = $userId AND page_number = $pageNumber ORDER BY number ASC, colNum ASC";

$result = mysql_query($query);
$numberOfRows = mysql_num_rows($result);

if($numberOfRows > 0)
{

	$liveQuery = "SELECT *  FROM user_settings WHERE user_id = '$userId'";
	$liveResult = mysql_query($liveQuery);
	$row2 = mysql_fetch_array($liveResult);
	$appName = $row2['app_name'];
	$iconChange = $row2['icon_change'];
	$versionNumber = $row2['version'];

	//**************************************
	// Start of the main loop for app pages
	//**************************************
    while($row = mysql_fetch_array($result))
    {
    	$liveapp_id = $row['liveapp_id'];
		$font = $row['font'];
		$size = $row['size'];
		$sizeH = $size;


		$size = $size.'px';



		$bold = $row['bold'];
		if($bold == 'on')
		{
			$bold = 'bold';
		}

		$underline = $row['underline'];
		if($underline == 'on')
		{
			$underline = 'underline';
		}

		$italic = $row['italic'];
		if($italic == 'on')
		{
			$italic = 'italic';
		}

		$color = $row['color'];
		$comment = $row['comment'];
		if($row['type'] == "textbox")
		{
			$comment = str_replace("\n","<br/>", $comment);
		}

		$textinput = $row['textinput'];
		if($textinput == "off" && ($row['type'] !== "textbox" && $row['type'] !== "html"))
		{
			$comment = "";
		}


        $locationImage = $row["location"];

        if($locationImage  != "" && ($row["type"] == "facebook" || $row["type"] == "sms" || $row["type"] == "web"
                                 || $row["type"] == "phone" || $row["type"] == "email"
                                 || $row["type"] == "youtube" || $row["type"] == "maps" || $row["type"] == "linkedin"))
        {

            $imgAction = "";
			$image = "users-folder/$folderUserName/liveapp/". $row['location'];
		    $image1 = $row["location"];

			//special case for CaymanIslandWeather, sharmeenTest and caymanweather apps
			$image2 = $image1;
			if($userId == 1452 || $userId == 1493 || $userId == 1586)
				$image1 = $image1 . ".txt";
            // IMAGE LOGIC

			//srsbg
			/*
			$imgType = substr(strrchr($image2,'.'),1);
			if($imgType == "png")
				$imageObj = @imageCreateFromPng($image);
			else
				$imageObj = @imagecreatefromjpeg($image);*/

			// Added by Zak (Dec 4, 2014)
			// Properly detects the image type, then runs the appropriate image loading function.
			$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mimetype extension
			$imgType = finfo_file($finfo, $image);
			finfo_close($finfo);

			if($imgType == "image/png")
				$imageObj = imagecreatefrompng($image);
			else
				$imageObj = imagecreatefromjpeg($image);

            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;

            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
            $imageWidth = 320*($row["item_width"]/100);
            // calculate height
            $imageHeight = ($imageWidth / $origWidth) * $origHeight;


            // MIKAEL - calculate margin
            $vMargin = (($imageHeight - 4)/2) - $sizeH/2;
          //  $vMargin = floor($vMargin);
          $vMarginbb1 = $vMargin;
            $vMargin = $vMargin."px";
             $vMarginbb1 = $vMarginbb1."px";


             // Mikael --- MArgin height for iPad
             $vIpadMarg = (51) - (0.225* $sizeH);
             $vIpadMarg = $vIpadMarg."%";


            // Mikael -- Margin height for iPhone
            $vIphoneMarg = (51) - (0.425* $sizeH);
            $vIphoneMarg = $vIphoneMarg."%";

			$align = $row['alignment'];

			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cache .= "\n".$image1;
				$cacheipad .= "\n".$image1;
				$cachebb .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$image1;
				$cachebb1 .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$image1;
				$cachebb1text .= makeTextFile($image);
				$cachebbtext .= makeTextFile($image);
			}

			//srs
			$item_width = $row['item_width'];
			$col_number = $row['colNum'];
			$number_of_cols = $row['numCols'];
			$bg_color = $row['bg_color'];
			//srs
			if($row["type"] == "web")
            {
				$link = $row["text"];
				if($link == "externallink" ||  $link == "")
				{
					$imageHeight = $imageHeight . "px";
					$web = $row["url"];
					$findme   = 'http://';
					$findmehttps = "https://";

					$pos = strpos($web, $findme);
					$poshttps = strpos($web,$findmehttps);

					if($pos === false && $poshttps === false)
					{
					$web = 'http://'.$web;
					}


					$test = getURL($web, false);

					if($number_of_cols > 1) {
						if($col_number == 0) {
							$page.= "<div class='rowHolder' style=' display:table; '>";
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$web\", \"".$test."\");'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pageipad.= "<div class='rowHolder' style=' display:table; '>";
							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$web\", \"".$test."\");'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pagebb.= "<div class='rowHolder' style=' display:table; '>";
							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a onClick = 'checkLinks(\"$web\", \"".$test."\")'>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb1.= "<div class='rowHolder' style=' display:table; '>";
							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$web\", \"".$test."\");'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
						}
						if($col_number > 0 && ($col_number < ($number_of_cols - 1))) {
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$web\", \"".$test."\");'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$web\", \"".$test."\");'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a onClick = 'checkLinks(\"$web\", \"".$test."\")'>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$web\", \"".$test."\");'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
						}
						if($col_number == ($number_of_cols - 1)) {
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$web\", \"".$test."\");'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$page.=	"</div>";
							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$web\", \"".$test."\");'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pageipad.=	"</div>";
							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a onClick = 'checkLinks(\"$web\", \"".$test."\")'>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb.=	"</div>";
							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$web\", \"".$test."\");'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
							$pagebb1.="</div>";
						}
					} else {
						$page .= "<div style='width= 100%;position:relative;' style='background-color:".$bg_color.";' onClick = 'checkLinks(\"$web\", \"".$test."\");'>
										<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size;'>
											<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;'>
												$comment
											</span>
										</div>
										<img class='buttonImage' style = 'border:none;width:100%;'  src='$image1'/>
									</div>";
						$pageipad .= "<div style='width= 100%;position:relative; background-color:".$bg_color.";'  onClick = 'checkLinks(\"$web\", \"".$test."\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;'>
													$comment
												</span>
											</div>
											<img class='buttonImage' style = 'border:none;width:100%;'  src='$image1'/>
										</div>";
						$pagebb.= "<div style='position:relative;padding:0px;margin:0px; background-color:".$bg_color.";' id = '".$row['liveapp_id']."'>
										<div align='center' style = 'position:absolute;top:40%;width:100%;margin:auto;'>
											<center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>
												$comment
											</center>
										</div>
										<a onClick = 'checkLinks(\"$web\", \"".$test."\")'>
											<img class='buttonImage' style = 'border:none;'  src='$image1' height='$imageHeight' width = '100%'/>
										</a>
									</div>";
						$pagebb1.= "<div style='width= 100%;position:relative;padding:-7px;margin-top:-7px; background-color:".$bg_color.";' onClick = 'checkLinks(\"$web\", \"".$test."\")'>
										<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
											<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;' >
												$comment
											</span>
										</div>
										<img class='buttonImage' style = 'border:none;width:100%;'  src='$image1'/>
									</div>";
					}
				}
				else // internal links are handled here
				{
					$imageHeight = $imageHeight . "px";
					if($link == 1) // 'link' stores the number of the page the image links to
					{
						$bblink = "liveapp_currentbb.php";
					}
					else
					{
						$bblink = "page".$link.".php";
					}
					if($number_of_cols > 1) {
						if($col_number == 0) {
							$page.= "<div class='rowHolder' style=' display:table; '>";
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = '".getPageForwardCall($link)."'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pageipad.= "<div class='rowHolder' style=' display:table; '>";
							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = '".getPageForwardCall($link)."'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pagebb.= "<div class='rowHolder' style=' display:table; '>";
							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a href = '".$bblink."'>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb1.= "<div class='rowHolder' style=' display:table; '>";
							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = '".getPageForwardCall($link)."'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
						}
						if($col_number > 0 && ($col_number < ($number_of_cols - 1))) {

							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = '".getPageForwardCall($link)."'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";

							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = '".getPageForwardCall($link)."'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";

							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a href = '".$bblink."'>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";

							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = '".getPageForwardCall($link)."'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
						}
						if($col_number == ($number_of_cols - 1)) {
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = '".getPageForwardCall($link)."'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$page.= "</div>";

							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = '".getPageForwardCall($link)."'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pageipad.= "</div>";

							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a href = '".$bblink."'>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb.= "</div>";

							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = '".getPageForwardCall($link)."'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
							$pagebb1.= "</div>";
						}
					} else {
						$page.= "<div  style='position:relative; background-color:".$bg_color.";' onClick = '".getPageForwardCall($link)."'>
										<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
											<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
												$comment
											</span>
										</div>
										<img class='buttonImage' style = 'border:none;width:100%'  src='$image1'/>
									</div>";
						$pageipad.= "<div style='position:relative; background-color:".$bg_color.";' onClick = '".getPageForwardCall($link)."' >
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
													$comment
												</span>
											</div>
											<img class='buttonImage' style = 'border:none;width:100%'  src='$image1'/>
										</div>";
						$pagebb.= "<div style='position:relative;padding:0px;margin:0px; background-color:".$bg_color.";'>
										<div align='center' style = 'position:absolute;top:40%;width:100%;margin:auto;'>
											<center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>
												$comment
											</center>
										</div>
										<a href = '".$bblink."'>
											<img class='buttonImage' style = 'border:none;'  src='$image1' height='$imageHeight' width = '100%'/>
										</a>
									</div>";
						$pagebb1.= "<div style='background-color:".$bg_color."; position:relative;padding:-7px;margin-top:-7px;' onClick = '".getPageForwardCall($link)."'>
										<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
											<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;' >
												$comment
											</span>
										</div>
										<img class='buttonImage' style = 'border:none;width:100%'  src='$image1'/>
									</div>";
					}
				}
            }
			//srs
            if($row["type"] == "email")
            {

				$email = $row["email"];
				$imageHeight = $imageHeight . "px";
				if($number_of_cols > 1) {
						if($col_number == 0) {
							$page.= "<div class='rowHolder' style=' display:table; '>";
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$email\", \"2\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pageipad.= "<div class='rowHolder' style=' display:table; '>";
							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$email\", \"2\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pagebb.= "<div class='rowHolder' style='display:table; '>";
							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a href=mailto:'$email'>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb1.= "<div class='rowHolder' style=' display:table; '>";
							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$email\", \"2\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
						}
						if($col_number > 0 && ($col_number < ($number_of_cols - 1))) {

							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$email\", \"2\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";

							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$email\", \"2\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";

							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a href=mailto:'$email'>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";

							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$email\", \"2\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";

						}
						if($col_number == ($number_of_cols - 1)) {
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$email\", \"2\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$page.= "</div>";

							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$email\", \"2\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pageipad.= "</div>";

							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a href=mailto:'$email'>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb.= "</div>";

							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$email\", \"2\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
							$pagebb1.= "</div>";
						}
				} else {
					$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative;' onClick = 'checkLinks(\"$email\", \"2\")'>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
										<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
											$comment
										</span>
									</div>
									<img class='buttonImage' style = 'border:none;width:100%'  src='$image1'/>
								</div>";
					$pageipad.= "<div style='background-color:".$bg_color."; position:relative;' onClick = 'checkLinks(\"$email\", \"2\")'>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
										<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
											$comment
										</span>
									</div>
									<img class='buttonImage' style = 'border:none;width:100%'  src='$image1'/>
								</div>";
					$pagebb.= "<div style='background-color:".$bg_color."; position:relative;padding:0px;margin:0px;'>
									<div align='center' style = 'position:absolute;top:40%;width:100%;margin:auto;'>
										<center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
											$comment
										</center>
									</div>
									<a href=mailto:'$email'>
										<img class='buttonImage' style = 'border:none;'  src='$image1' height='$imageHeight' width = '100%'/>
									</a>
								</div>";
					$pagebb1.= "<div style='background-color:".$bg_color."; position:relative;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$email\", \"2\")'>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
										<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
											$comment
										</span>
									</div>
									<img class='buttonImage' style = 'border:none;width:100%'  src='$image1'/>
								</div>";
				}
            }
			//srs
            if($row["type"] == "sms")
            {
				$imageHeight = $imageHeight . "px";

				$sms = $row["sms_number"];
				if($number_of_cols > 1) {
						if($col_number == 0) {
							$page.= "<div class='rowHolder' style=' display:table; '>";
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a href=sms:$sms>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";
							$pageipad.= "<div class='rowHolder' style=' display:table; '>";
							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a href=sms:$sms>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";
							$pagebb.= "<div class='rowHolder' style=' display:table; '>";
							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a href=sms:$sms>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb1.= "<div class='rowHolder' style=' display:table; '>";
							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$sms\", \"3\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
						}
						if($col_number > 0 && ($col_number < ($number_of_cols - 1))) {
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a href=sms:$sms>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";

							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a href=sms:$sms>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";

							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a href=sms:$sms>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";

							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$sms\", \"3\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
						}
						if($col_number == ($number_of_cols - 1)) {
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a href=sms:$sms>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";
							$page.= "</div>";

							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a href=sms:$sms>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";
							$pageipad.= "</div>";

							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a href=sms:$sms>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb.= "</div>";

							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$sms\", \"3\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
							$pagebb1.= "</div>";
						}
				} else {
					$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative;' >
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
										<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
											$comment
										</span>
									</div>
									<a href=sms:$sms>
										<img class='buttonImage' style = 'border:none;width:100%'  src='$image1' />
									</a>
								</div>";
					$pageipad.= "<div style='background-color:".$bg_color."; position:relative;' onClick = 'checkLinks(\"$sms\", \"3\")'>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
										<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
											$comment
										</span>
									</div>
									<a href=sms:$sms>
										<img class='buttonImage' style = 'border:none;width:100%'  src='$image1' />
									</a>
								</div>";
					$pagebb.= "<div style='background-color:".$bg_color."; position:relative; padding:0px;margin:0px;'>
									<div align='center' style = 'position:absolute;top:40%;width:100%;margin:auto;'>
										<center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
											$comment
										</center>
									</div>
									<a href=sms:$sms>
										<img class='buttonImage' style = 'border:none;'  src='$image1' height='$imageHeight' width = '100%'/>
									</a>
								</div>";
					$pagebb1.= "<div style='background-color:".$bg_color."; position:relative; padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$sms\", \"3\")'>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
										<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
											$comment
										</span>
									</div>
									<img class='buttonImage' style = 'border:none;width:100%'  src='$image1' />
								</div>";
				}
			}
			//srs
            else if($row["type"] == "phone")
            {
				$imageHeight = $imageHeight . "px";

				$number = $row["phone_number"];
				if($number_of_cols > 1) {
						if($col_number == 0) {
							$page.= "<div class='rowHolder' style=' display:table; '>";
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a href=tel:$number>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";
							$pageipad.= "<div class='rowHolder' style=' display:table; '>";
							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a href=tel:$number>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";
							$pagebb.= "<div class='rowHolder' style=' display:table; '>";
							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a href=tel:$number>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb1.= "<div class='rowHolder' style=' display:table; ' >";
							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$number\", \"4\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
						}
						if($col_number > 0 && ($col_number < ($number_of_cols - 1))) {

							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a href=tel:$number>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";

							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a href=tel:$number>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";

							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a href=tel:$number>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";

							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$number\", \"4\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
						}
						if($col_number == ($number_of_cols - 1)) {

							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a href=tel:$number>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";
							$page.= "</div>";

							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a href=tel:$number>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";
							$pageipad.= "</div>";

							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a href=tel:$number>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb.= "</div>";

							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$number\", \"4\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
										</div>";
							$pagebb1.= "</div>";
						}
				} else {
					$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative;' >
								<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
									<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;' >
										$comment
									</span>
								</div>
								<a href=tel:$number>
									<img class='buttonImage' style = 'border:none;width:100%;'  src='$image1'/>
								</a>
							</div>";
				$pageipad.= "<div style='background-color:".$bg_color."; position:relative;' onClick = 'checkLinks(\"$number\", \"4\")'>
							<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
								<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
									$comment
								</span>
							</div>
						     <a href=tel:$number>
							<img class='buttonImage' style = 'border:none;width:100%;'  src='$image1'/>
						      </a>
							</div>";
				$pagebb.= "<div style='background-color:".$bg_color."; position:relative;padding:0px;margin:0px;'>
							<div align='center' style = 'position:absolute;top:40%;width:100%;margin:auto;'>
								<center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
									$comment
								</center>
							</div>
							<a href=tel:$number>
							<img class='buttonImage' style = 'border:none;'  src='$image1' height='$imageHeight' width = '100%'/>
							</a>
							</div>";
				$pagebb1.= "<div style='background-color:".$bg_color."; position:relative;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$number\", \"4\")'>
							<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
								<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
									$comment
								</span>
							</div>

							<img class='buttonImage' style = 'border:none;width:100%;'  src='$image1'/>

							</div>";
				}
            }

			// srs
            if($row["type"] == "maps")
            {


			$street = $row["street"];
			$city = $row["city"];
			$zip = $row["zip"];
			$state = $row["state"];
			$imageHeight = $imageHeight . "px";
			$loc = "http://maps.google.com/maps?q=$street+$city+$state+$zip";
			$loc2 = "geo:0,0?q=$street+$city+$state+$zip";
			if($number_of_cols > 1) {
						if($col_number == 0) {
							$page.= "<div class='rowHolder' style=' display:table; '>";
							//srs_map
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'showGoogleMap(\"$loc2\", \"$loc\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";
							$pageipad.= "<div class='rowHolder' style=' display:table; '>";
							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$loc\", \"1\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pagebb.= "<div class='rowHolder' style=' display:table; '>";
							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;' onClick = 'checkLinks(\"$loc\", \"1\")'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a onClick = 'checkLinks(\"$loc\", \"1\")'>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb1.= "<div class='rowHolder' style=' display:table; ' >";
							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$loc\", \"1\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<a onClick = 'checkLinks(\"$loc\", \"1\")'>
												<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
											</a>
										</div>";
						}
						if($col_number > 0 && ($col_number < ($number_of_cols - 1))) {

							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'showGoogleMap(\"$loc2\", \"$loc\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";

							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$loc\", \"1\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";

							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;' onClick = 'checkLinks(\"$loc\", \"1\")'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a onClick = 'checkLinks(\"$loc\", \"1\")'>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";

							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$loc\", \"1\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<a onClick = 'checkLinks(\"$loc\", \"1\")'>
												<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
											</a>
										</div>";

						}
						if($col_number == ($number_of_cols - 1)) {
							$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'showGoogleMap(\"$loc2\", \"$loc\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<a>
												<img class='buttonImage' src='$image1' style='width:100%;'/>
											</a>
										</div>";
							$page.= "</div>";

							$pageipad.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;' onClick = 'checkLinks(\"$loc\", \"1\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
												<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
											</div>
											<img class='buttonImage' src='$image1' style='width:100%;'/>
										</div>";
							$pageipad.= "</div>";

							$pagebb.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;' onClick = 'checkLinks(\"$loc\", \"1\")'>
											<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
											<a onClick = 'checkLinks(\"$loc\", \"1\")'>
												<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
											</a>
										</div>";
							$pagebb.= "</div>";

							$pagebb1.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$loc\", \"1\")'>
											<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
											<a onClick = 'checkLinks(\"$loc\", \"1\")'>
												<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
											</a>
										</div>";
							$pagebb1.= "</div>";

						}
				} else {
					$page.= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative;' onClick = 'showGoogleMap(\"$loc2\", \"$loc\")'>
							<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
								<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'  >
									$comment
								</span>
							</div>
							<a>
							<img class='buttonImage' style = 'border:none;width:100%;'  src='$image1' />
							</a>
							</div>";
					$pageipad.= "<div style='background-color:".$bg_color."; position:relative;' onClick = 'checkLinks(\"$loc\", \"1\")'>
							<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
								<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
									$comment
								</span>
							</div>

							<img class='buttonImage' style = 'border:none;width:100%;'  src='$image1' />

							</div>";
					$pagebb.= "<div style='background-color:".$bg_color."; position:relative;padding:0px;margin:0px;' onClick = 'checkLinks(\"$loc\", \"1\")'>
							<div align='center' style = 'position:absolute;top:40%;width:100%;margin:auto;'>
								<center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
									$comment
								</center>
							</div>
							<a onClick = 'checkLinks(\"$loc\", \"1\")'>
							<img class='buttonImage' style = 'border:none;'  src='$image1' height='$imageHeight' width = '100%'/>
							</a>
							</div>";
					$pagebb1.= "<div style='background-color:".$bg_color."; position:relative;padding:-7px;margin-top:-7px;' onClick = 'checkLinks(\"$loc\", \"1\")'>
							<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
								<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>
									$comment
								</span>
							</div>
							<a onClick = 'checkLinks(\"$loc\", \"1\")'>
							<img class='buttonImage' style = 'border:none;width:100%;'  src='$image1' />
							</a>
							</div>";
				}
            }
        }
		// separated it from img - SRS
		else if ($row["type"] == "header" || $row["type"] == "footer") {
			$image = "users-folder/$folderUserName/liveapp/". $row['location'];
			$image1 = $row['location'];
            // IMAGE LOGIC
            $imageObj = @imagecreatefromjpeg($image);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
            $imageWidth = 320*($row["hor_width"]/100);
            $imageHeight = ($imageWidth / $origWidth) * $origHeight;
			$imageHeight = $imageHeight . "px";


            // MIKAEL - calculate margin
            $vMargin = (($imageHeight - 4)/2) - $sizeH/2;
          //  $vMargin = floor($vMargin);
          $vMarginbb1 = $vMargin;
            $vMargin = $vMargin."px";
             $vMarginbb1 = $vMarginbb1."px";


             // Mikael --- MArgin height for iPad
             $vIpadMarg = (51) - (0.225* $sizeH);
             $vIpadMarg = $vIpadMarg."%";


            // Mikael -- Margin height for iPhone
            $vIphoneMarg = (51) - (0.425* $sizeH);
            $vIphoneMarg = $vIphoneMarg."%";


            $placeW = (320- $imageWidth);
			$align = $row["alignment"];
			$page .= "<div class='buttonHolder' style='position:relative;'>
						<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
						<img class='buttonImage' src='$image1' style='width:100%;'/>
						</div>";
			 $pageipad .= "<div style='position:relative;'>
						<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
						<img src='$image1' style='width:100%;'/>
						</div>";
			$pagebb .= "<div style='position:relative;padding:0px;margin:0px;'>
						<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
						<img src='$image1' height='$imageHeight' width = '100%' />
						</div>";
			$pagebb1 .= "<div style='position:relative;padding:-7px;margin-top:-7px;'>
						<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
						<img class='buttonImage' src='$image1' style='width:100%;'/>
						</div>";

			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cache .= "\n".$image1;
				$cacheipad .= "\n".$image1;
				$cachebb .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$image1;
				$cachebb1 .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$image1;
				$cachebb1text .= makeTextFile($image);
				$cachebbtext .= makeTextFile($image);
			}

		}
        // srs_image
		else if($row["type"] == "image" && $row['location'] == '' && $row['numCols'] > 1) {
			$col_number = $row['colNum'];
			$number_of_cols = $row['numCols'];
			$item_width = $row['item_width'];
			$bg_color = $row['bg_color'];
			if($number_of_cols > 1) {
				if($col_number == 0) {
					$page .= "<div class='rowHolder' style=' display:table; '>
									<div style = 'width:".$item_width."%; background-color:".$bg_color.";display:table-cell; vertical-align:top;'></div>";
					$pageipad .= "<div class='rowHolder' style=' display:table; '>
									<div style = 'width:".$item_width."%; background-color:".$bg_color.";display:table-cell; vertical-align:top;'></div>";
					$pagebb .= "<div class='rowHolder' style=' display:table; '>
									<div style = 'width:".$item_width."%; background-color:".$bg_color.";display:table-cell; vertical-align:top;'></div>";
					$pagebb1 .= "<div class='rowHolder' style=' display:table; '>
									<div style = 'width:".$item_width."%; background-color:".$bg_color.";display:table-cell; vertical-align:top;'></div>";
				}
				if($col_number > 0 && ($col_number < ($number_of_cols - 1))) {
					$page .= "<div align='center' style = 'width:".$item_width."%; background-color:".$bg_color."; display:table-cell;vertical-align:top;'></div>";
					$pageipad .= "<div align='center' style = 'width:".$item_width."%; background-color:".$bg_color."; display:table-cell;vertical-align:top;'></div>";
					$pagebb .= "<div align='center' style = 'width:".$item_width."%; background-color:".$bg_color."; display:table-cell;vertical-align:top;'></div>";
					$pagebb1 .= "<div align='center' style = 'width:".$item_width."%; background-color:".$bg_color."; display:table-cell;vertical-align:top;'></div>";

				}
				if($col_number == ($number_of_cols - 1)) {
						$page .= " <div align='center' style = 'width:".$item_width."%; background-color:".$bg_color."; display:table-cell;vertical-align:top; '></div>
								</div>";
						$pageipad .= " <div align='center' style = 'width:".$item_width."%; background-color:".$bg_color."; display:table-cell;vertical-align:top; '></div>
							</div>";
						$pagebb .= " <div align='center' style = 'width:".$item_width."%; background-color:".$bg_color."; display:table-cell;vertical-align:top; '></div>
							</div>";
						$pagebb1 .= " <div align='center' style = 'width:".$item_width."%; background-color:".$bg_color."; display:table-cell;vertical-align:top; '></div>
							</div>";
				}
			}
		}
		else if($row["type"] == "image" && $row['location'] != '') {

			$image = "users-folder/$folderUserName/liveapp/". $row['location'];
			$image1 = $row['location'];
			//special case for CaymanIslandWeather, sharmeenTest and caymanweather apps
			$image2 = $image1;
			if($userId == 1452 || $userId == 1493 || $userId == 1586)
				$image1 = $image1 . ".txt";

			 // IMAGE LOGIC

			//srsbg
			/*
			$imgType = substr(strrchr($image2,'.'),1);
			if($imgType == "png")
				$imageObj = @imageCreateFromPng($image);
			else
				$imageObj = @imagecreatefromjpeg($image);
			*/

			// Added by Zak (Dec 4, 2014)
			// Properly detects the image type, then runs the appropriate image loading function.
			$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mimetype extension
			$imgType = finfo_file($finfo, $image);
			finfo_close($finfo);

			if($imgType == "image/png")
				$imageObj = imagecreatefrompng($image);
			else
				$imageObj = imagecreatefromjpeg($image);

            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;

            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
            $imageWidth = 320*($row["item_width"]/100); // not sure why 320?
            $imageHeight = ($imageWidth / $origWidth) * $origHeight;
			$imageHeight = $imageHeight . "px";

			//item height for portal 380px


            // MIKAEL - calculate margin
            $vMargin = (($imageHeight - 4)/2) - $sizeH/2;
			// $vMargin = floor($vMargin);
			$vMarginbb1 = $vMargin;
            $vMargin = $vMargin."px";
            $vMarginbb1 = $vMarginbb1."px";

             // Mikael --- MArgin height for iPad
            $vIpadMarg = (51) - (0.225* $sizeH);
            $vIpadMarg = $vIpadMarg."%";

            // Mikael -- Margin height for iPhone
            $vIphoneMarg = (51) - (0.425* $sizeH);
            $vIphoneMarg = $vIphoneMarg."%";

			$item_width = $row['item_width'];

            $placeW = (320- $imageWidth);
			$align = $row["alignment"];

			$col_number = $row['colNum'];
			$number_of_cols = $row['numCols'];
			$bg_color = $row['bg_color'];

			if($number_of_cols > 1) {
				if($col_number == 0) {
					$page .= "<div class='rowHolder' style=' display:table; '>";
					$page .= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."px; display:table-cell;vertical-align:top; '>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
										<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
									</div>
									<img class='buttonImage' src='$image1' style='width:100%;'/>
								</div>";
					$pageipad .= "<div class='rowHolder' style=' display:table; '>";
					$pageipad .= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
										<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
									</div>
									<img class='buttonImage' src='$image1' style='width:100%;'/>
								</div>";
					$pagebb .= "<div class='rowHolder' style=' display:table; '>";
					$pagebb .= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
									<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
									<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
								</div>";
					$pagebb1 .= "<div class='rowHolder' style=' display:table; '>";
					$pagebb1 .= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;'>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
									<img class='buttonImage' src='$image1' style='width:100%; display:block;'/>
								</div>";
				}

				if($col_number > 0 && ($col_number < ($number_of_cols - 1))) {
					$page .= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top; '>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
										<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
									</div>
									<img class='buttonImage' src='$image1' style='width:100%;'/>
								</div>";
					$pageipad .= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
										<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
									</div>
									<img class='buttonImage' src='$image1' style='width:100%;'/>
								</div>";
					$pagebb .= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
									<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
									<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block;' />
								</div>";
					$pagebb1 .= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;'>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
									<img class='buttonImage' src='$image1' style='width:100%;display:block;'/>
								</div>";

				}
				if($col_number == ($number_of_cols - 1)) {
					$page .= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top; '>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
										<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
									</div>
									<img class='buttonImage' src='$image1' style='width:100%;'/>
								</div>";
					$page .=	"</div>";
					$pageipad .= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;'>
										<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
											<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
										</div>
										<img class='buttonImage' src='$image1' style='width:100%;'/>
									</div>";
					$pageipad .=	"</div>";
					$pagebb .= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:0px;margin:0px;'>
									<div style = 'position:absolute;top:40%;width:100%;margin:auto'>
										<center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center>
									</div>
									<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' style='display:block' />
								</div>";
					$pagebb .= "</div>";
					$pagebb1 .= "<div class='buttonHolder' style='background-color:".$bg_color."; position:relative; width:".$item_width."%; display:table-cell;vertical-align:top;padding:-7px;margin-top:-7px;'>
									<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
										<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
									</div>
									<img class='buttonImage' class='buttonImage' src='$image1' style='width:100%;display:block;'/>
								</div>";
					$pagebb1 .= "</div>";
				}
			} else {

				$page .= "<div class='buttonHolder' style='position:relative; background-color:".$bg_color.";'>
								<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
									<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
								</div>
								<img class='buttonImage' src='$image1' style='width:100%;'/>
							</div>";
				$pageipad .= "<div style='position:relative; background-color:".$bg_color.";'>
								<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'>
									<span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span>
								</div>
								<img class='buttonImage' src='$image1' style='width:100%;'/>
							</div>";
				$pagebb .= "<div style='position:relative;padding:0px;margin:0px; background-color:".$bg_color.";'>
								<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></div>
								<img class='buttonImage' src='$image1' height='$imageHeight' width = '100%' />
							</div>";
				$pagebb1 .= "<div style='position:relative;padding:-7px;margin-top:-7px; background-color:".$bg_color.";'>
								<div align='center' style = 'position:absolute;width:100%;top:40%;font-size:$size'><span class='buttonText' style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;'>$comment</span></div>
								<img class='buttonImage' src='$image1' style='width:100%;'/>
							</div>";
			}

			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cache .= "\n".$image1;
				$cacheipad .= "\n".$image1;
				$cachebb .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$image1;
				$cachebb1 .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$image1;
				$cachebb1text .= makeTextFile($image);
				$cachebbtext .= makeTextFile($image);
			}

        }
		else if($row["type"] == "audio" && $paid == 1)
		{
		    $audioCounter += 1;
		    /*
			$location = $row['location'];
			$source = "http://anilmehta.powweb.com/LiveAppPlayer_UI2.jpg";
			$imageObj = @imagecreatefromjpeg($source);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
			$imageWidth = 320;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
            $imageHeight = ($imageWidth / $origWidth) * $origHeight;
			$imageHeight = $imageHeight . "px";

			*/

			$page .= '<div id="'.$audioArray[$audioCounter - 1][0].'" class="jp-jplayer"></div>
    <div id="'.$audioArray[$audioCounter - 1][3].'" class="jp-audio">
    <div class="jp-type-single">
	 <div class="jp-title" style = "background-size:100% 100%">
        <ul>
          <li>'.$audioArray[$audioCounter - 1][2].'</li>
        </ul>
      </div>
      <div class="jp-gui jp-interface">
        <ul class="jp-controls">
          <li><a href="javascript:;" onclick="featureNot();" class="jp-play" tabindex="1">play</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-pause" tabindex="1">pause</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-stop" tabindex="1">stop</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-mute" tabindex="1" title="mute">mute</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
        </ul>
        <div class="jp-progress">
          <div class="jp-seek-bar">
            <div class="jp-play-bar"></div>
          </div>
        </div>
     <!--   <div class="jp-volume-bar">
          <div class="jp-volume-bar-value"></div>
        </div> -->
        <div class="jp-time-holder">
          <div class="jp-current-time"></div>
          <div class="jp-duration"></div>
          <ul class="jp-toggles">
            <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
            <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
          </ul>
        </div>
      </div>


    </div>
  </div>


';

	$pageipad .= '<div id="'.$audioArray[$audioCounter - 1][0].'" class="jp-jplayer"></div>
    <div id="'.$audioArray[$audioCounter - 1][3].'" class="jp-audio">
    <div class="jp-type-single">
	 <div class="jp-title" style = "background-size:100% 100%">
        <ul>
          <li>'.$audioArray[$audioCounter - 1][2].'</li>
        </ul>
      </div>
      <div class="jp-gui jp-interface">
        <ul class="jp-controls">
          <li><a href="javascript:;" onclick="featureNot();" class="jp-play" tabindex="1">play</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-pause" tabindex="1">pause</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-stop" tabindex="1">stop</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-mute" tabindex="1" title="mute">mute</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
        </ul>
        <div class="jp-progress">
          <div class="jp-seek-bar">
            <div class="jp-play-bar"></div>
          </div>
        </div>
        <div class="jp-volume-bar">
          <div class="jp-volume-bar-value"></div>
        </div>
        <div class="jp-time-holder">
          <div class="jp-current-time"></div>
          <div class="jp-duration"></div>
          <ul class="jp-toggles">
            <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
            <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
          </ul>
        </div>
      </div>


    </div>
  </div>


';
			$pagebb.= '<div style="position:relative;padding:0px;margin:0px;">
				<div id="'.$audioArray[$audioCounter - 1][0].'" class="jp-jplayer"></div>
    <div id="'.$audioArray[$audioCounter - 1][3].'" class="jp-audio">
    <div class="jp-type-single">
	 <div class="jp-title">
        <ul>
          <li>'.$audioArray[$audioCounter - 1][2].'</li>
        </ul>
      </div>
      <div class="jp-gui jp-interface">
        <ul class="jp-controls">
          <li><a href="javascript:;" onclick="featureNot();" class="jp-play" tabindex="1">play</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-pause" tabindex="1">pause</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-stop" tabindex="1">stop</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-mute" tabindex="1" title="mute">mute</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
        </ul>
        <div class="jp-progress">
          <div class="jp-seek-bar">
            <div class="jp-play-bar"></div>
          </div>
        </div>
        <div class="jp-volume-bar">
          <div class="jp-volume-bar-value"></div>
        </div>
        <div class="jp-time-holder">
          <div class="jp-current-time"></div>
          <div class="jp-duration"></div>
          <ul class="jp-toggles">
            <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
            <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
          </ul>
        </div>
      </div>


    </div>
  </div>



						</div>';


			$pagebb1.= '<div style="position:relative;padding:-7px;margin-top:-7px;">
						<div id="'.$audioArray[$audioCounter - 1][0].'" class="jp-jplayer"></div>
    <div id="'.$audioArray[$audioCounter - 1][3].'" class="jp-audio">
    <div class="jp-type-single">
	 <div class="jp-title" style = "background-size:100% 100%">
        <ul>
          <li>'.$audioArray[$audioCounter - 1][2].'</li>
        </ul>
      </div>
      <div class="jp-gui jp-interface">
        <ul class="jp-controls">
          <li><a href="javascript:;" onclick="featureNot();" class="jp-play" tabindex="1">play</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-pause" tabindex="1">pause</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-stop" tabindex="1">stop</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-mute" tabindex="1" title="mute">mute</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
          <li><a href="javascript:;" onclick="featureNot();" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
        </ul>
        <div class="jp-progress">
          <div class="jp-seek-bar">
            <div class="jp-play-bar"></div>
          </div>
        </div>
        <div class="jp-volume-bar">
          <div class="jp-volume-bar-value"></div>
        </div>
        <div class="jp-time-holder">
          <div class="jp-current-time"></div>
          <div class="jp-duration"></div>
          <ul class="jp-toggles">
            <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
            <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
          </ul>
        </div>
      </div>


    </div>
  </div>



						</div>';


		}
		else if($row["type"] == "video" && $paid == 1)
		{
		    $videoCount += 1;
			$location = $row['location'];
			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cache .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$row["liveapp_id"].".jpg";
				$cachebb1 .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$row["liveapp_id"].".jpg";
				$cachebb1text .= makeTextFile( "users-folder/".$folderUserName."/liveapp/".$row["liveapp_id"].".jpg");
				$cachebbtext .= makeTextFile( "users-folder/".$folderUserName."/liveapp/".$row["liveapp_id"].".jpg");
				$cacheipad .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$row["liveapp_id"].".jpg";
			}

			$page .= '
			<div id="'.$videoArray[$videoCount - 1][3].'" class="jp-video ">
			 <div class="jp-title">
            <ul>
              <li>'.$videoArray[$videoCount - 1][2].'</li>
            </ul>
          </div>
    <div class="jp-type-single">
      <div id="'.$videoArray[$videoCount - 1][0].'" class="jp-jplayer"></div>
      <div class="jp-gui">

        <div class="jp-video-play">
          <a href="javascript:;" class="jp-video-play-icon" tabindex="1">play</a>
        </div>
        <div class="jp-interface">
          <div class="jp-progress">
            <div class="jp-seek-bar">
              <div class="jp-play-bar"></div>
            </div>
          </div>
          <div class="jp-current-time"></div>
          <div class="jp-duration"></div>
          <div class="jp-controls-holder">
            <ul class="jp-controls">
              <li><a href="javascript:;" onclick="featureNot();" class="jp-play" tabindex="1">play</a></li>
              <li><a href="javascript:;" onclick="featureNot();" class="jp-pause" tabindex="1">pause</a></li>
              <li><a href="javascript:;" onclick="featureNot();"  class="jp-stop" tabindex="1">stop</a></li>
              <li><a href="javascript:;" onclick="featureNot();" class="jp-mute" tabindex="1" title="mute">mute</a></li>
              <li><a href="javascript:;" onclick="featureNot();" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
              <li><a href="javascript:;" onclick="featureNot();" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
            </ul>
            <div class="jp-volume-bar">
              <div class="jp-volume-bar-value"></div>
            </div>
            <ul class="jp-toggles">
              <li><a href="javascript:;" class="jp-full-screen" tabindex="1" title="full screen">full screen</a></li>
              <li><a href="javascript:;" class="jp-restore-screen" tabindex="1" title="restore screen">restore screen</a></li>
              <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
              <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
            </ul>
          </div>

        </div>
      </div>

    </div>
  </div>

  ';

	$pageipad .= '
			<div id="'.$videoArray[$videoCount - 1][3].'" class="jp-video ">
			 <div class="jp-title">
            <ul>
              <li>'.$videoArray[$videoCount - 1][2].'</li>
            </ul>
          </div>
    <div class="jp-type-single">
      <div id="'.$videoArray[$videoCount - 1][0].'" class="jp-jplayer"></div>
      <div class="jp-gui">

        <div class="jp-video-play">
          <a href="javascript:;" class="jp-video-play-icon" tabindex="1">play</a>
        </div>
        <div class="jp-interface">
          <div class="jp-progress">
            <div class="jp-seek-bar">
              <div class="jp-play-bar"></div>
            </div>
          </div>
          <div class="jp-current-time"></div>
          <div class="jp-duration"></div>
          <div class="jp-controls-holder">
            <ul class="jp-controls">
              <li><a href="javascript:;" onclick="featureNot();" class="jp-play" tabindex="1">play</a></li>
              <li><a href="javascript:;" onclick="featureNot();" class="jp-pause" tabindex="1">pause</a></li>
              <li><a href="javascript:;" onclick="featureNot();" class="jp-stop" tabindex="1">stop</a></li>
              <li><a href="javascript:;" onclick="featureNot();" class="jp-mute" tabindex="1" title="mute">mute</a></li>
              <li><a href="javascript:;" onclick="featureNot();" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
              <li><a href="javascript:;" onclick="featureNot();" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
            </ul>
            <div class="jp-volume-bar">
              <div class="jp-volume-bar-value"></div>
            </div>
            <ul class="jp-toggles">
              <li><a href="javascript:;" class="jp-full-screen" tabindex="1" title="full screen">full screen</a></li>
              <li><a href="javascript:;" class="jp-restore-screen" tabindex="1" title="restore screen">restore screen</a></li>
              <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
              <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
            </ul>
          </div>

        </div>
      </div>

    </div>
  </div>

  ';
			$pagebb.= '<div style="position:relative;background-image:url(http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/'.$row["liveapp_id"].'.jpg);background-size:100% 100%;height:300px" height = "300px" onclick = "video.play()">
			</div>';

	  if($videoArray[$videoCount - 1][4] == "")
      {
	  $videoURL = "http://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$videoArray[$videoCount - 1][1];
	  //$videoURL = "";
	  } else {
	  $videoURL = $videoArray[$videoCount - 1][4];
	  }

			$pagebb1.= '<div style="position:relative;padding:-7px;margin-top:-7px;background-image:url('.$row["liveapp_id"].'.jpg);background-size:100% 100%;height:300px" height = "300px" onclick = "youtube.play('.$videoURL.')">

			</div>';
		}

        else if($row["type"] == "textbox")
        {

			$bgColor = $row['background_color'];

				$page.= "
			<div style = 'position:relative;background-color:$bgColor;padding:-10px;margin-top:-10px;height:100px' height = '100px'>
			<p style = 'font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;margin:auto;padding:10px 10px 10px 10px;'>$comment</p>
			</div>"	;
			$pageipad.= "
			<div style = 'position:relative;background-color:$bgColor;padding:-7px;margin-top:-7px;height:100px' height = '100px'>
			<p style = 'font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;margin:auto;padding:10px 10px 10px 10px;'>$comment</p>
			</div>"	;

			$pagebb.= "
			<div style = 'position:relative;background-color:$bgColor;height:100px' height = '100px'>
			<p style = 'font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;margin:auto;padding:10px 10px 10px 10px;'>$comment</p>
			</div>"	;
			$pagebb1.= "
			<div style = 'position:relative;background-color:$bgColor;padding:-7px;margin-top:-7px;height:100px' height = '100px'>
			<p style = 'font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;margin:auto;padding:10px 10px 10px 10px;'>$comment</p>
			</div>"	;


		}
		else if($row['type'] == "filebrowser"){
			$url = $row['url'];
			$style = $row['style'];
			$pos = strpos($url, "FTP/");
			$baseDir = substr($url, 0, $pos+4);
			$dirName = substr($url, $pos+4, strlen($url)-1);
			$page.=
				'<div id="'.$liveapp_id.'" class="filemanager"></div>'.
				'<script type="text/javascript" src="//www.launchliveapp.com/CuteFileBrowser/assets/js/script.js"></script>'.
				'<link rel="stylesheet" type="text/css" href="//www.launchliveapp.com/CuteFileBrowser/assets/css/styles.css">'.
				'<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">'.
				'<script type="text/javascript">'.
					'fileBrowserInitialize("'.$liveapp_id.'", "'.$baseDir.'", "'.$style.'");'.
					'fileBrowserScan("'.$liveapp_id.'", "'.$dirName.'");'.
				'</script>';
            $pageipad.=
            	'<div id="'.$liveapp_id.'" class="filemanager"></div>'.
				'<script type="text/javascript" src="//www.launchliveapp.com/CuteFileBrowser/assets/js/script.js"></script>'.
				'<link rel="stylesheet" type="text/css" href="//www.launchliveapp.com/CuteFileBrowser/assets/css/styles.css">'.
				'<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">'.
				'<script type="text/javascript">'.
					'fileBrowserInitialize("'.$liveapp_id.'", "'.$baseDir.'", "'.$style.'");'.
					'fileBrowserScan("'.$liveapp_id.'", "'.$dirName.'");'.
				'</script>';
            $pagebb.=
            	'<div id="'.$liveapp_id.'" class="filemanager"></div>'.
				'<script type="text/javascript" src="//www.launchliveapp.com/CuteFileBrowser/assets/js/script.js"></script>'.
				'<link rel="stylesheet" type="text/css" href="//www.launchliveapp.com/CuteFileBrowser/assets/css/styles.css">'.
				'<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">'.
				'<script type="text/javascript">'.
					'fileBrowserInitialize("'.$liveapp_id.'", "'.$baseDir.'", "'.$style.'");'.
					'fileBrowserScan("'.$liveapp_id.'", "'.$dirName.'");'.
				'</script>';
            $pagebb1.=
            	'<div id="'.$liveapp_id.'" class="filemanager"></div>'.
				'<script type="text/javascript" src="//www.launchliveapp.com/CuteFileBrowser/assets/js/script.js"></script>'.
				'<link rel="stylesheet" type="text/css" href="//www.launchliveapp.com/CuteFileBrowser/assets/css/styles.css">'.
				'<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">'.
				'<script type="text/javascript">'.
					'fileBrowserInitialize("'.$liveapp_id.'", "'.$baseDir.'", "'.$style.'");'.
					'fileBrowserScan("'.$liveapp_id.'", "'.$dirName.'");'.
				'</script>';
        }
        else if($row["type"] == "html")
        {
			$page .= "<tr><td>$comment</td></tr>";
			$pageipad .= "<tr><td>$comment</td></tr>";

			$pagebb.= "
			<div>
			$comment
			</div>"	;
			$pagebb1.= "
			<div>
			$comment
			</div>"	;
        }

		else if($row["type"] == "photogallery")
		{
			 $arrows = $row['mode'];
		     $titleOn = $row["style"];
			 $galleryID = $row["liveapp_id"];


			 $ipadratio = (1024/764);
			 $iphoneratio = (480/320);

			 $tallest_img = $tallestimage[$totalgalleries];

			 $height = ((320/380)*$galleryheight[$totalgalleries]);
			 $arrowMargin = $height/2 - 30.5;
			 $ipadheight = ((768/380)*$galleryheight[$totalgalleries]);
			 $arrowIpadMargin = $ipadheight/2 - 30.5;

			 $iphone90 = $height * $iphoneratio;
			 $iphone90arrow = $arrowMargin * $iphoneratio;
			 $ipad90 = $ipadheight * $ipadratio;
			 $ipad90arrow = $arrowIpadMargin * $ipadratio;

			 $otherheight = (($height)/2)-10;
			 $otheripadheight = ($ipadheight)/2;

			 $bbheight = $height;
			 $bbotherheight = ($bbheight + 20)/2;
			 $addition = 0;
			if($titleOn == "true")
			{
				$height = $height + 20;
				 $arrowMargin = $arrowMargin + 20;

				$iphone90 = $iphone90 + 20;
				$iphone90arrow = $iphone90arrow + 20;

				$ipadheight = $ipadheight + 20;
				$arrowIpadMargin = $arrowIpadMargin + 20;

				$ipad90 = $ipad90 + 20;
				$ipad90arrow = $ipad90arrow + 20;

				$addition = + 22;
			}

			$page .="
			<script type='text/javascript'>
			function updateImageGallery".$totalgalleries."(type)
			{
				if(type == 0)
				{
					$('#ig".$galleryID."').css('height', '".$height."px');
					if(('#leftarrow".$galleryID."').length > 0){
						$('#leftarrow".$galleryID."').css('height', '".$arrowMargin."px');
						$('#rightarrow".$galleryID."').css('height', '".$arrowMargin."px');
						$('#leftarrow".$galleryID."').css('bottom', '".$arrowMargin."px');
						$('#rightarrow".$galleryID."').css('bottom', '".$arrowMargin."px');
					}
				}

				if(type == 90)
				{
					$('#ig".$galleryID."').css('height', '".$iphone90."px');
					if(('#leftarrow".$galleryID."').length > 0){
						$('#leftarrow".$galleryID."').css('height', '".$iphone90arrow."px');
						$('#rightarrow".$galleryID."').css('height', '".$iphone90arrow."px');
						$('#leftarrow".$galleryID."').css('bottom', '".$iphone90arrow."px');
						$('#rightarrow".$galleryID."').css('bottom', '".$iphone90arrow."px');
					}
				}

			}
			</script>
			";


			 $page .= "

					<div class='imagegallerytd' id = 'ig".$galleryID."' style = 'position:relative;background-color:black;padding:0px;;height:".$height."px' data-role = 'content'>
					<div id = ".$galleryID." style = 'padding:0px;margin:0px;position:relative;background-color:black;'>
					";
				if($titleOn == "true")
				{

					$page .="
					<div class = 'titlebar' align = 'middle' style = 'height:20px;top:0px;border:1px solid white;padding:0px;color:white;position:relative;vertical-align: middle;'>
					</div>
					";
				}
              $page .="<img id = 'img".$galleryID."' src = '' width = \"100%\" style = 'z-index:0;position:relative;' />
				</div>
				";

				if($arrows == "true")
				{
					$page .="
					<div id = 'leftarrow".$galleryID."' style = 'display:inline;position:absolute;bottom:".$arrowMargin."px' onClick = 'switchImage(".$galleryID.", true, \"left\")'><img src = '../../../galleryfiles/galleryimgs/left_arrow.png' /></div>
					<div id = 'rightarrow".$galleryID."' align = 'right' style = 'display:inline;position:absolute;right:0px;bottom:".$arrowMargin."px' onClick = 'switchImage(".$galleryID.", true, \"right\")'><img src = '../../../galleryfiles/galleryimgs/right_arrow.png' /></div>
					";
				}

				$page .= "
				</div>

				";

			$pageipad .="
			<script type='text/javascript'>
			function updateImageGallery".$totalgalleries."(type)
			{
				if(type == 0)
				{
					$('#ig".$galleryID."').css('height', '".$ipadheight."px');
				}

				if(type == 90)
				{
					$('#ig".$galleryID."').css('height', '".$ipad90."px');
				}

			}
			</script>
			";

			 $pageipad .= "

			 <div class='imagegallerytd'  id = 'ig".$galleryID."' style = 'display:block;background-color:black;padding:0px;height:".$ipadheight."px;position:relative' >
					<div id = ".$galleryID." style = 'padding:0px;margin:0px;background-color:black;'>
					";
				if($titleOn == "true")
				{
					$pageipad .="
					<div class = 'titlebar' align = 'middle' style = 'height:20px;margin-bottom:0px;border:1px solid white;padding:0px;color:white;position:relative;top:0px;vertical-align: middle;'>
					</div>
					";
				}
              $pageipad .="<img id = 'img".$galleryID."' src = '' width = \"100%\" style = 'z-index:0;position:relative;' />
				</div>";

				if($arrows == "true")
				{
					$pageipad .="
					<div id = 'leftarrow' style = 'display:inline;position:absolute;bottom:50%' onClick = 'switchImage(".$galleryID.", true, \"left\")'><img src = '../../../galleryfiles/galleryimgs/left_arrow.png' /></div>
					<div id = 'rightarrow' align = 'right' style = 'display:inline;position:absolute;bottom:50%;right:0px' onClick = 'switchImage(".$galleryID.", true, \"right\")'><img src = '../../../galleryfiles/galleryimgs/right_arrow.png' /></div>
					";
				}

			$pageipad .= "
				</div>

				";

			$pagebb1 .="
			<script type='text/javascript'>
			function updateImageGallery".$totalgalleries."()
			{
				var addition = ".$addition.";
				var winW = window.innerWidth;
				var imgH = ".$galleryheight[$totalgalleries]."
				var newH = ((winW/380)*imgH) + addition;
				$('#leftarrow".$galleryID."').css('bottom', '".$arrowMargin."px');
				$('#rightarrow".$galleryID."').css('bottom', '".$arrowMargin."px');
				$('#ig".$galleryID."').css('height', newH + 'px');
			}
			</script>
			";

			 $pagebb1 .= "<div style = 'background-color:black;padding:0px;margin-top:-7px;height:260px;position:relative' id = 'ig".$galleryID."'>
					<div id = ".$galleryID." style = 'padding:0px;margin:0px;position:relative;background-color:black;'>
					";
				if($titleOn == "true")
				{
					$pagebb1 .="
					<div class = 'titlebar' align = 'middle' style = 'height:20px;margin-bottom:0px;border:1px solid white;padding:0px;color:white;position:relative;top:0px;vertical-align: middle;'>
					</div>
					";
				}
              $pagebb1 .="<img id = 'img".$galleryID."' src = '' width = \"100%\" style = 'z-index:0;position:relative;' />

				</div>";

				if($arrows == "true")
				{
					$pagebb1 .="
					<div id = 'leftarrow".$galleryID."' style = 'display:inline;position:absolute;bottom:45%' onClick = 'switchImage(".$galleryID.", true, \"left\")'><img src = '../../../galleryfiles/galleryimgs/left_arrow.png' /></div>
					<div id = 'rightarrow".$galleryID."' align = 'right' style = 'display:inline;position:absolute;bottom:45%;right:0px' onClick = 'switchImage(".$galleryID.", true, \"right\")'><img src = '../../../galleryfiles/galleryimgs/right_arrow.png' /></div>
					";
				}

				$pagebb1 .="
				</div>

				";


		$totalgalleries = $totalgalleries + 1;

		}
		else if($row["type"] == "videogallery")
		{

			$source = "http://www.launchliveapp.com/portalGraphics/Youtube_bar.jpg";
			$imageObj = @imagecreatefromjpeg($source);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
			$imageWidth = 320;
            $imageHeight = ($imageWidth / $origWidth) * $origHeight;
			$imageHeight1 = $imageHeight . "px";
			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cachebb1text .= "\nhttp://www.launchliveapp.com/portalGraphics/Youtube_bar.jpg.txt";
				$cachebbtext .= "\nhttp://www.launchliveapp.com/portalGraphics/Youtube_bar.jpg.txt";

				$cachebb1 .="\nhttp://www.launchliveapp.com/portalGraphics/Youtube_bar.jpg";
			}
		     $titleOn = $row["style"];
			 $galleryID = $row["liveapp_id"];
				$page .= "<div  style = 'border:1px solid black;background-color:black;padding:0px;margin-top:-7px;' data-role = 'content'>
					<div id = ".$galleryID." style = 'padding:0px;margin:0px;position:relative;'>
					";
				if($titleOn == "true")
				{
					$page .="
					<div class = 'titlebar' align = 'middle' style = 'height:20px;margin-bottom:0px;border:1px solid white;padding:0px;color:white;position:relative;top:0px'>
					</div>
					";
				}
              $page .="<img id = 'img".$galleryID."' src = '' width = \"100%\" style = 'z-index:0;position:relative;' onClick = 'playVideoGallery(".$galleryID.");' />
				<img src = '$source' style = 'position:relative;top:0px;width:100%' onClick = 'playVideoGallery(".$galleryID.");'></img>
				</div>
				<div id = 'leftarrow' style = 'float:left;bottom:200px;z-index:1;position:relative;;margin:0px;padding:0px' onClick = 'switchVImage(".$galleryID.", true, \"left\")'><img src = '../../../galleryfiles/galleryimgs/left_arrow.png' /></div>
				<div id = 'rightarrow' align = 'right' style = 'float:right;bottom:200px;z-index:1;position:relative;;margin:0px;padding:0px' onClick = 'switchVImage(".$galleryID.", true, \"right\")'><img src = '../../../galleryfiles/galleryimgs/right_arrow.png' /></div>
				</div>
				";

				$pageipad .= "<div  style = 'border:1px solid black;background-color:black;padding:0px;margin-top:-7px;' data-role = 'content'>
					<div id = ".$galleryID." style = 'padding:0px;margin:0px;position:relative;'>
					";
				if($titleOn == "true")
				{
					$pageipad .="
					<div class = 'titlebar' align = 'middle' style = 'height:20px;margin-bottom:0px;border:1px solid white;padding:0px;color:white;position:relative;top:0px'>
					</div>
					";
				}
              $pageipad .="<img id = 'img".$galleryID."' src = '' width = \"100%\" style = 'z-index:0;position:relative;' onClick = 'playVideoGallery(".$galleryID.");' />
				<img src = '$source' style = 'position:relative;top:30px;width:100%' onClick = 'playVideoGallery(".$galleryID.");'></img>
				</div>
				<div id = 'leftarrow' style = 'float:left;bottom:200px;z-index:1;position:relative;;margin:0px;padding:0px' onClick = 'switchVImage(".$galleryID.", true, \"left\")'><img src = '../../../galleryfiles/galleryimgs/left_arrow.png' /></div>
				<div id = 'rightarrow' align = 'right' style = 'float:right;bottom:200px;z-index:1;position:relative;;margin:0px;padding:0px' onClick = 'switchVImage(".$galleryID.", true, \"right\")'><img src = '../../../galleryfiles/galleryimgs/right_arrow.png' /></div>
				</div>
				";

				$pagebb1 .= "<div  style = 'border:1px solid black;background-color:black;padding:0px;margin-top:-7px;' data-role = 'content'>
					<div id = ".$galleryID." style = 'padding:0px;margin:0px;position:relative;'>
					";
				if($titleOn == "true")
				{
					$pagebb1 .="
					<div class = 'titlebar' align = 'middle' style = 'height:20px;margin-bottom:0px;border:1px solid white;padding:0px;color:white;position:relative;top:0px;vertical-align: middle;'>
					</div>
					";
				}
              $pagebb1 .="<img id = 'img".$galleryID."' src = '' width = \"100%\" style = 'z-index:0;position:relative;' onClick = 'playVideoGallery(".$galleryID.");' />
				<img src = '$source' style = 'position:relative;top:0px;width:100%' onClick = 'playVideoGallery(".$galleryID.");'></img>
				</div>
				<div id = 'leftarrow' style = 'float:left;bottom:200px;z-index:1;position:relative;;margin:0px;padding:0px' onClick = 'switchVImage(".$galleryID.", true, \"left\")'><img src = '../../../galleryfiles/galleryimgs/left_arrow.png' /></div>
				<div id = 'rightarrow' align = 'right' style = 'float:right;bottom:200px;z-index:1;position:relative;margin:0px;padding:0px' onClick = 'switchVImage(".$galleryID.", true, \"right\")'><img src = '../../../galleryfiles/galleryimgs/right_arrow.png' /></div>
				</div>
				";

				/*
			 $configipad .= "<tr><td  style = 'border:1px solid black;height:".$height."px;background-color:black'>
					<div id = ".$galleryID." style = 'padding:0px;margin:0px;'>
					";
				if($titleOn == "true")
				{
					$configipad .="
					<div class = 'titlebar' align = 'middle' style = 'height:20px;margin-bottom:0px;border-top:1px solid white;border-left:1px solid white;border-right:1px solid white;padding:0px;color:white'>
					</div>
					";
				}
              $configipad .="<img src = '' width = \"100%\" style = 'z-index:0;margin:0px;padding:0px;position:relative' />
				</div>
				<div id = 'leftarrow' style = 'float:left;bottom:".$otherheight."px;z-index:1;position:relative;margin:0px;padding:0px' onClick = 'switchImage(".$galleryID.", true, \"left\")'><img src = '../../../galleryfiles/galleryimgs/left_arrow.png' /></div>
				<div id = 'rightarrow' align = 'right' style = 'float:right;bottom:".$otherheight."px;z-index:1;position:relative;margin:0px;padding:0px' onClick = 'switchImage(".$galleryID.", true, \"right\")'><img src = '../../../galleryfiles/galleryimgs/right_arrow.png' /></div>
				</td>
				</tr>

				";

			 $configbb1 .= "<div  style = 'border:1px solid black;height:".$height."px;background-color:black;margin-bottom:-10px;padding:0px'>
					<div id = ".$galleryID." style = 'padding:0px;margin:0px;position:relative;'>
					";
				if($titleOn == "true")
				{
					$configbb1 .="
					<div class = 'titlebar' align = 'middle' style = 'height:20px;margin-bottom:0px;border-top:1px solid white;border-left:1px solid white;border-right:1px solid white;padding:0px;color:white'>
					</div>
					";
				}
              $configbb1 .="<img src = '' width = \"100%\" style = 'z-index:0;margin:0px;padding:0px;position:relative' />
				</div>
				<div id = 'leftarrow' style = 'float:left;bottom:".$otherheight."px;z-index:1;position:relative;color:red' onClick = 'switchImage(".$galleryID.", true, \"left\")'><img src = '../../../galleryfiles/galleryimgs/left_arrow.png' /></div>
				<div id = 'rightarrow' align = 'right' style = 'float:right;bottom:".$otherheight."px;z-index:1;position:relative;color:red' onClick = 'switchImage(".$galleryID.", true, \"right\")'><img src = '../../../galleryfiles/galleryimgs/right_arrow.png' /></div>
				</div>
				";

			 $configbb .= "<div  style = 'border:1px solid black;height:".$height."px;background-color:black;margin-bottom:-10px;padding:0px' valign = 'middle'>
					<div id = ".$galleryID." style = 'padding:0px;margin:0px;position:relative;'>
					";
				if($titleOn == "true")
				{
					$configbb .="
					<div class = 'titlebar' align = 'middle' style = 'height:20px;margin-bottom:0px;border-top:1px solid white;border-left:1px solid white;border-right:1px solid white;padding:0px;color:white'>
					</div>
					";
				}
              $configbb .="<img src = '' width = \"100%\" style = 'z-index:0;margin:0px;padding:0px;position:relative' />
				</div>
				<div id = 'leftarrow' style = 'float:left;bottom:".$otherheight."px;z-index:1;position:relative;color:red' onClick = 'switchImage(".$galleryID.", true, \"left\")'><img src = '../../../galleryfiles/galleryimgs/left_arrow.png' /></div>
				<div id = 'rightarrow' align = 'right' style = 'float:right;bottom:".$otherheight."px;z-index:1;position:relative;color:red' onClick = 'switchImage(".$galleryID.", true, \"right\")'><img src = '../../../galleryfiles/galleryimgs/right_arrow.png' /></div>
				</div>

				";
		*/
		}


        else if($row["type"] == "facebook")
        {
			$web = 'http://www.facebook.com/'.$fbid;
			$comment = $row["comment"];
			$align = $row["alignment"];
			$source1 = "http://launchliveapp.com/portalGraphics/fb_header.jpg";
			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cache .="\n".$source1;
			}
			$imageObj1 = @imagecreatefromjpeg($source1);
            $imageWidth1 = getWidth($imageObj1);
            $origWidth1 = $imageWidth1;
			$imageWidth1 = 320;
            $imageHeight1 = getHeight($imageObj1);
            $origHeight1 = $imageHeight1;
            $imageHeight1 = ($imageWidth1 / $origWidth1) * $origHeight1;
			$source = "http://launchliveapp.com/portalGraphics/fb_med3.jpg";
			$imageObj = @imagecreatefromjpeg($source);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
			$imageWidth = 320;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
            $imageHeight = ($imageWidth / $origWidth) * $origHeight;
			$source = "http://launchliveapp.com/portalGraphics/fbTop_iPad_768.jpg";
			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cacheipad .="\n".$source;
				$cachebb .="\n".$source;
				$cachebb1 .="\n".$source;
				$cachebb1text .= "\n".$source.".txt";
				$cachebbtext .= "\n".$source.".txt";
			}
			$imageObj = @imagecreatefromjpeg($source);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
			$imageWidth = 320;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
            $imageHeight2 = ($imageWidth / $origWidth) * $origHeight;
			$imageHeightpic = $imageHeight2 - 4;
			$imageHeight2bb = $imageheight2 + 30;
			$source = "http://launchliveapp.com/portalGraphics/fbBot_iPad_768.jpg";
			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cacheipad .= "\n".$source;
			}
			$imageObj = @imagecreatefromjpeg($source);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
			$imageWidth = 320;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
            $imageHeight3 = ($imageWidth / $origWidth) * $origHeight;
			$source4 = "http://launchliveapp.com/portalGraphics/fbBot_380.jpg";
			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cache .= "\n".$source4;
				$cachebb .= "\n".$source4;
				$cachebb1 .= "\n".$source4;
				$cachebb1text .=  "\n".$source4.".txt";
				$cachebbtext .=  "\n".$source4.".txt";
			}
			$imageObj = @imagecreatefromjpeg($source4);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
			$imageWidth = 320;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
            $imageHeight4 = ($imageWidth / $origWidth) * $origHeight;

			$sourcefb = "https://graph.facebook.com/".$fbid."/picture?type=large";
			$imagefb = @imagecreatefromjpeg($sourcefb);
			imagejpeg($imagefb, "users-folder/".$folderUserName."/liveapp/fbimage.jpg", 100);


			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cache .= "\nfbimage.jpg";
				$cacheipad .= "\nfbimage.jpg";
				$cachebb .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/fbimage.jpg";
				$cachebb1 .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/fbimage.jpg";
				$cachebb1text .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/fbimage.jpg";
				$cachebbtext .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/fbimage.jpg";
			}

			$page .= '
			<?php
			$fbid1 ="'.$fbid.'";
			$fbtoken1 = "'.$fbtoken.'";
			$fbfile = file_get_contents("http://www.launchliveapp.com/functiontest.php?token=$fbtoken1&id=$fbid1");
			list($fbname, $fbstatus) = explode(";",$fbfile);
			if (strlen($fbstatus) > 250)
			{
				$fbstatus = substr($fbstatus, 0, 250);
				$fbstatus = $fbstatus ."...";
			}
			if($fbstatus == "")
			{
				$fbstatus = "Facebook is not responding";
			}
			?>';
			$pageipad .= '
			<?php
			$fbid1 ="'.$fbid.'";
			$fbtoken1 = "'.$fbtoken.'";
			$fbfile = file_get_contents("http://www.launchliveapp.com/functiontest.php?token=$fbtoken1&id=$fbid1");
			list($fbname, $fbstatus) = explode(";",$fbfile);
			if (strlen($fbstatus) > 250)
			{
				$fbstatus = substr($fbstatus, 0, 250);
				$fbstatus = $fbstatus ."...";
			}
			if($fbstatus == "")
			{
				$fbstatus = "Facebook is not responding";
			}
			?>';
			$pagebb .= '
			<?php
			$fbid1 ="'.$fbid.'";
			$fbtoken1 = "'.$fbtoken.'";
			$fbfile = file_get_contents("http://www.launchliveapp.com/functiontest.php?token=$fbtoken1&id=$fbid1");
			list($fbname, $fbstatus) = explode(";",$fbfile);

			if (strlen($fbstatus) > 250)
			{
				$fbstatus = substr($fbstatus, 0, 250);
				$fbstatus = $fbstatus ."...";
			}

			if($fbstatus == "")
			{
				$fbstatus = "Facebook is not responding";
			}
			?>';
			$pagebb1 .= '
			<?php
			$fbid1 ="'.$fbid.'";
			$fbtoken1 = "'.$fbtoken.'";
			$fbfile = file_get_contents("http://www.launchliveapp.com/functiontest.php?token=$fbtoken1&id=$fbid1");
			list($fbname, $fbstatus) = explode(";",$fbfile);

			if (strlen($fbstatus) > 250)
			{
				$fbstatus = substr($fbstatus, 0, 250);
				$fbstatus = $fbstatus ."...";
			}

			if($fbstatus == "")
			{
				$fbstatus = "Facebook is not responding";
			}
			?>';
		$page .= '<tr><td style="background-image:url(http://launchliveapp.com/portalGraphics/fb_header.jpg);background-size: 100% 100%;" align = "right" height = '.$imageHeight1.'><img src="fbimage.jpg" height = "'.$imageHeight1.'" style = "position:absolute;"><a onClick = "checkLinks(\''.$web.'\', \'1\')"  style="display:block;width:100%;height:100%;text-decoration:none;"><div align = "left"><span style="color:white; font-size:1.4em; position:relative; left:80px;top:10px"><?php echo $fbname ?></span></div></a></td></tr>';
		$page .= '<tr><td style="background-color:white;background-size: 100% 100%;margin-bottom:10px" align = "right"><a onClick = "checkLinks(\''.$web.'\', \'1\')"  style="display:block;width:100%;height:100%;text-decoration:none;"><div align = "left"><span style="color:black; font-size:1.0em; position:relative; top:0%; " ><?php echo $fbstatus ?></span></div></a></td></tr>';
		$page .= '<tr><td style="background-image:url(http://launchliveapp.com/portalGraphics/fbBot_380.jpg);background-size: 100% 100%;" align = "right" height = '.$imageHeight4.'><a onClick = "checkLinks(\''.$web.'\', \'1\')"  style="display:block;width:100%;height:100%;text-decoration:none;"></a></td></tr>';

		$pageipad .= '<tr><td style="background-image:url(http://launchliveapp.com/portalGraphics/fbTop_iPad_768.jpg);background-size: 100% 100%;" align = "right" height = '.$imageHeight2.'><img src="fbimage.jpg" height = "'.$imageHeightpic.'px" style = "position:absolute;" id = "fbimage"><a onClick = "checkLinks(\''.$web.'\', \'1\')" style="display:block;width:100%;height:100%;text-decoration:none;"><div align = "left"><span style="color:white; font-size:2.0em; position:relative; left:78px"><?php echo $fbname ?></span></div></a></td></tr>';
		$pagepad .= '<tr><td style="background-color:white;background-size: 100% 100%;" align = "right" ><a onClick = "checkLinks(\''.$web.'\', \'1\')"  style="display:block;width:100%;height:100%;text-decoration:none;"><div align = "left"><span style="color:black; font-size:1.6em; position:relative; top:0%;" ><?php echo $fbstatus ?></span></div></a></td></tr>';
		$pagepad .= '<tr><td style="background-image:url(http://launchliveapp.com/portalGraphics/fbBot_iPad_768.jpg);background-size: 100% 100%;" align = "right" height = '.$imageHeight3.'><a onClick = "checkLinks(\''.$web.'\', \'1\')"  style="display:block;width:100%;height:100%;text-decoration:none;"></a></td></tr>';

			$pagebb.= '<div style="position:relative;" >
							<a onClick = "checkLinks(\''.$web.'\', \'1\')" style= "display:block;width:100%;height:100%;text-decoration:none;">
							<span style="color:white; font-size:1.6em; position:absolute;left:60px"><?php echo $fbname ?></span>
							<div style = "position:absolute;"><img style = "border:none;" src="fbimage.jpg" width = "50px" height = "40px"/></div>
							<img style = "border:none;" src="../../../portalGraphics/fbTop_iPad_768.jpg" height="50px" width = "100%" />
							</a>
							</div>';
			$pagebb.= '<div style = "position:relative;width:100%;margin:auto;background-color:white" align = "left"><span style="color:black;position:relative;"><?php echo $fbstatus ?></span></div>';

			$pagebb.= '<div style="position:relative;">
							<a onClick = "checkLinks(\''.$web.'\', \'1\')"  style= "display:block;width:100%;height:100%;text-decoration:none;">
							<img style = "border:none;" src="../../../portalGraphics/fbBot_380.jpg" height="'.$imageHeight4.'px" width = "100%" />
							</a>
							</div>';

			$pagebb1.= '<div style="position:relative;padding:-7px;margin-top:-7px;" height='.$imageHeight2bb.'>
							<a onClick = "checkLinks(\''.$web.'\', \'1\')"  style= "display:block;width:100%;height:100%;text-decoration:none;">
							<span style="color:white; font-size:1.6em; position:absolute;left:60px"><?php echo $fbname ?></span>
							<div style = "position:absolute;"><img style = "border:none;" src="fbimage.jpg" width = "50px" height = "40px"/></div>
							<img style = "border:none;" src="../../../portalGraphics/fbTop_iPad_768.jpg" height="50px" width = "100%" />
							</a>
							</div>';

			$pagebb1.= '<div style = "position:relative;width:100%;margin:auto;background-color:white" align = "left"><span style="color:black;position:relative;"><?php echo $fbstatus ?></span></div>';

			$pagebb1.= '<div style="position:relative;padding:-7px;margin-top:-7px;">
							<a onClick = "checkLinks(\''.$web.'\', \'1\')"  style= "display:block;width:100%;height:100%;text-decoration:none;">
							<img style = "border:none;" src="../../../portalGraphics/fbBot_380.jpg" height="'.$imageHeight4.'px" width = "100%" />
							</a>
							</div>';
        }

        else if($row["type"] == "twitter")
        {
	$twittername = $row["url"];
	$web = "http://twitter.com/$twittername";
	$source1 = "http://www.launchliveapp.com/portalIMG/twitterBottomBox.jpg";
	$imageObj = @imagecreatefromjpeg($source1);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
	$imageWidth = 320;
            $imageHeight1 = ($imageWidth / $origWidth) * $origHeight;
	$imageHeight1 = $imageHeight1 . "px";
	$source4 = "http://www.launchliveapp.com/portalGraphics/twitBot_iPad.jpg";
	$imageObj = @imagecreatefromjpeg($source4);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
	$imageWidth = 320;
            $imageHeight4 = ($imageWidth / $origWidth) * $origHeight;
	$imageHeight4 = $imageHeight4 . "px";
	$source5 = "http://www.launchliveapp.com/portalGraphics/twitTop_iPad_768.jpg";
	$imageObj = @imagecreatefromjpeg($source5);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
	$imageWidth = 320;
            $imageHeight5 = ($imageWidth / $origWidth) * $origHeight;
	$imageHeight5 = $imageHeight5 . "px";
	$source2 = "http://www.launchliveapp.com/portalIMG/twitterMiddleBox.jpg";
	$imageObj = @imagecreatefromjpeg($source2);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
	$imageWidth = 320;
            $imageHeight2 = ($imageWidth / $origWidth) * $origHeight;
	$imageHeight2 = $imageHeight2 . "px";
	$source3 = "http://www.launchliveapp.com/portalIMG/twitterTopBox.jpg";
	$imageObj = @imagecreatefromjpeg($source3);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
	$imageWidth = 320;
            $imageHeight3 = ($imageWidth / $origWidth) * $origHeight;
	$imageHeight3 = $imageHeight3 . "px";

	// Only cache this widget's resources if the page isn't password protected.
	if ($requireLogin == 0) // does not require a login
	{
		$cache .= "\nhttp://www.launchliveapp.com/portalIMG/twitterBottomBox.jpg";
		$cache .= "\nhttp://www.launchliveapp.com/portalIMG/twitterMiddleBox.jpg";
		$cache .= "\nhttp://www.launchliveapp.com/portalIMG/twitterTopBox.jpg";
		$cacheipad .="\nhttp://www.launchliveapp.com/portalGraphics/twitTop_iPad_768.jpg";
		$cacheipad .= "\nhttp://www.launchliveapp.com/portalIMG/twitterMiddleBox.jpg";
		$cacheipad .= "\nhttp://www.launchliveapp.com/portalGraphics/twitBot_iPad.jpg";
		$cachebb .= "\nhttp://www.launchliveapp.com/portalIMG/twitterBottomBox.jpg";
		$cachebb .= "\nhttp://www.launchliveapp.com/portalIMG/twitterTopBox.jpg";
		//$cachebb1 .= "\nhttp://www.launchliveapp.com/portalIMG/twitterBottomBox.jpg";
		//	$cachebb1 .= "\nhttp://www.launchliveapp.com/portalIMG/twitterTopBox.jpg";
			   $cachebb1text .= "\nhttp://www.launchliveapp.com/portalIMG/twitterBottomBox.jpg.txt";
		$cachebb1text .= "\nhttp://www.launchliveapp.com/portalIMG/twitterTopBox.jpg.txt";
		$cachebbtext .= "\nhttp://www.launchliveapp.com/portalIMG/twitterBottomBox.jpg.txt";
		$cachebbtext .= "\nhttp://www.launchliveapp.com/portalIMG/twitterTopBox.jpg.txt";
	}

	$twittername1 = '$twittername1';
	$status = '$status';
	$time = '$time';
	$status1 = '$status1';
	$time1 = '$time1';
	$page .= '
	<?php
	$twittername1 ="'.$twittername.'";
	$twittername1 ="'.$twittername.'";
	$feed = new appTweetRequest($twittername1);


	?>';
	$pageipad .= '
	<?php
	$twittername1 ="'.$twittername.'";
	$twittername1 ="'.$twittername.'";
	$feed = new appTweetRequest($twittername1);

	?>';
	$pagebb .= '
	<?php
	$twittername1 ="'.$twittername.'";
	$twittername1 ="'.$twittername.'";
	$feed = new appTweetRequest($twittername1);

	?>';
	$pagebb1 .= '
	<?php
	$twittername1 ="'.$twittername.'";
	$twittername1 ="'.$twittername.'";
	$feed = new appTweetRequest($twittername1);

	?>';

	$page.= "<div style='position:relative;padding:-7px;'>
	<div align='left' style = 'position:absolute;top:0%;width:100%;margin:auto;'>
	<span style='color:#0099FF;position:relative;top:1px;font-size:30px'>
	@<?php echo $twittername1; ?>
	</span>
	</div>
	<a  onClick = 'checkLinks(\"$web\", \"1\")'>
	<img class='buttonImage' style = 'border:none;width:100%;'  src='http://www.launchliveapp.com/portalIMG/twitterTopBox.jpg'/>
	</a>
	</div>";
	$page.= '<div style="position:relative;padding:-7px;;">
	<table style = "width:100%">
	<tr><td style = "background-color:black;">
	<span style="color:gray; font-size:0.8em; position:relative; top:0px; float:left;"><?php echo $feed -> times[0]; ?></span><br><span style="color:white; font-size:1.0em; float:left; left:10px" align = "left"><?php echo $feed -> statuses[0]; ?></span><br>
	<span style="color:gray; font-size:0.8em; position:relative; top:0px; float:left;"><?php echo $feed -> times[1]; ?></span><br><span style="color:white; font-size:1.0em; float:left; left:10px;" align = "left"><?php echo $feed -> statuses[1]; ?></span></a>
	</td></tr>
	</table>
	</div>';
	$page.= "<div style='position:relative;padding:-2px;'>
	<div align='center' style = 'position:absolute;width:100%;margin:auto;'>
	</div>
	<a  onClick = 'checkLinks(\"$web\", \"1\")'>
	<img class='buttonImage' style = 'border:none;width:100%;'  src='http://www.launchliveapp.com/portalIMG/twitterBottomBox.jpg'/>
	</a>
	</div>";


	$pageipad.= "<div style='position:relative;padding:-7px;'>
	<div align='left' style = 'position:absolute;top:0%;width:100%;'>
	<span style='color:#0099FF;position:relative;top:1px;font-size:30px;'>
	@<?php echo $twittername1; ?>
	</span>
	</div>
	<a  onClick = 'checkLinks(\"$web\", \"1\")'>
	<img class='buttonImage' style = 'border:none;width:100%;'  src='http://www.launchliveapp.com/portalGraphics/twitTop_iPad_768.jpg'/>
	</a>
	</div>";
	$pageipad.= '<div style="position:relative;padding:-7px;;">
	<table style = "width:100%">
	<tr><td style = "background-color:black;">
	<span style="color:gray; font-size:0.8em; position:relative; top:0px; float:left;"><?php echo $feed -> times[0]; ?></span><br><span style="color:white; font-size:1.0em; float:left; left:10px" align = "left"><?php echo $feed -> statuses[0]; ?></span><br>
	<span style="color:gray; font-size:0.8em; position:relative; top:0px; float:left;"><?php echo $feed -> times[1]; ?></span><br><span style="color:white; font-size:1.0em; float:left; left:10px;" align = "left"><?php echo $feed -> statuses[1]; ?></span></a>
	</td></tr>
	</table>
	</div>';
	$pageipad.= "<div style='position:relative;padding:-2px;'>
	<div align='center' style = 'position:absolute;width:100%;margin:auto;'>
	</div>
	<a  onClick = 'checkLinks(\"$web\", \"1\")'>
	<img class='buttonImage' style = 'border:none;width:100%;'  src='http://www.launchliveapp.com/portalGraphics/twitBot_iPad.jpg'/>
	</a>
	</div>";

	$pagebb.= "<div style='position:relative;'>
	<div align='left' style = 'position:absolute;top:0%;width:100%;margin:auto;'>
	<span style='color:#0099FF;position:relative;top:10%;font-size:30px'>
	@<?php echo $twittername1; ?>
	</span>
	</div>
	<a  onClick = 'checkLinks(\"$web\", \"1\")'>
	<img style = 'border:none;'  src='../../../portalIMG/twitterTopBox.jpg' height='$imageHeight3' width = '100%'/>
	</a>
	</div>";
	$pagebb.= "<div style = 'position:relative;width:100%;margin:auto;background-color:black'>
	<a  onClick = 'checkLinks(\"$web\", \"1\")'>
	<span style='color:gray; font-size:0.8em; position:relative; top:0px; float:left;'><?php echo $time; ?></span><br><span style='color:white; font-size:1.0em; float:left; left:10px' align = 'left'><?php echo $status; ?></span><br>
	<span style='color:gray; font-size:0.8em; position:relative; top:0px; float:left;'><?php echo $time1; ?></span><br><span style='color:white; font-size:1.0em; float:left; left:10px;' align = 'left'><?php echo $status1; ?></span></a></td></tr>
	</a>
	</div>";
	$pagebb.= "<div style='position:relative;'>
	<div align='center' style = 'position:absolute;top:40%;width:100%;margin:auto;'>
	</div>
	<a  onClick = 'checkLinks(\"$web\", \"1\")'>
	<img style = 'border:none;'  src='../../../portalIMG/twitterBottomBox.jpg' height='$imageHeight1' width = '100%'/>
	</a>
	</div>";
	$pagebb1.= "<div style='position:relative;padding:-7px;margin-top:-7px;'>
	<div align='left' style = 'position:absolute;top:0%;width:100%;margin:auto;'>
	<span style='color:#0099FF;position:relative;top:10%;font-size:30px'>
	@<?php echo $twittername1; ?>
	</span>
	</div>
	<a  onClick = 'checkLinks(\"$web\", \"1\")'>
	<img style = 'border:none;width:100%;display:block '  src='../../../portalIMG/twitterTopBox.jpg'/>
	</a>
	</div>";
	$pagebb1.= '<div style="position:relative;padding:-7px;;">
	<table style = "width:100%">
	<tr><td style = "background-color:black;">
	<span style="color:gray; font-size:0.8em; position:relative; top:0px; float:left;"><?php echo $feed -> times[0]; ?></span><br><span style="color:white; font-size:1.0em; float:left; left:10px" align = "left"><?php echo $feed -> statuses[0]; ?></span><br>
	<span style="color:gray; font-size:0.8em; position:relative; top:0px; float:left;"><?php echo $feed -> times[1]; ?></span><br><span style="color:white; font-size:1.0em; float:left; left:10px;" align = "left"><?php echo $feed -> statuses[1]; ?></span></a>
	</td></tr>
	</table>
	</div>';

	$pagebb1.= "<div style='position:relative;padding:-2px;margin-top:-2px;'>
	<div align='center' style = 'position:absolute;width:100%;margin:auto;'>
	</div>
	<a  onClick = 'checkLinks(\"$web\", \"1\")'>
	<img style = 'border:none;width:100%;'  src='../../../portalIMG/twitterBottomBox.jpg'/>
	</a>
	</div>";

	   }
        else if($row["type"] == "youtube")
        {
			$web = $row["url"];
			$webnew = explode("=",$web);
			$webnew1 = explode("&", $webnew[1]);

			$youtubeURL = $row["style"];
			$videoId = $row['text'];
			$comment = $row["comment"];
			$source = "http://www.launchliveapp.com/portalIMG/youtubeBox.jpg";
			$imageObj = @imagecreatefromjpeg($source);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
			$imageWidth = 320;
            $imageHeight = ($imageWidth / $origWidth) * $origHeight;
			$imageHeight = $imageHeight . "px";
			$source = "http://www.launchliveapp.com/portalGraphics/Youtube_bar.jpg";
			$imageObj = @imagecreatefromjpeg($source);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
			$imageWidth = 320;
            $imageHeight = ($imageWidth / $origWidth) * $origHeight;
			$imageHeight1 = $imageHeight . "px";
			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cache .= "\nhttp://img.youtube.com/vi/".$videoId."/0.jpg";
				$cache .= "\nhttp://www.launchliveapp.com/portalGraphics/Youtube_bar.jpg";
				$cache .= "\n".$videoId.".jpg";
				$cacheipad .= "\nhttp://img.youtube.com/vi/".$videoId."/0.jpg";
				$cacheipad .= "\nhttp://www.launchliveapp.com/portalGraphics/Youtube_bar.jpg";
				$cachebb .= "\nhttp://www.launchliveapp.com/portalIMG/youtubeBox.jpg";
				$cachebb .= "\nhttp://www.launchliveapp.com/portalGraphics/Youtube_bar.jpg";
				$cachebb1 .= "\nhttp://img.youtube.com/vi/".$videoId."/0.jpg";
			//	$cachebb1 .= "\nhttp://www.launchliveapp.com/portalGraphics/Youtube_bar.jpg";
				$cachebb1text .= "\nhttp://img.youtube.com/vi/".$videoId."/0.jpg";
				$cachebb1text .= "\nhttp://www.launchliveapp.com/portalGraphics/Youtube_bar.jpg.txt";
				$cachebbtext .= "\nhttp://img.youtube.com/vi/".$videoId."/0.jpg";
				$cachebbtext .= "\nhttp://www.launchliveapp.com/portalGraphics/Youtube_bar.jpg.txt";
			}

			$page .= "<div class='youtube-holder' style='position:relative;padding:-7px;margin-top:-7px;background-image:url(http://img.youtube.com/vi/".$videoId."/0.jpg);background-size:100% 100%;height:200px;' onclick='playYoutube(\"".$videoId."\");'>	<div id = 'youtube-player".$youtubenumber."' class = 'ytindex'></div>
			<img style = 'border:none;position:absolute;width:100%;bottom:0.5px'  src='http://www.launchliveapp.com/portalGraphics/Youtube_bar.jpg'/>
					</div>";


				$pageipad .= "<div class='youtube-holder' style='position:relative;padding:-7px;margin-top:-7px;background-image:url(http://img.youtube.com/vi/".$videoId."/0.jpg);width:100%;background-size:100% 100%;height:485px;' onclick='playYoutube(\"".$videoId."\");'>	<div id = 'youtube-player".$youtubenumber."' class = 'ytindex'></div>

					</div>";



		$pagebb.= "<div style='position:relative;'>
						<div align='center' style = 'position:absolute;top:40%;width:100%;margin:auto;'>
							<center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>
								$comment
							</center>
						</div>
					     <a onclick='youtube.play(\"".$youtubeURL."\")'>
						<img style = 'border:none;'  src='http://img.youtube.com/vi/".$videoId."/0.jpg' width = '100%'/>
						<img style = 'border:none;position:absolute;bottom:0px;;left:0px;'  src='../../../portalGraphics/Youtube_bar.jpg' height='$imageHeight1' width = '100%'/>
						</a>
						</div>";



		$pagebb1.= "<div style='position:relative;padding:-7px;margin-top:-7px;background-image:url(http://img.youtube.com/vi/".$videoId."/0.jpg);height:355px;background-size:100% 100%' onclick='youtube.play(\"".$youtubeURL."\")'>
						<img style = 'border:none;position:absolute;bottom:0.5px;width:100%;'  src='../../../portalGraphics/Youtube_bar.jpg'/>
					</div>";

	   $youtubenumber++;
	   }
        else if($row["type"] == "linkedin")
        {
			$web = $row["url"];
			$findme   = 'http://';
			$pos = strpos($web, $findme);
			if($pos === false)
			{
			$web = 'http://'.$web;
			}
			$comment = $row["comment"];
			$source = "http://www.launchliveapp.com/portalIMG/LinkedInBox.jpg";
			$imageObj = @imagecreatefromjpeg($source);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
			$imageWidth = 320;
            $imageHeight = ($imageWidth / $origWidth) * $origHeight;
			$imageHeight = $imageHeight . "px";
			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cache .= "\nhttp://www.launchliveapp.com/portalIMG/LinkedInBox.jpg";
				$cache .= "\nlinkedin.jpg";
				$cacheipad .= "\nhttp://www.launchliveapp.com/portalIMG/LinkedInBox.jpg";
				$cacheipad .= "\nlinkedin.jpg";
				$cachebb .= "\nhttp://www.launchliveapp.com/portalIMG/LinkedInBox.jpg";
				$cachebb .= "\nhttp://www.launchliveapp.com/users-folder/$folderUserName/liveapp/linkedin.jpg";
			//	$cachebb1 .= "\nhttp://www.launchliveapp.com/portalIMG/LinkedInBox.jpg";
				$cachebb1 .= "\nhttp://www.launchliveapp.com/users-folder/$folderUserName/liveapp/linkedin.jpg";

				$cachebb1text .= "\nhttp://www.launchliveapp.com/portalIMG/LinkedInBox.jpg.txt";
				$cachebbtext .= "\nhttp://www.launchliveapp.com/portalIMG/LinkedInBox.jpg.txt";
			}
			$page .='<?php
			$rinkedIn = file_get_contents("https://www.launchliveapp.com/linkedin/loadProfile.php?id='.$userId.'");
			?>';
			$page.= "<div style='position:relative;margin-bottom:7px;padding:-7px;'>
						<div style = 'border:none;background-image:url($source);width:100%;'/>";

			$page.= '<?php echo $rinkedIn;?>';

			$page  .= "</div>
							</div>";

			//$config .= "<tr><td style = 'background-image:url($source);background-repeat:no-repeat;background-size:100% 100%' height = '$imageHeight' width = '$imageWidth'><a href='$web' target='' style='display:block;width:100%;height:100%'><center style = 'font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></a></td></tr>";
			$pageipad .='<?php
			$rinkedIn = file_get_contents("https://www.launchliveapp.com/linkedin/loadProfile.php?id='.$userId.'");
			?>';
			$pageipad.= "<div style='position:relative;margin-bottom:7px;padding:-7px;'>
						<div style = 'border:none;background-image:url($source);width:100%;'/>";

			$pageipad .= '<?php echo $rinkedIn;?>';

			$pageipad  .= "</div>
							</div>";

			//$configipad .= "<tr><td style = 'background-image:url($source);background-repeat:no-repeat;background-size:100% 100%' height = '$imageHeight' width = '$imageWidth'><a href='$web' target='' style='display:block;width:100%;height:100%'><center style = 'font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'>$comment</center></a></td></tr>";
			/*
			$configbb .='<?php
			$rinkedIn = file_get_contents("https://www.launchliveapp.com/linkedin/loadProfile.php?id='.$userId.'");
			?>';
			$configbb.= "<div style='position:relative;margin-bottom:2%;'>
						<div style = 'border:none;background-image:url($source)' height='$imageHeight' width = '100%'/>";

			$configbb .= '<?php echo $rinkedIn;?>';

			$configbb.="
						</div>";



			/*$configbb.= "<div style='position:relative;'>
						<div align='center' style = 'position:absolute;top:40%;width:100%;margin:auto;'>
							<center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:0%;'>
								$comment
							</center>
						</div>
						<a href='$web'>
						<img style = 'border:none;'  src='$source' height='$imageHeight' width = '100%'/>
						</a>
						</div>";*/


			$pagebb1 .='<?php
			$rinkedIn = file_get_contents("https://www.launchliveapp.com/linkedin/loadProfile.php?id='.$userId.'");
			?>';
			$pagebb1.= "<div style='position:relative;margin-bottom:7px;padding:-7px;margin-top:-7px;'>
						<div style = 'border:none;background-image:url($source);width:100%;'/>";

			$pagebb1 .= '<?php echo $rinkedIn;?>';

			$pagebb1  .= "</div>
							</div>";

			/*$configbb1.= "<div style='position:relative;padding:-7px;margin-top:-7px;'>
						<div align='center' style = 'position:absolute;top:35%;width:100%;margin:auto;'>
							<center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:0%;'>
								$comment
							</center>
						</div>
						<a href='$web'>
						<img style = 'border:none;'  src='$source' height='$imageHeight' width = '100%'/>
						</a>
						</div>";*/
 }

		else if ($row["type"] == "seperator")
		{
			$bgColor = $row['background_color'];
			$barWidth = $row['hor_width'];
			$hideBar = $row['bold'];

			$display = "";
			if ($hideBar == "on")
			{
				// Show bar
				$display = "display:none";
			}
			else
			{
				// Hide bar
				$display = "";
			}

			$page.= "
			<div style='position:relative;padding:-2%;margin-top:-4%;'>
			<div style = 'position:relative;background-color:$bgColor;height:$size;padding:-10px;margin-top:-10px'>
			<hr style = '$display;position:relative;top:45%;max-width:100%;width:$barWidth%;height:3px;background-color:$color;border:0' />
			</div>
			</div>";

			$pageipad.= "
			<div style='position:relative;padding:-2%;margin-top:-4%;'>
			<div style = 'position:relative;background-color:$bgColor;height:$size;padding:-7px;margin-top:-7px'>
			<hr style = '$display;position:relative;top:45%;max-width:100%;width:$barWidth%;height:3px;background-color:$color;border:0' />
			</div>
			</div>";

			$pagebb.= "
			<div style='position:relative;padding:-2%;margin-top:-2%;margin-bottom:-2%;'>
			<div style = 'position:relative;background-color:$bgColor;height:$size'>
			<hr style = '$display;position:relative;top:45%;max-width:100%;width:$barWidth%;height:3px;background-color:$color;border:0' />
			</div>
			</div>";
			$pagebb1.= "
			<div style='position:relative;padding:-2%;margin-top:-4%;'>
			<div style = 'position:relative;background-color:$bgColor;height:$size;padding:-7px;margin-top:-7px'>
			<hr style = '$display;position:relative;top:45%;max-width:100%;width:$barWidth%;height:3px;background-color:$color;border:0' />
			</div>
			</div>";
		}
        else if($row["type"] == "findme")
        {

			$image = "users-folder/$folderUserName/liveapp/". $row['location'];
			$imagebb = $row['location'];
			$image1 = $row['location'];
            // IMAGE LOGIC
            $imageObj = @imagecreatefromjpeg($image);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
            $imageWidth = 320*($row["hor_width"]/100);
            $imageHeight = ($imageWidth / $origWidth) * $origHeight;
			$imageHeight = $imageHeight . "px";
            $placeW = (320- $imageWidth);
			$align = $row["alignment"];

			$page .='<div align="center">
				<img id="findme" style="width:100%;" onClick="findMe()" src="'.$imagebb.'"/>
				</div>
			';
			$pageipad .='<div align="center">
				<img id="findme" style="width:100%;" onClick="findMe()" src="'.$imagebb.'"/>
				</div>
			';
			$pagebb .='<div align="center">
				<img id="findme" style="width:100%;height:'.$imageHeight.'" onClick="findMe()" src="'.$imagebb.'"/>
				</div>
			';

			$pagebb1 .='<div align="center">
				<img id="findme" style="width:100%;" onClick="findMe()" src="'.$imagebb.'"/>
				</div>
			';
			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cache .= "\n".$image1;
				$cacheipad .= "\n".$image1;
				$cachebb .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$image1;
				$cachebb1 .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$image1;
				$cachebb1text .= makeTextFile($image);
				$cachebbtext .= makeTextFile($image);
			}
        }

		else if($row["type"] == "vcard")
        {
			$image = "users-folder/$folderUserName/liveapp/". $row['location'];
			$imagebb = $row['location'];
			$image1 = $row['location'];
            // IMAGE LOGIC
            $imageObj = @imagecreatefromjpeg($image);
            $imageWidth = getWidth($imageObj);
            $origWidth = $imageWidth;
            $imageHeight = getHeight($imageObj);
            $origHeight = $imageHeight;
            $imageWidth = 320*($row["hor_width"]/100);
            $imageHeight = ($imageWidth / $origWidth) * $origHeight;
			$imageHeight = $imageHeight . "px";
            $placeW = (320- $imageWidth);
			$align = $row["alignment"];

				$page .='<div align="center">
				<img id="findme" style="width:100%;" onClick = \'sendVcard("'.$folderUserName.'", "'.$row['liveapp_id'].'")\' src="'.$imagebb.'"/>
				</div>
			';

				$pageipad .='<div align="center">
				<img id="findme" style="width:100%;" onClick = \'sendVcard("'.$folderUserName.'", "'.$row['liveapp_id'].'")\' src="'.$imagebb.'"/>
				</div>
			';

			$pagebb .='<div align="center">
				<img id="findme" style="width:100%;height:'.$imageHeight.'" onClick = \'sendVcard("'.$folderUserName.'", "'.$row['liveapp_id'].'")\'  src="'.$imagebb.'"/>
				</div>
			';

			$pagebb1 .='<div align="center">
				<img id="findme" style="width:100%;" onClick = \'sendVcard("'.$folderUserName.'", "'.$row['liveapp_id'].'")\' src="'.$imagebb.'"/>
				</div>
			';
			// Only cache this widget's resources if the page isn't password protected.
			if ($requireLogin == 0) // does not require a login
			{
				$cache .= "\n".$image1;
				$cacheipad .= "\n".$image1;
				$cachebb .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$image1;
				$cachebb1 .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/".$image1;
				$cachebb1text .= makeTextFile($image);
				$cachebbtext .= makeTextFile($image);
            }

        }
    }

}

if($paid == 0){
$image1 = "http://www.launchliveapp.com/portalIMG/LiveApp_SignUpButton.jpg";
// IMAGE LOGIC
$imageObj = @imagecreatefromjpeg($image1);
$imageWidth = getWidth($imageObj);
$origWidth = $imageWidth;
$imageHeight = getHeight($imageObj);
$origHeight = $imageHeight;
$imageWidth = 320;
$imageHeight = ($imageWidth / $origWidth) * $origHeight;
$imageHeight = $imageHeight . "px";
$placeW = (320- $imageWidth);
$config .= "<tr><td style = 'background-image:url($image1);background-repeat:no-repeat;background-size:100% 100%' height = '$imageHeight'><a href='http://www.launchliveapp.com/' target='' style='display:block;width:100%;height:100%'><center style = 'font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;'></center></a></td></tr>
";
$configipad .= "<tr><td style = 'background-image:url($image1);background-repeat:no-repeat;background-size:100% 100%' height = '$imageHeight'><a href='http://www.launchliveapp.com/' target='' style='display:block;width:100%;height:100%'><center style = 'font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;'></center></a></td></tr>
";
$configbb.= "<div style='position:relative;padding:0px;margin:0px;'>
			<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:40%;'></center></div>
			<img src='$image1' height='$imageHeight' width = '100%' />
			</div>";
$configbb1.= "<div style='position:relative;padding:-7px;margin-top:-7px;'>
			<div style = 'position:absolute;top:40%;width:100%;margin:auto'><center style='font-family:$font;font-style:$italic;font-weight:$bold;color:$color;font-size:$size;text-decoration:$underline;position:relative;top:0%;'></center></div>
			<img src='$image1' height='$imageHeight' width = '100%' />
			</div>";

$cache .= "\n".$image1;
$cacheipad .= "\n".$image1;
$cachebb .= "\n".$image1;
//$cachebb1 .= "\n".$image1;
$cachebb1text .= "\n".$image1.".txt";
$cachebbtext .= "\n".$image1.".txt";

}
$source = '../../../images/sharepage_facebook.png';
$configbb .=	'
		</div>
		<table width = "100%" cellpadding="0">
		<tr><td>
		<div data-theme="c" data-rel="dialog" id="menu" style = "display:none">
		<div data-role="content" class="ui-overlay-shadow ui-corner-bottom ui-content ui-body-a">
		<h2 align="center"></h2>
		<a onclick="window.location.reload()" data-role="button"  data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Update</span>
			</span>
		</a>
		<a onclick="menulogic(\'qr\')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Share</span>
			</span>
		</a>
		<!-- FRANCIS: REMOVED ABOUT BUTTON/PAGE FROM ALL APPS -->
		<!--<a onclick="menulogic(\'about\')" data-role="button"  data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">About</span>
			</span>
		</a>  		-->
		<a onclick="menulogic(\'menu\')" data-role="button" data-rel="dialog" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Cancel</span>
			</span>
		</a>
		</div>
		</div>
		</td></tr>
		<tr><td width = "100%">
		<div data-theme="c"  data-rel="dialog" id="qr" style = "top:40px;display:none">
		<div data-role="content" class="ui-overlay-shadow ui-corner-bottom ui-content ui-body-a" width = "100%" style="text-align:center;">
		<h1>Share This App</h1>
		<div width = "100%">
		<p>Scan the QR code below <br>to download this app</p>
		<br>
		<img src="qrcode.jpg" align="center" width="240" height="240"></img>
		<br>
		<br>
		<p>Or download your the app from this link:</p>
		<a style = "font-size:0.8em;" href="http://www.launchliveapp.com/'.$folderUserName.'/">www.launchliveapp.com/'.$folderUserName.'</a>
		<br>
		</div>
		</div>
		</div>
		</td></tr>
		<tr><td>
		<!-- FRANCIS: REMOVED ABOUT BUTTON/PAGE FROM ALL APPS -->
		<div data-theme="c" data-transition="slidedown" data-rel="dialog" id="about" style = "top:40px;display:none">
		<div data-role="content" class="ui-overlay-shadow ui-corner-bottom ui-content ui-body-a" style="display:none;">
		<a onclick="menulogic(\'main\')"Back</a>
		<h1>About LiveApp</h1>
		</div>
		<div align="center" style="display:none;">
		<h2>Powered by:</h2>
		<img src="http://www.launchliveapp.com/Logo.png" width="320" height="120"/>
		<br>
		</div>
		</div>
		</div>
		</td></tr>
		</table>
		'
		;

/*
if ($bbpages == 0)
{
	$config .= $page;
	$configipad .= $pageipad;
	$configbb .= $pagebb;
	$configbb1 .= $pagebb1;
}
*/
// Added by Zak (July 17, 2015)
// Add the page to the app if it's not password protected.
// The homepage can't require a login, so it's always included.
if ($requireLogin == 0)
{
	$config .= $page;
	$configipad .= $pageipad;
	$configbb .= $pagebb;
	$configbb1 .= $pagebb1;
}

$config .= '
		</div>';
$configipad .= '
</div>';
$configbb1 .= '</div>';

if($bbpages == 0)
{
	$handle1 = fopen("users-folder/$folderUserName/liveapp/liveapp_currentbb.php","w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
	fputs($handle1,$configbb); // Write the contents of $config to the file opened in $handle
	fclose($handle1); // Close the opened file
}

if ($requireLogin == 1) // If we're looking at any page that's password protected, then save it to a file.
{
	$pageFile = "page".$pageNumber.".php";
	$handle1 = fopen("users-folder/$folderUserName/liveapp/".$pageFile,"w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
	fputs($handle1, $page); // Write the contents of $page to the file opened in $handle
	fclose($handle1); // Close the opened file

	$pageFile = "page".$pageNumber."ipad.php";
	$handle1 = fopen("users-folder/$folderUserName/liveapp/".$pageFile,"w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
	fputs($handle1, $pageipad); // Write the contents of $pageipad to the file opened in $handle
	fclose($handle1); // Close the opened file

	//$pageFile = "page".$pageNumber."bb.php";
	//$handle1 = fopen("users-folder/$folderUserName/liveapp/".$pageFile,"w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
	//fputs($handle1, $pagebb); // Write the contents of $pagebb to the file opened in $handle
	//fclose($handle1); // Close the opened file

	$pageFile = "page".$pageNumber."bb1.php";
	$handle1 = fopen("users-folder/$folderUserName/liveapp/".$pageFile,"w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
	fputs($handle1, $pagebb1); // Write the contents of $pagebb to the file opened in $handle
	fclose($handle1); // Close the opened file

}

$bbpages = $bbpages + 1;

}


$config .=	'
		</div>
		<table width = "100%" cellpadding="0" style="position:absolute; top:10vh; z-index: 50">
		<tr><td>

		<div data-theme="c" data-rel="dialog" id = "signup"  style = "display:none" class = "login">
			<form data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "signupform" id = "signupform">
				<span id = "signupmessage" style = "color:red"></span>
				<label for="username">Username:</label>
				<input type="text" name="username" id="username" value="" class="required"/>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value=""  class="required email" />
				<label for="password">Password:</label>
				<input type="password" name="password" id="password" value="" class="required"/>';
/*
     MIKAEL: Commenting this out until adding custom fields is working
		for($i = 1; $i < 8; $i++)
		{
			if($inputs[$i] !== "None")
			{
				if($inputr[$i] == 1)
				{
					$required = "required";
				}
				else
				{
					$required = "";
				}
				$config .='
				<label for="input'.$i.'">'.$inputs[$i].':</label>
				<input type="text" name="input'.$i.'" id="input'.$i.'" value="" class = "'.$required.'" />
				';
			}
		}
		*/
$config .='
		<input type="submit" name = "submit" value = "Sign Up" />
		<input type = "hidden" name = "formTXT" id = "formTXT" value = "signup" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id = "login" style = "display:none" class = "login">
			<form style="width: 95%; margin: 2%; margin-top: 4%;" data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "loginform" id = "loginform">
				<span id = "loginmessage" style = "color:red"></span>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value="" class="required email" />
				<label for="password" style="margin-top: 2%">Password:</label>
				<input type="password" name="password" id="password" value=""  class="required" />
				<input type = "hidden" name = "formTXT" id = "formTXT" value = "login" />
				<input type="submit" name = "submit" value = "Log In" style="margin-top: 2%" />
				<!-- <input type = "button" name = "" value = "Not a member? Sign Up Here" onclick = "menulogic(\'signup\')" id = "loginsignup"/> -->
				<input type = "button" name = "Forgot Password?" value = "Reset Password" onclick = "menulogic(\'forgotpass\')" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id = "verificationdiv" style = "display:none" class = "login">
			<form data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "verification" id = "verification">
				<span id = "verificationmessage" style = "color:red"></span>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value=""  class="required email" />
				<label for="verification">Verification Code:</label>
				<input type="text" name="verification" id="verification" value="" class="required"/>
				<input type = "hidden" name = "formTXT" id = "formTXT" value = "verification" />
				<input type="submit" name = "submit" value = "Get Verified!" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id = "forgotpass" style = "display:none" class = "login">
			<form style="width: 95%; margin: 2%;" data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "forgotpass" id = "forgotpass">
				<span id = "forgotpassmsg" style = "color:red"></span>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value=""  class="required email" />
				<input type = "hidden" name = "formTXT" id = "formTXT" value = "forgotpass" />
				<input type="submit" name = "submit" value = "Get Password!" style="margin-top: 2%" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id = "changepass" style = "display:none" class = "login">
			<form data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "changepass" id = "changepass">
				<span id = "changepassmsg" style = "color:red"></span>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value=""  class="required email" />
				<label for="email">Temp Password:</label>
				<input type="password" name="temppass" id="temppass" value=""  class="required" />
				<label for="email">New Password:</label>
				<input type="password" name="pass" id="pass" value=""  class="required" />
				<label for="email">Confirm New Password:</label>
				<input type="password" name="pass1" id="pass1" value=""  class="required" />
				<input type = "hidden" name = "formTXT" id = "formTXT" value = "changepass" />
				<input type="submit" name = "submit" value = "Change Password!" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id="menu" style = "display:none">
		<div data-role="content" class="ui-overlay-shadow ui-corner-bottom ui-content ui-body-a">
		<h2 align="center"></h2>
		<span id = "menumessage"><?php echo $email;?></span>
		<a onclick = "checkForUpdates();" data-role="button"  data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Update</span>
			</span>
		</a>';
for($i = 0; $i < $numberofpages; $i++)
{
	if($pagesHidden[$i] == 0)
	{	//srs_lnm
		$config	.= '<a id="AppPage_'.$pagesArray[$i].'" onclick="pageForward(\''.$pagesArray[$i].'\', '.$pagesLogin[$i].')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
					<span class="ui-btn-inner ui-btn-corner-all">
						<span class="ui-btn-text">'.$pagesNames[$i].'</span>
					</span>
				</a>';
	}
}

$config	.= '
		<a onclick="menulogic(\'login\')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow login" id = "menulogin" style = "display:none">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Log In</span>
			</span>
		</a>
		<a onclick="logOut()" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow login" id = "menulogout" style = "display:none">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Log Out</span>
			</span>
		</a>
		<!-- <a onclick="menulogic(\'signup\')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow login">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Sign Up</span>
			</span>
		</a>  -->
		<a onclick="menulogic(\'qr\')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Share</span>
			</span>
		</a>
		<!-- FRANCIS: REMOVED ABOUT BUTTON/PAGE FROM ALL APPS -->
		<!--<a onclick="menulogic(\'about\')" data-role="button"  data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">About</span>
			</span>
		</a> -->
		<a onclick="menulogic(\'menu\')" data-role="button" data-rel="dialog" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Cancel</span>
			</span>
		</a>
		</div>
		</div>
		</td></tr>
		<tr><td width = "100%">
		<div data-theme="c"  data-rel="dialog" id="qr" style = "top:40px">
			<div  width = "100%" data-role="content" style="text-align:center; margin-top: 4%;">
				<div id="QRShareBox" width = "100%"  style=" background:url(\'../../../images/sharepage_QR.png\') no-repeat;background-position:center; background-size:225px 225px;">
					<img src="qrcode.jpg" align="center" width="195" height="195" style=" box-sizing: initial; display:block;margin-left: auto;margin-right:auto; padding-top:20px;padding-bottom:20px"></img>
				</div>
				<br>
				<a style = "position:relative;bottom:0px; left:0px;right:-7px" onclick ="checkLinks(\'https://www.facebook.com/dialog/feed?app_id=396518167046244&redirect_uri=http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/liveapp_current.php&display=touch&link=http://www.launchliveapp.com/'.$folderUserName.'/&picture=http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/appicon.png?post_id=none\',\'1\');" ><img src = "'.$source.'"></img></a>
				<br>
				<br>
				<a style = "position:relative;bottom:0px; left:0px;right:-7px;" onclick ="checkLinks(\'https://twitter.com/share?url=https://www.launchliveapp.com/'.$folderUserName.'/&text=Check+out+this+app!\', \'1\');"><img src="../../../images/sharepage_twitter.png" style="padding-left:-20px;padding-right:-20px;"/></a>

				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				<br><br>
				<div width = "300px">
				<a href="mailto:?subject=Download%20the%20'.$folderUserName.'%20LiveApp&body=Hello!%0A%0ADownload%20the%20'.$folderUserName.'%20LiveApp%20by%20clicking%20the%20link%20below!%0A%0Ahttp://www.launchliveapp.com/'.$folderUserName.'"><img src="../../../images/sharepage_email.png"></a>
				<br>
				</div>
			</div>
		</div>
		</td></tr>
		<tr><td>

		<div data-theme="c" data-transition="slidedown" data-rel="dialog" id="about" style = "top:40px;text-align:center;background-color:#707079;color:#ffffff;">
			<div data-role="content" style="display:none;"><!-- FRANCIS: REMOVED ABOUT BUTTON/PAGE FROM ALL APPS -->
			<h2>About '.$folderUserName.'</h2>
			<div>
				<img src="http://www.launchliveapp.com/images/about_top.png" style="width:100%;"/>
				<div style="color:#707079;background-image:url(\'http://www.launchliveapp.com/images/about_bg.png\');background-repeat:repeat-y;background-size:100%;margin-top:-7px;margin-bottom:-7px;">
				' . $about . '
				</div>
				<img src="http://www.launchliveapp.com/images/about_bottom.png" style="width:100%;"/>
				<div>
				<div align="center">
				<h2>Powered by:</h2>
				<img src="http://www.launchliveapp.com/images/about_logo.png" width="320" height="112"/>
				<br>
				</div>
				</div>
				</div>
			</div>
		</div>

		</td></tr>
		<tr><td width = "100%">
		<div data-theme="c"  data-rel="dialog" id="videoplayerdiv" style = "top:40px;background-color:black;">
		<div  width = "100%" style="height:480px;width:320px;" id = "videoholder">
		</div>
		</div>
		</td></tr>
		</table>
		<div style = "display:none">
		<input type = "button" id = "confirm" onclick = "confirmReload();" style = "display:none;visibility:hidden" height = "1px" />
		</div>
		'
		;


$configipad .=	'
		</div>
		<table width = "100%" cellpadding="0" style="position:relative; top:8vh; z-index: 50">
		<tr><td>
		<div data-theme="c" data-rel="dialog" id = "signup" class = "login" style = "display:none">
			<form data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "signupform" id = "signupform">
				<span id = "signupmessage" style = "color:red"></span>
				<label for="username">Username:</label>
				<input type="text" name="username" id="username" value="" class="required"/>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value=""  class="required email" />
				<label for="password">Password:</label>
				<input type="password" name="password" id="password" value="" class="required"/>';
/*
		for($i = 1; $i < 8; $i++)
		{
			if($inputs[$i] !== "None")
			{
				if($inputr[$i] == 1)
				{
					$required = "required";
				}
				else
				{
					$required = "";
				}
				$configipad .='
				<label for="input'.$i.'">'.$inputs[$i].':</label>
				<input type="text" name="input'.$i.'" id="input'.$i.'" value="" class = "'.$required.'" />
				';
			}
		}
		*/
$configipad .='
		<input type="submit" name = "submit" value = "Sign Up" />
		<input type = "hidden" name = "formTXT" id = "formTXT" value = "signup" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id = "login" style = "display:none" class = "login" style="width: 95%; margin-top: 1%; margin-right: 1%; margin-left: 1.5%;">
			<form style="width: 95%; margin: 2%;" data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "loginform" id = "loginform">
				<span id = "loginmessage" style = "color:red"></span>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value="" class="required email" />
				<label for="password" style="margin-top: 2%">Password:</label>
				<input type="password" name="password" id="password" value=""  class="required"/>
				<input type = "hidden" name = "formTXT" id = "formTXT" value = "login" />
				<input type="submit" name = "submit" value = "Log In" style="margin-top: 2%" />
				<!-- <input type = "button" name = "Sign Up" value = "Not a member? Sign Up Here" onclick = "menulogic(\'signup\')" id = "loginsignup"/> -->
				<input type = "button" name = "Forgot Password?" value = "Reset Password" onclick = "menulogic(\'forgotpass\')" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id = "verificationdiv" style = "display:none" class = "login">
			<form data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "verification" id = "verification">
				<span id = "verificationmessage" style = "color:red"></span>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value=""  class="required email" />
				<label for="verification">Verification Code:</label>
				<input type="text" name="verification" id="verification" value="" class="required"/>
				<input type = "hidden" name = "formTXT" id = "formTXT" value = "verification" />
				<input type="submit" name = "submit" value = "Get Verified!" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id = "forgotpass" style = "display:none" class = "login">
			<form style="width: 95%; margin: 2%;" data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "forgotpass" id = "forgotpass">
				<span id = "forgotpassmsg" style = "color:red"></span>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value=""  class="required email" />
				<input type = "hidden" name = "formTXT" id = "formTXT" value = "forgotpass" />
				<input type="submit" name = "submit" value = "Get password!" style="margin-top: 2%" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id = "changepass" style = "display:none" class = "login">
			<form data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "changepass" id = "changepass">
				<span id = "changepassmsg" style = "color:red"></span>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value=""  class="required email" />
				<label for="email">Temp Password:</label>
				<input type="password" name="temppass" id="temppass" value=""  class="required" />
				<label for="email">New Password:</label>
				<input type="password" name="pass" id="pass" value=""  class="required" />
				<label for="email">Confirm New Password:</label>
				<input type="password" name="pass1" id="pass1" value=""  class="required" />
				<input type = "hidden" name = "formTXT" id = "formTXT" value = "changepass" />
				<input type="submit" name = "submit" value = "Change Password!" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id="menu" style = "display:none">
		<div data-role="content" class="ui-overlay-shadow ui-corner-bottom ui-content ui-body-a">
		<h2 align="center"></h2>
		<a onclick="checkForUpdates();" data-role="button"  data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Update</span>
			</span>
		</a> ';
for($i = 0; $i < $numberofpages; $i++)
{
	if($pagesHidden[$i] == 0)
	{
		$configipad	.= '<a id="AppPage_'.$pagesArray[$i].'" onclick="pageForward(\''.$pagesArray[$i].'\', '.$pagesLogin[$i].')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
					<span class="ui-btn-inner ui-btn-corner-all">
						<span class="ui-btn-text">'.$pagesNames[$i].'</span>
					</span>
				</a>';
	}
}

$configipad .='
		<a onclick="menulogic(\'login\')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow login" id = "menulogin" style = "display:none">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Log In</span>
			</span>
		</a>
		<a onclick="logOut()" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow login" id = "menulogout" style = "display:none">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Log Out</span>
			</span>
		</a>
		<!-- <a onclick="menulogic(\'signup\')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow login">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Sign Up</span>
			</span>
		</a> -->
		<a onclick="menulogic(\'qr\')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Share</span>
			</span>
		</a>
		<!-- FRANCIS: REMOVED ABOUT BUTTON/PAGE FROM ALL APPS -->
		<!--<a onclick="menulogic(\'about\')" data-role="button"  data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">About</span>
			</span>
		</a>-->
		<a onclick="menulogic(\'menu\')" data-role="button" data-rel="dialog" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Cancel</span>
			</span>
		</a>
		</div>
		</div>
		</td></tr>
		<tr><td width = "100%">
		<div data-theme="c"  data-rel="dialog" id="qr" style = "top:40px">
			<div  width = "100%" data-role="content" style="text-align:center; margin-top: 4%;">
				<div id="QRShareBox" width = "100%"  style=" background:url(\'../../../images/sharepage_QR.png\') no-repeat;background-position:center; background-size:225px 225px;">
					<img src="qrcode.jpg" align="center" width="195" height="195" style=" box-sizing: initial; display:block;margin-left: auto;margin-right:auto; padding-top:20px;padding-bottom:20px"></img>
				</div>
				<br>
				<a style = "position:relative;bottom:0px; left:0px;right:-7px" onclick ="checkLinks(\'https://www.facebook.com/dialog/feed?app_id=396518167046244&redirect_uri=http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/liveapp_current.php&display=touch&link=http://www.launchliveapp.com/'.$folderUserName.'/&picture=http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/appicon.png?post_id=none\',\'1\');" ><img src = "'.$source.'"></img></a>
				<br>
				<br>
				<a style = "position:relative;bottom:0px; left:0px;right:-7px;" onclick ="checkLinks(\'https://twitter.com/share?url=https://www.launchliveapp.com/'.$folderUserName.'/&text=Check+out+this+app!\', \'1\');"><img src="../../../images/sharepage_twitter.png" style="padding-left:-20px;padding-right:-20px;"/></a>

				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				<br><br>
				<div width = "300px">
				<a href="mailto:?subject=Download%20the%20'.$folderUserName.'%20LiveApp&body=Hello!%0A%0ADownload%20the%20'.$folderUserName.'%20LiveApp%20by%20clicking%20the%20link%20below!%0A%0Ahttp://www.launchliveapp.com/'.$folderUserName.'"><img src="../../../images/sharepage_email.png"></a>
				<br>
				</div>
			</div>
		</div>
		</td></tr>

		<tr><td>
		<div data-theme="c" data-transition="slidedown" data-rel="dialog" id="about" style = "top:40px;text-align:center;background-color:#707079;color:#ffffff;">
			<div data-role="content" style="display:none;"><!-- FRANCIS: REMOVED ABOUT BUTTON/PAGE FROM ALL APPS -->
			<h2>About '.$folderUserName.'</h2>
			<div>
				<img src="http://www.launchliveapp.com/images/about_top.png" style="width:100%;"/>
				<div style="color:#707079;background-image:url(\'http://www.launchliveapp.com/images/about_bg.png\');background-repeat:repeat-y;background-size:100%;margin-top:-7px;margin-bottom:-7px;">
				' . $about . '
				</div>
				<img src="http://www.launchliveapp.com/images/about_bottom.png" style="width:100%;"/>
				<div>
				<div align="center">
				<h2>Powwered by:</h2>
				<img src="http://www.launchliveapp.com/images/about_logo.png" width="320" height="112"/>
				<br>
				</div>
				</div>
				</div>
			</div>
		</div>
		</td></tr>

		<tr><td width = "100%">
		<div data-theme="c"  data-rel="dialog" id="videoplayerdiv" style = "top:40px;background-color:black;">
		<div  width = "100%" style="height:1024px;width:100%;" id = "videoholder">
		</div>
		</div>
		</td></tr>
		</table>
		'
		;



$configbb1 .=	'
		</div>
		<table width = "100%" cellpadding="0" style="position: absolute; z-index: 50">
		<tr><td>

		<div style="display:none;" data-theme="c" data-rel="dialog" id = "signup" class = "login">
			<form data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "signupform" id = "signupform">
				<span id = "signupmessage" style = "color:red"></span>
				<label for="username">Username:</label>
				<input type="text" name="username" id="username" value="" class="required"/>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value=""  class="required email" />
				<label for="password">Password:</label>
				<input type="password" name="password" id="password" value="" class="required"/>';
/*
		for($i = 1; $i < 8; $i++)
		{
			if($inputs[$i] !== "None")
			{
				$configbb1 .='
				<label for="input'.$i.'">'.$inputs[$i].':</label>
				<input type="text" name="input'.$i.'" id="input'.$i.'" value=""  />
				';
			}
		}
		*/
$configbb1 .='
		<input type="submit" name = "submit" value = "Sign Up" />
		<input type = "hidden" name = "formTXT" id = "formTXT" value = "signup" />
			</form>
		</div>
		<div style="display:none;" data-theme="c" data-rel="dialog" id = "login" class = "login" >
			<form style="width: 95%; margin: 2%;" data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "loginform" id = "loginform">
				<span id = "loginmessage" style = "color:red"></span>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value="" class="required email" />
				<label for="password" style="margin-top: 2%">Password:</label>
				<input type="password" name="password" id="password" value=""  class="required"/>
				<input type = "hidden" name = "formTXT" id = "formTXT" value = "login" />
				<input type="submit" name = "submit" value = "Log In" style="margin-top: 2%" />
				<!-- <input type = "button" name = "Sign Up" value = "Not a member? Sign Up Here" onclick = "menulogic(\'signup\')" id = "loginsignup" /> -->
				<input type = "button" name = "Forgot Password?" value = "Reset Password" onclick = "menulogic(\'forgotpass\')" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id = "verificationdiv" style = "display:none" class = "login">
			<form data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "verification" id = "verification">
				<span id = "verificationmessage" style = "color:red"></span>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value=""  class="required email" />
				<label for="verification">Verification Code:</label>
				<input type="text" name="verification" id="verification" value="" class="required"/>
				<input type = "hidden" name = "formTXT" id = "formTXT" value = "verification" />
				<input type="submit" name = "submit" value = "Get Verified!" />
			</form>
		</div>
				<div data-theme="c" data-rel="dialog" id = "forgotpass" style = "display:none" class = "login">
			<form style="width: 95%; margin: 2%;" data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "forgotpass" id = "forgotpass">
				<span id = "forgotpassmsg" style = "color:red"></span>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value=""  class="required email" />
				<input type = "hidden" name = "formTXT" id = "formTXT" value = "forgotpass" />
				<input type="submit" name = "submit" value = "Get password!" style="margin-top: 2%" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id = "changepass" style = "display:none" class = "login">
			<form data-ajax="false" action = "http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/processusers.php" method = "post" name = "changepass" id = "changepass">
				<span id = "changepassmsg" style = "color:red"></span>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value=""  class="required email" />
				<label for="email">Temp Password:</label>
				<input type="password" name="temppass" id="temppass" value=""  class="required" />
				<label for="email">New Password:</label>
				<input type="password" name="pass" id="pass" value=""  class="required" />
				<label for="email">Confirm New Password:</label>
				<input type="password" name="pass1" id="pass1" value=""  class="required" />
				<input type = "hidden" name = "formTXT" id = "formTXT" value = "changepass" />
				<input type="submit" name = "submit" value = "Change Password!" />
			</form>
		</div>
		<div data-theme="c" data-rel="dialog" id="menu" style = "display:none">
		<div data-role="content" class="ui-overlay-shadow ui-corner-bottom ui-content ui-body-a">
		<h2 align="center"></h2>
		<span id = "menumessage"><?php echo $email;?></span>
		<a onclick = "updateBlackberry();" data-role="button"  data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Update</span>
			</span>
		</a>';
for($i = 0; $i < $numberofpages; $i++)
{
	if($pagesHidden[$i] == 0)
	{
		$configbb1	.= '<a id="AppPage_'.$pagesArray[$i].'" onclick="pageForward(\''.$pagesArray[$i].'\', '.$pagesLogin[$i].')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
					<span class="ui-btn-inner ui-btn-corner-all">
						<span class="ui-btn-text">'.$pagesNames[$i].'</span>
					</span>
				</a>';
	}
}

$configbb1	.= '
		<a onclick="menulogic(\'login\')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow login" id = "menulogin" style = "display:none">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Log In</span>
			</span>
		</a>
		<a onclick="logOut()" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow login" id = "menulogout" style = "display:none">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Log Out</span>
			</span>
		</a>
		<!-- <a onclick="menulogic(\'signup\')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow login">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Sign Up</span>
			</span>
		</a> -->
		<a onclick="menulogic(\'qr\')" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Share</span>
			</span>
		</a>
		<!-- FRANCIS: REMOVED ABOUT BUTTON/PAGE FROM ALL APPS -->
		<!--<a onclick="menulogic(\'about\')" data-role="button"  data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">About</span>
			</span>
		</a> -->
		<a onclick="menulogic(\'menu\')" data-role="button" data-rel="dialog" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Cancel</span>
			</span>
		</a>
		</div>
		</div>
		</td></tr>

		<tr><td width = "100%">
		<div data-theme="c"  data-rel="dialog" id="qr" style = "top:40px">
			<div  width = "100%" data-role="content" style="text-align:center; margin-top: 4%;">
				<div id="QRShareBox" width = "100%"  style=" background:url(\'../../../images/sharepage_QR.png\') no-repeat;background-position:center; background-size:225px 225px;">
					<img src="qrcode.jpg" align="center" width="195" height="195" style=" box-sizing: initial; display:block;margin-left: auto;margin-right:auto; padding-top:20px;padding-bottom:20px"></img>
				</div>
				<br>
				<a style = "position:relative;bottom:0px; left:0px;right:-7px" onclick ="checkLinks(\'https://www.facebook.com/dialog/feed?app_id=396518167046244&redirect_uri=http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/liveapp_current.php&display=touch&link=http://www.launchliveapp.com/'.$folderUserName.'/&picture=http://www.launchliveapp.com/users-folder/'.$folderUserName.'/liveapp/appicon.png?post_id=none\',\'1\');" ><img src = "'.$source.'"></img></a>
				<br>
				<br>
				<a style = "position:relative;bottom:0px; left:0px;right:-7px;" onclick ="checkLinks(\'https://twitter.com/share?url=https://www.launchliveapp.com/'.$folderUserName.'/&text=Check+out+this+app!\', \'1\');"><img src="../../../images/sharepage_twitter.png" style="padding-left:-20px;padding-right:-20px;"/></a>

				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				<br><br>
				<div width = "300px">
				<a href="mailto:?subject=Download%20the%20'.$folderUserName.'%20LiveApp&body=Hello!%0A%0ADownload%20the%20'.$folderUserName.'%20LiveApp%20by%20clicking%20the%20link%20below!%0A%0Ahttp://www.launchliveapp.com/'.$folderUserName.'"><img src="../../../images/sharepage_email.png"></a>
				<br>
				</div>
			</div>
		</div>
		</td></tr>

		<tr><td>
			<div data-theme="c" data-transition="slidedown" data-rel="dialog" id="about" style = "top:40px;text-align:center;background-color:#707079;color:#ffffff;display:none;">
				<div data-role="content" style="display:none;"><!-- FRANCIS: REMOVED ABOUT BUTTON/PAGE FROM ALL APPS -->
					<h2>About '.$folderUserName.'</h2>
					<div>
						<img src="../../../images/about_top.png" style="width:100%;"/>
						<div style="color:#707079;background-image:url(\'../../../images/about_bg.png\');background-repeat:repeat-y;background-size:100%;margin-top:-7px;margin-bottom:-7px;">
							'.$about.'
						</div>
						<img src="../../../images/about_bottom.png" style="width:100%;"/>
						<div>
							<div align="center">
								<h2>Powered by:</h2>
								<img src="../../../images/about_logo.png" width="320" height="112"/>
								<br>
							</div>
						</div>
					</div>
				</div>
			</div>
		</td></tr>
		<tr><td width = "100%">
		<div data-theme="c"  data-rel="dialog" id="videoplayerdiv" style = "top:40px;background-color:black;display:none;">
		<div  width = "100%" style="height:480px;width:320px;" id = "videoholder">
		</div>
		</div>
		</td></tr>
		</table>
		'
		;

$menu = '<!DOCTYPE html>
		<html>
		<head>
		<title>LiveApp Menu</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" href="../../../testskins/jplayer.blue.monday.css" rel="stylesheet" />
<link rel="stylesheet" href="../../../liveappmenucss/themes/liveapp.css" />
<link rel="stylesheet" href="../../../liveappmenucss/jquery.mobile.structure-1.0.1.min.css" />

<script type="text/javascript" src="../../../scripts/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="../../../scripts/jquery.mobile-1.0.1.min.js"></script>

<script type="text/javascript" src="../../../js/jquery.jplayer.min.js"></script>
<script type="text/javascript">
function updateBlackberry()
{
if(navigator.onLine == false)
	{
		alert("You must have a data connection to update this app.");
		return false;
	}
	else
	{



	  window.location.reload();
	   blackberry.update();
	}

}
</script>

		</head>
		<body>
		<div data-theme="c" data-transition="slidedown" data-role="page" data-rel="dialog" id="popup">
		<div data-role="content" class="ui-overlay-shadow ui-corner-bottom ui-content ui-body-a" role="main">
		<h2 align="center"></h2>
		';
for($i = 0; $i < $numberofpages; $i++)
{
if($pagesArray[$i] == 1)
{
$bblinks = "liveapp_currentbb.php";
}
else
{
$bblinks = "page".$pagesArray[$i].".php";
}
	if($pagesHidden[$i] == 0)
	{
	$menu .= '<a id="AppPage_'.$pagesArray[$i].'" onclick="home.go()" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
					<span class="ui-btn-inner ui-btn-corner-all">
						<span class="ui-btn-text">'.$pagesNames[$i].'</span>
					</span>
				</a>';
	/*
		$menu .= '<a href= "'.$bblinks.'" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
					<span class="ui-btn-inner ui-btn-corner-all">
						<span class="ui-btn-text">'.$pagesNames[$i].'</span>
					</span>
				</a>';
				*/
	}
}
$menu .= '
		<a href="qr.html" data-role="button" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Share</span>
			</span>
		</a>
		<a href="../../../liveappabout.html" data-role="button"  data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">About</span>
			</span>
		</a>
		<a onclick="history.go(-1)" data-role="button" data-rel="dialog" data-transition="slidedown" data-theme="b" class="ui-btn ui-btn-up-b ui-btn-corner-all ui-shadow">
			<span class="ui-btn-inner ui-btn-corner-all">
				<span class="ui-btn-text">Cancel</span>
			</span>
		</a>
		</div>
		</div>
		</body>
		</html>';

// do all your file writing here
$menuhandle = fopen("users-folder/$folderUserName/liveapp/liveappmenu.html","w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
fputs($menuhandle,$menu); // Write the contents of $config to the file opened in $handle
fclose($menuhandle); // Close the opened file

$qr = '<!DOCTYPE html>
<html>
	<head>
	<title>Share Your LiveApp</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" href="../../../testskins/jplayer.blue.monday.css" rel="stylesheet" />
<link rel="stylesheet" href="../../../liveappmenucss/themes/liveapp.css" />
<link rel="stylesheet" href="../../../liveappmenucss/jquery.mobile.structure-1.0.1.min.css" />

<script type="text/javascript" src="../../../scripts/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="../../../scripts/jquery.mobile-1.0.1.min.js"></script>

<script type="text/javascript" src="../../../js/jquery.jplayer.min.js"></script>
</head>
<body>
<div data-role="page">
	<div data-role="header" data-transition="slidedown" data-theme="b" data-position="inline">
		<a onclick="history.go(-1)">Back</a>
		<h1>Share This App</h1>
	</div>
	<div align="center">
	<p>Scan the QR code below to download this app</p>
	<br>
	<img src="http://chart.apis.google.com/chart?chs=545x545&cht=qr&chld=M|0&chl=http://www.launchliveapp.com/'.$folderUserName.'/" align="center" width="240" height="240"></img>
	<br>
	<br>
	<p>Or download your the app from this link:</p>
	<a href="http://www.launchliveapp.com/'.$folderUserName.'/">www.launchliveapp.com/'.$folderUserName.'</a>
	<br>
	</div>
</div><!-- /page -->
</body>
</html>';
// do all your file writing here
$qrhandle = fopen("users-folder/$folderUserName/liveapp/qr.html","w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
fputs($qrhandle,$qr); // Write the contents of $config to the file opened in $handle
fclose($qrhandle); // Close the opened file


$config .= "
</div><!-- closing the 'appDiv' and page -->";


$config .= '</div>
<script>
//srsbg
$("#app-page").bind("pageshow", function(event) {
	/*var height_interval = setInterval(function(){
		console.log("height: " + $("html").height());
		if($("body").height() != 0) {
			clearInterval(height_interval);
		}
	},10);*/
});

</script>

</body>
</html>';

$configipad .='
</div></div> <!--appDiv srsipad-->
<script>
//srsbg
$("#app-page").bind("pageshow", function(event) {
	/*var height_interval = setInterval(function(){
		if($("html").height() != 0) {
			clearInterval(height_interval);
		}
	},10);*/
});
</script>';

$configbb .= '
</div>
<script>
//srsbg
$("#app-page").bind("pageshow", function(event) {
	/*var height_interval = setInterval(function(){
		if($("html").height() != 0) {
			clearInterval(height_interval);
		}
	},10);*/
});
</script>';

$configbb1 .= '
</div>
<script>
//srsbg
$("#app-page").bind("pageshow", function(event) {
	/*var height_interval = setInterval(function(){
		if($("html").height() != 0) {
			clearInterval(height_interval);
		}
	},10);*/
});
</script>
';

$configipad .= "</body>
</html>";
$configbb .= "</body>
</html>";
$configbb1 .= "</body>
</html>";


//if($currentVersion != "0")
//{
$cache .= "\nliveapp_v1.$prevVersion.html";
$cacheipad .= "\nliveapp_v1.$prevVersion.html";
$cachebb .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/liveapp_v1.".$prevVersion.".xml";
$cachebb1 .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/liveapp_v1.".$prevVersion.".xml";
$cachebb1text .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/liveapp_v1.".$prevVersion.".xml";
$cachebbtext .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/liveapp_v1.".$prevVersion.".xml";

//}

$cachebb1 .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/liveapp_currentbb1.php\n";
$cachebb .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/liveapp_currentbb.php\n";
$cachebb1text .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/liveapp_currentbb1.php\n";
$cachebbtext .= "\nhttp://www.launchliveapp.com/users-folder/".$folderUserName."/liveapp/liveapp_currentbb.php\n";



$cachebb10 = str_replace("\nliveapp_current.php", "\nliveapp_currentbb10.php", $cache);

//Mikael - removing cache
$configWeb = $config;
$configBB10 = str_replace("http://www.launchliveapp.com/scripts/bookmark_bubble_test.js", "http://www.launchliveapp.com/scripts/bookmark_bubble_bb10.js", $config);
$configBB10 = str_replace(".manifest>", "bb10.manifest>", $configBB10);
$start = strpos($configWeb, "<html");
$end = strpos($configWeb, "<head>");

$configWeb = substr_replace($configWeb,"<html>",$start,$end - $start);
$configWeb = str_replace("http://www.launchliveapp.com/scripts/bookmark_bubble_test.js"," ",$configWeb);

$handle3 = fopen("users-folder/$folderUserName/liveapp/liveapp_currentWeb.php","w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
fputs($handle3,$configWeb); // Write the contents of $config to the file opened in $handle
fclose($handle3); // Close the opened file
/*
$configWeb = '<!DOCTYPE html>
<html>
'.$remainder;
//End of Mikael
*/

/*
Jake 7-19-2012
<link type="text/css" href="local:///jplayer.blue.monday.css" rel="stylesheet" />
<link rel="stylesheet" href="local:///liveapp.css" />
<link rel="stylesheet" href="local:///jquery.mobile.structure-1.0.1.min.css"/>
<script type="text/javascript" src="local:///jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="local:///jquery.mobile-1.0.1.min.js" ></script>
<script type = "text/javascript" src = "../../../scripts/jquery.validate.js" ></script>
<script type="text/javascript" src="local:///jquery.jplayer.min.js" ></script>

<link type="text/css" href="../../../testskins/jplayer.blue.monday.css" rel="stylesheet" />
<link rel="stylesheet" href="../../../liveappmenucss/themes/liveapp.css" />
<link rel="stylesheet" href="../../../liveappmenucss/jquery.mobile.structure-1.0.1.min.css" />
<script type="text/javascript" src="../../../scripts/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="../../../scripts/jquery.mobile-1.0.1.min.js"></script>
<script type = "text/javascript" src = "../../../scripts/jquery.validate.js"></script>
<script type="text/javascript" src="../../../js/jquery.jplayer.min.js"></script>
*/
$configbb = $configbb1;
$configbb1 = str_replace('<link type="text/css" href="local:///jplayer.blue.monday.css" rel="stylesheet" />', '<link type="text/css" href="../../../testskins/jplayer.blue.monday.css" rel="stylesheet" />
', $configbb1);
$configbb1 = str_replace('<link rel="stylesheet" href="local:///liveapp.css" />', '<link rel="stylesheet" href="../../../liveappmenucss/themes/liveapp.css" />
', $configbb1);
$configbb1 = str_replace('<link rel="stylesheet" href="local:///jquery.mobile.structure-1.0.1.min.css"/>', '<link rel="stylesheet" href="../../../liveappmenucss/jquery.mobile.structure-1.0.1.min.css" />
', $configbb1);
$configbb1 = str_replace('<script type="text/javascript" src="local:///jquery-1.6.4.min.js"></script>', '<script type="text/javascript" src="../../../scripts/jquery-1.6.4.min.js"></script>
', $configbb1);
$configbb1 = str_replace('<script type="text/javascript" src="local:///jquery.mobile-1.0.1.min.js" ></script>', '<script type="text/javascript" src="../../../scripts/jquery.mobile-1.0.1.min.js"></script>
', $configbb1);
$configbb1 = str_replace('<script type="text/javascript" src="local:///jquery.jplayer.min.js" ></script>', '<script type="text/javascript" src="../../../js/jquery.jplayer.min.js"></script>
', $configbb1);
$configbb = str_replace('alert("Your Blackberry code needs to be updated to be able to access the latest features. Please update your application by navigating to the following link in your browser: http://www.launchliveapp.com/' .$folderUserName.'");', '', $configbb);
//alert("Your application appears to be out of date. Please visit http://www.launchliveapp.com/' .$folderUserName.' to download the latest version.");



// do all your file writing here
$handle = fopen("users-folder/$folderUserName/liveapp/liveapp_current.php","w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
fputs($handle,$config); // Write the contents of $config to the file opened in $handle
fclose($handle); // Close the opened file
$handle0 = fopen("users-folder/$folderUserName/liveapp/liveapp_currentipad.php","w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
fputs($handle0,$configipad); // Write the contents of $config to the file opened in $handle
fclose($handle); // Close the opened file
$handle2 = fopen("users-folder/$folderUserName/liveapp/liveapp_currentbb1.php","w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
fputs($handle2,$configbb1); // Write the contents of $config to the file opened in $handle
fclose($handle2); // Close the opened file
$handle3 = fopen("users-folder/$folderUserName/liveapp/liveapp_currentbb.php","w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
fputs($handle3,$configbb); // Write the contents of $config to the file opened in $handle
fclose($handle3); // Close the opened file
$newmanifest = fopen("users-folder/$folderUserName/liveapp/".$folderUserName.".manifest","w+");
fputs($newmanifest, $cache);
fclose($newmanifest);
$newmanifestipad = fopen("users-folder/$folderUserName/liveapp/".$folderUserName."ipad.manifest","w+");
fputs($newmanifestipad, $cacheipad);
fclose($newmanifestipad);
$newmanifestbb = fopen("users-folder/$folderUserName/liveapp/".$folderUserName."bb.manifest","w+");
fputs($newmanifestbb, $cachebb);
fclose($newmanifestbb);
$newmanifestbbtext = fopen("users-folder/$folderUserName/liveapp/".$folderUserName."bbtext.manifest","w+");
fputs($newmanifestbbtext, $cachebbtext);
fclose($newmanifestbbtext);
$newmanifestbb1 = fopen("users-folder/$folderUserName/liveapp/".$folderUserName."bb1.manifest","w+");
fputs($newmanifestbb1, $cachebb1);
fclose($newmanifestbb1);
$newmanifestbb1text = fopen("users-folder/".$folderUserName."/liveapp/".$folderUserName."bb1text.manifest","w+");
fputs($newmanifestbb1text, $cachebb1text);
fclose($newmanifestbb1text);

$handle = fopen("users-folder/$folderUserName/liveapp/liveapp_currentbb10.php","w+"); // Opens the script with w+ (writing intentions) and stores it in variable for quicker access.
fputs($handle,$configBB10);
$newmanifestbb10 = fopen("users-folder/$folderUserName/liveapp/".$folderUserName."bb10.manifest","w+");
fputs($newmanifestbb10, $cachebb10);
fclose($newmanifestbb10);

// ZAC - NEW
// Update the app's publishing timestamp to the current date and time.
// The Portal displays this timestamp, which always uses GMT timezone.
$currentDate = gmdate("Y-m-d H:i:s", time());
$publishDateQuery = "UPDATE user_settings SET last_publish='$currentDate' WHERE user_id='$userId'";
$publishDateResult = mysql_query($publishDateQuery);


$downloadFile = new appDownloadFile($userId, $folderUserName,$loyalty);
$download = $downloadFile -> getDownloadFileString();

$newDownload = fopen("".$folderUserName."/index.php","w+");
fputs($newDownload,$download);
fclose($newDownload);

if($login == 1)
{
	$loginobject -> createFiles();
}


$sourceqr = "http://chart.apis.google.com/chart?chs=545x545&cht=qr&chld=M|0&chl=http://www.launchliveapp.com/".$folderUserName."/";
$imageqr = @imageCreateFromPng($sourceqr);
imagejpeg($imageqr, "users-folder/".$folderUserName."/liveapp/qrcode.jpg", 100);
if($autonew == '1')
{
	header("Location:http://www.launchliveapp.com/autopublish.php?user_id=$userId&user_name=$folderUserName");
}
else
{

	if($published == '0')
	{
		if($appName == '')
		{
			$appName = $folderUserName;
		}

		$updateQuery = "UPDATE users SET published = '1' WHERE user_id = '$userId'";
		$updateResult = mysql_query($updateQuery);

		header('Location:http://50.63.60.90/CompileApps.php?fromServer=launchliveapp&ext=com&username='.$folderUserName.'&appname="'.$appName.'"');

	}
	elseif($iconChange == '1')
	{
		if($appName == '')
		{
			$appName = $folderUserName;
		}

		$newVersionNumber = $versionNumber + 1;
		$updateQuery = "UPDATE user_settings SET icon_change = '0', version = '$newVersionNumber' WHERE user_id = '$userId'";
		$updateResult = mysql_query($updateQuery);

		header('Location:http://50.63.60.90/CompileAppsVersioning.php?fromServer=launchliveapp&ext=com&username='.$folderUserName.'&appname="'.$appName.'"&version=1.'.$versionNumber);

	}
	else
	{
		$var = strtolower($_SERVER['HTTP_USER_AGENT']);


	    	 if(strpos($var, "ipad") === False){

	    		header("Location:portal.php?alert=publish");

	    	}else{

	    	    header("Location:testportal_galleryiPad.php?alert=publish");

	    	}

	}
}
?>
